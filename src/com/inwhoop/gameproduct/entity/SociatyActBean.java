package com.inwhoop.gameproduct.entity;

/**
 * @Describe: TODO    公会活动对象
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/11 10:13
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SociatyActBean extends BaseBean {
	public int id =0;
	public String userid;
	public String guildid;
	public String actname;
	public String actcontent;
	public String actimage;
	public String begintime;
	public String endtime;
	public String creattime ="";
}
