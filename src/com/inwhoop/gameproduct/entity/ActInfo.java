package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * 
 * 活动详情
 * @Project: MainActivity
 * @Title: ActInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-5-8 下午8:39:30
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ActInfo implements Serializable{
	
	public String actHtml;
	
}
