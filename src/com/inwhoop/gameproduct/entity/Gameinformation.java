package com.inwhoop.gameproduct.entity;

/**
 * @Project: MainActivity
 * @Title: Gameinformation.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO  游戏资讯
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-28 下午8:56:52
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Gameinformation extends BaseBean{
	
	public int newsid;
	public String newsname;
	public String newsimage;
	public String newsremark;
    public String newshtml ="";//内容
    public String creattime ="";
}
