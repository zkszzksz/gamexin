package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * 
 * 通讯录联系人信息
 * @Project: MainActivity
 * @Title: MailListInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-5-30 下午2:48:40
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class MailListInfo implements Serializable{

	public int id;  //id
	public String mailname;  //姓名
	public String tel;  //电话
	public boolean isInvate;  //是否已经发送了邀请
	
}
