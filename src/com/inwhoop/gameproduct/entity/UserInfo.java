package com.inwhoop.gameproduct.entity;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 * @date 2014/4/28 18:33
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class UserInfo extends BaseBean{
    public String id = ""; // 注意此项是登录后返回的"id"字段,
    public String userid = ""; // 登录账号
    public String username ="";//,用户昵称
    public String sex = "-1";   //服务器返回-1保密/0女/1男
    public String birthday = "";
    public String remark = "";//个人说明
    public String userphoto = "";
    public String email="";
    public String useraddress = "";
    public String xlat ="";
    public String ylong ="";
    public int islook =1;
    public String huanxinpwd =""; //返回的环信密码
//    public String userpwd = "";   //
//    public String openfirepassword =""; //返回的环信id,目前没用

    public String chatId = "";


    public String password="";

    //把服务器数据 返回男女字符
    public String getSexString() {
        if (sex.equals("1")) {
            return "男";
        } else if (sex.equals("0")) {
            return "女";
        } else
            return "保密";
    }
    @Override
    public String toString() {
        return "UserInfo{" +
                ", chatId='" + chatId + '\'' +
                ", id='" + id + '\'' +
                ", userphoto='" + userphoto + '\'' +
                ", userNick='" + username + '\'' +
                ", remark='" + remark + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", useraddress='" + useraddress + '\'' +
                '}';
    }
}
