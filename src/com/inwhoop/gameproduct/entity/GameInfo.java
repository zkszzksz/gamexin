package com.inwhoop.gameproduct.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: GameInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO 首页游戏信息
 * @date 2014-4-3 下午12:49:40
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GameInfo extends BaseBean {

    //活动类型静态变量
//    public static final int TYPE_ACTIVITY=0;
//    public static final int TYPE_HOT=1;
//    public static final int TYPE_HOME_BANNER=2;
//    public static final int TYPE_INTRASTING=4;

    public int gameid; //游戏id ，
    public int typeid; //类型id ,游戏类型  1|网游 2|手游 3|页游 4|单机
    public String gamename; //游戏名称
    public String gameicon = ""; //游戏icon图片，游戏图片 无用
    public String gamelogo = ""; //游戏小正方形图
    public String gamemainimage = "";  //游戏首页展示图
//    public String gamemainimage; //游戏缩略图
    public String gameinfoimage = "";  //游戏首页banner图
    public String logotitle; //游戏首页banner图题目
    public String schtxt = "";       //游戏简介
    //            hotvalue          //热值
    public int gamestate = 0; //游戏状态   1|开发中 2|内测 3|公测 4|停运 ，列表需要手动写如“正在运营”字样 ，GamelistAdapter里要根据状态设置颜色
    public String gameremark = "";  // 游戏标签
    public int collectcount;  //多少人收藏
    public String type1 ="";//动作冒险/赛车竞速/即时战略/经营策略/
    public List<GameImages> imglist = new ArrayList<GameImages>();// 图集的图片地址集合
    public boolean collectgame = false;//标识用户是否收藏了此游戏

//----以上为目前所用参数-------

    public String infoimageUrl = "";///upload/gameinfo/info/
    public boolean isActive =false; //是否有活动
    public boolean isActcode =false; //是否有激活码
    public boolean isGift=false; //是否有礼包
    public int gametype =0; //游戏是否属于活动、热门、首页banner、感兴趣4。。用于自定义保存到数据库的
//    public int gameStateCode =0;//游戏运营状态数字，对应gameState标识

}
