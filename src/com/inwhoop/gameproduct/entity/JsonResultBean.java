package com.inwhoop.gameproduct.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2015 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: TODO
 * @Package com.inwhoop.gameproduct.entity
 * @Date: 2015/03/16 11:53
 * @version: 1.0
 */
public class JsonResultBean {
    public boolean isOk = false;
    public String msg = "";
    public List list = new ArrayList();

    public Object oneBean() {
        if (list.size() != 0)
            return list.get(0);
        else
            return null;
    }

}
