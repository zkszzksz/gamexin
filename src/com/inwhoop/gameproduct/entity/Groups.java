package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**
 * 
 *   群组信息
 * @Project: MainActivity
 * @Title: Groups.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-18 下午3:17:09
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Groups implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String groupid;
	public String groupname;
	
}
