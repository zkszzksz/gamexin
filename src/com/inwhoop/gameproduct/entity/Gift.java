package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**
 * @Describe: TODO  礼包实体
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/04 18:08
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class Gift implements Serializable{
    public String name="";//礼包名称
    public String type="";//礼包类型。激活码|特权卡|礼包等等名字
    public String cd_key="";//礼包可兑换key，一般是key形式


}
