package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2015 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: 通用对象的父类
 * @Package com.inwhoop.gameproduct.entity
 * @Date: 2015/03/16 11:24
 * @version: 1.0
 */
public class BaseBean implements Serializable{
}
