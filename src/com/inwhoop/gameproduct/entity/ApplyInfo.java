package com.inwhoop.gameproduct.entity;

/**
 * @Project: MainActivity
 * @Title: SociatyApplyInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-23 下午1:49:27
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ApplyInfo extends BaseBean{

	public String askremark;   //附加消息
	public String birthday;
	public String guildName;
	public int id;
	public int sex;  //1男，0女，其他保密
	public String userId;
	public String userName;
	public String userPhoto;
	public String remark;
	public String address;
	public int guildid = 0;
	public String guildname;
	public String guildheadphoto;
	public String guildremark;
	public String username;   //指谁邀请我
	public boolean isAgree=false;//是否已经同意
	public boolean isManage=false; //此消息是否被处理
	public int isF;  //1表示好友申请 2表示公会申请3表示公会邀请  。访问接口的时候手动设置
    public String ugid;//应该是这条信息的id

	public String getsex(){
        if (sex == 1){
			return "男";
		}else if (sex == 0)
		    return "女";
        else
            return "保密";
	}

}
