package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * 
 * 存储聊天对象包括群聊单聊对象
 * @Project: MainActivity
 * @Title: ChatTagetList.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-17 下午3:35:01
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ChatTagetList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String tagetid;  //对象id
	public String newtime;  //最新聊天记录时间
	public String myid;  //登录用户id
	public int msgtype;   //消息类型 1表示单聊  2表示群聊
	public String tagethead;//对象头像
	public String tagetnick;//对象昵称

}
