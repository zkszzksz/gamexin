package com.inwhoop.gameproduct.entity;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2015 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: TODO 上传服务器，邀请加入工会的对象
 * @Package com.inwhoop.gameproduct.entity
 * @Date: 2015/03/24 10:33
 * @version: 1.0
 */
public class NetInvateBean extends BaseBean {
    private String guildid;
    private String userid ;  //逗号隔开的复数
    private String askremark;
    private String zuserid;

    public NetInvateBean(String guildid, String userid, String askremark, String zuserid) {
        this.guildid = guildid;
        this.userid = userid;
        this.askremark = askremark;
        this.zuserid = zuserid;
    }
}
