package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * 
 * 图集信息
 * @Project: MainActivity
 * @Title: GameImages.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-26 上午11:18:14
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class GameImages implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public int id;
	public String image="";
	public String imagetxt ="";

}
