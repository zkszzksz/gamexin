package com.inwhoop.gameproduct.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO   公会对象
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/13 15:43
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SociatyBean extends BaseBean {
    public int guildid = 0;
    public String guildname = "";
    public String guildheadphoto = "";     //logo图  //上传时是头像转成base64
    public String guildremark = "";//简介
    public String guildment= "";//公告
    public String userid;//公会会长Id
    public String createtime = "";//公会创建时间
    public String nickname="";//公会会长名字 ,现在新接口返回nickname
    public int personnum = 0;//公会成员数
    public List<GameInfo> gamemsg = new ArrayList<GameInfo>();//入驻游戏
    public List<String> gameids = new ArrayList<String>();  //创建公会时传的游戏id字符串,目前无用

    public String hxid ="";//环信id


}
