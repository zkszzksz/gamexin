package com.inwhoop.gameproduct.entity;

/**
 * 
 * 创建圈子信息
 * @Project: MainActivity
 * @Title: CreateLoopInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-5-28 下午7:09:49
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class CreateLoopInfo extends BaseBean{

	public String userid;
	public String circlename;
	public String circleheadphoto;
	public String circleremark;
	public double xlat;
	public double ylong;

	public int id =0;
	public String circleaddress ="";

}
