package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * @Project: MainActivity
 * @Title: Actvitieslist.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-19 下午3:32:59
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Actvitieslist implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int actId;
	public String actName;
	public String imgUrl;
	public String introduce;
	public String gameName;
	public String imgUrlPosition;
	public String actInfo;

}
