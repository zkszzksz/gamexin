package com.inwhoop.gameproduct.entity;

import android.content.Context;
import com.inwhoop.gameproduct.R;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO  圈子信息
 * @date 2014/5/21   15:15
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class LoopInfoBean extends BaseBean {
    public int id = 0;             //100001
    public String circleheadphoto = "";  //查询圈子，这里直接有全路径，不用拼字符串了
    public String circlename = "";    //名字
    public String circleremark = "";   //圈子简短说明
    public int personnum = 0;         //人数
    public int isHead = 0;
    public int userid = 0 ;//圈子boss的id

    //TODO  根据返回数据判断是否是圈子创建者，以改变圈子信息页面是否退出圈子按钮字，待完善
    public String isLoopHead(Context c) {
        switch (isHead) {
            case 0:
                return c.getResources().getString(R.string.add_loop);
            case 1:
                return c.getResources().getString(R.string.getOutLoop);
            default:
                return c.getResources().getString(R.string.add_loop);
        }
    }
}
