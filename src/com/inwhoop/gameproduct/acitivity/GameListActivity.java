package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GamelistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.entity.SubscibeGuideInfo;
import com.inwhoop.gameproduct.entity.SubscibeInfo;
import com.inwhoop.gameproduct.view.MyListView;
import com.inwhoop.gameproduct.view.MyListView.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project: MainActivity
 * @Title: SubscribeListActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 游戏列表页面
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-4 下午3:14:36
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GameListActivity extends BaseActivity implements
		OnRefreshListener {

	private LayoutInflater inflater = null;

	private MyListView leftListView = null; // 订阅信息列表 左边listview

	private ListView rightListView = null; // 导航列表 右边listview

	private String[] guide; // 右边引导数组

	private List<SubscibeInfo> list = null; // 游戏订阅信息列表

	private int page = 1; // 分页标签

	private List<SubscibeGuideInfo> guidelist = new ArrayList<SubscibeGuideInfo>(); // 引导的list

	private int id; // 当前选择的引导id

	private GamelistAdapter adapter = null; // 左边listview的adapter

	private RightAdapter rightAdapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.subscribe_layout);
		mContext = GameListActivity.this;
		inflater = LayoutInflater.from(mContext);
		init();
	}

	@Override
	public void init() {
		super.init();
		guide = getResources().getStringArray(R.array.subcribe_guide);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
				MyApplication.widthPixels / 4 * 3, LayoutParams.FILL_PARENT);
		leftListView = (MyListView) findViewById(R.id.leftlistview);
		leftListView.setLayoutParams(parm);
		leftListView.setRefreshListener(this);
		rightListView = (ListView) findViewById(R.id.rightlistview);
		setTitle(R.string.subscribe_tv);
		setHeadLeftButton(R.drawable.back_bg);
		setRightSecondBt(R.drawable.search);
		getGuidelist();
		read();
	}

	/**
	 * 根据id读取游戏信息
	 * 
	 * @Title: read
	 * @Description: TODO
	 * @param @param id 类型
	 * @return void
	 */
	private void read() {
		showProgressDialog("正在努力加载...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = getSubcrilist(id);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if (list.size() < 10) {
					leftListView.isRefreshable = false;
				} else {
					page++;
				}
				leftListView.setAdapter(adapter);
				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onRefresh() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = getSubcrilist(id);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				addhandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler addhandler = new Handler() {
		public void handleMessage(Message msg) {
			leftListView.onRefreshComplete();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if (list.size() < 10) {
					leftListView.isRefreshable = false;
				} else {
					page++;
				}
				break;

			default:
				break;
			}

		};
	};

	class RightAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return guidelist.size();
		}

		@Override
		public Object getItem(int position) {
			return guidelist.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			Holder holder = null;
			if (null == convertView) {
				holder = new Holder();
				convertView = inflater.inflate(
						R.layout.subscribe_rightlist_item, null);
				holder.guidetv = (TextView) convertView
						.findViewById(R.id.guide);
				holder.guidelayout = (LinearLayout) convertView
						.findViewById(R.id.guidelayout);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			if (guidelist.get(position).isSelect) {
				holder.guidelayout
						.setBackgroundResource(R.drawable.sub_right_select_bg);
			} else {
				holder.guidelayout
						.setBackgroundResource(R.drawable.sub_right_noselect_bg_03);
			}
			holder.guidetv.setText(guidelist.get(position).guide);
			return convertView;
		}

		class Holder {
			TextView guidetv;
			LinearLayout guidelayout;
		}
	}

	private void getGuidelist() {
		for (int i = 0; i < guide.length; i++) {
			SubscibeGuideInfo info = new SubscibeGuideInfo();
			info.id = i + 1;
			info.guide = guide[i];
			info.isSelect = false;
			guidelist.add(info);
		}
		guidelist.get(1).isSelect = true;
		rightAdapter = new RightAdapter();
		rightListView.setAdapter(rightAdapter);
		rightListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				for (int i = 0; i < guidelist.size(); i++) {
					guidelist.get(i).isSelect = false;
				}
				guidelist.get(position).isSelect = true;
				rightAdapter.notifyDataSetChanged();
				id = guidelist.get(position).id;
				page = 1;
				read();
			}
		});
	}

	/**
	 * 测试数据
	 * 
	 * @Title: getSubcrilist
	 * @Description: TODO
	 * @param @param id
	 * @param @return
	 * @return List<SubscibeInfo>
	 */
	private List<SubscibeInfo> getSubcrilist(int id) {
		List<SubscibeInfo> list = new ArrayList<SubscibeInfo>();
		for (int i = 0; i < 10; i++) {
			SubscibeInfo info = new SubscibeInfo();
			info.id = i + 1;
			info.count = 15782;
			GameInfo game = new GameInfo();
//			game.typ123eid = "角色扮演  武侠  3D";
			info.info = game;
			list.add(info);
		}
		return list;
	}

}
