package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMContactManager;
import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.easemob.util.EMLog;
import com.easemob.util.HanziToPinyin;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.daompl.MLocationLinsener;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.User;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.hxpackage.Constant;
import com.inwhoop.gameproduct.hxpackage.UserDao;
import com.inwhoop.gameproduct.utils.DialogShowStyle;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录页面-dingwenlong
 * Created by Administrator on 2014/4/4.
 */
public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener, MLocationLinsener {
    public static final int CHANGE_PWD = 202;
    private EditText userName;
    private EditText passWord;
    private Button login;
    private TextView registered;
    private TextView forgetPassword;
    private RelativeLayout back;
    private DialogShowStyle dialogShowStyle;
    private String loginTag = "";

    private int changePwd;//如果从修改密码界面跳转而来
    private double latitude =0;
    private double longitude =0;

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        changePwd = getIntent().getIntExtra("changePwd", 0);

        initView();
        startLocation(this);
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            loginTag = bundle.getString("LoginTag");
        }
        back = (RelativeLayout) findViewById(R.id.login_activity_back);
        userName = (EditText) findViewById(R.id.login_activity_username);
        passWord = (EditText) findViewById(R.id.login_activity_password);
//        userName.setText("Ddddd");  //此俩行后面需要取消
//        passWord.setText("123456");

        login = (Button) findViewById(R.id.login_activity_login);
        registered = (TextView) findViewById(R.id.login_activity_registered);
        forgetPassword = (TextView) findViewById(R.id.login_activity_forget_password);

        back.setOnClickListener(this);
        login.setOnClickListener(this);
        registered.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
        passWord.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_NULL:
                        break;
                    case EditorInfo.IME_ACTION_SEND:
                        break;
                    case EditorInfo.IME_ACTION_DONE:
                        break;
                    case EditorInfo.IME_ACTION_GO:
                        gotoLogin();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_activity_back:
                if (changePwd == CHANGE_PWD)
                    MainActivity.old_pos = 0;
                MyApplication.isLoginBack = true;
                finish();
                break;
            case R.id.login_activity_login:
                gotoLogin();
//                loginHX();
                break;
            case R.id.login_activity_registered:
                startActivity(new Intent(LoginActivity.this, RegisteredActivity.class));
                break;
            case R.id.login_activity_forget_password:
                break;
        }
    }

    private void gotoLogin() {
        dialogShowStyle = new DialogShowStyle(LoginActivity.this, getResources().getString(R.string.logining));
        UserInfo userInfo = new UserInfo();
        userInfo.username = userName.getText().toString();
        userInfo.password = passWord.getText().toString();
        if (userInfo.username.equals("")) {
            Toast.makeText(LoginActivity.this, this.getString(R.string.noemail), Toast.LENGTH_SHORT).show();
        } else if (userInfo.password.equals("")) {
            Toast.makeText(LoginActivity.this, this.getString(R.string.registered_pwd_toast), Toast.LENGTH_SHORT).show();
        } else {
            dialogShowStyle.dialogShow();
            userInfo.xlat=""+latitude;
            userInfo.ylong=""+longitude;
            userLogin(userInfo);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (changePwd == CHANGE_PWD)
                MainActivity.old_pos = 0;
            MyApplication.isLoginBack = true;
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void userLogin(final UserInfo userInfo) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getLoginData(userInfo);
                    msg.what = MyApplication.SUCCESS;
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    JsonResultBean jb = (JsonResultBean) msg.obj;
                    if (jb.isOk) {
                        UserInfo user = (UserInfo) jb.oneBean();
                        UserInfoUtil.rememberUserInfo(mContext, user);
                        MyApplication.my_uid = user.id;
                        loginHX(user.huanxinpwd);
                    } else
                        showToast(jb.msg);
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void loginHX(final String hxPwd) {
        dialogShowStyle = new DialogShowStyle(LoginActivity.this, getResources().getString(R.string.logining));
        dialogShowStyle.dialogShow();

        EMChatManager.getInstance().login(userName.getText().toString(), hxPwd, new EMCallBack() {

                @Override
            public void onSuccess() {
                //umeng自定义事件，开发者可以把这个删掉
//                loginSuccess2Umeng(start);

//                if (!progressShow) {
//                    return;
//                }
                // 登陆成功，保存用户名密码
                MyApplication.getInstance().setUserName(userName.getText().toString());
                MyApplication.getInstance().setPassword(hxPwd);
                runOnUiThread(new Runnable() {
                    public void run() {
//                        pd.setMessage(getString(R.string.list_is_for));
                    }
                });
                try {
                    // ** 第一次登录或者之前logout后再登录，加载所有本地群和回话
                    // ** manually load all local groups and
                    // conversations in case we are auto login
                    EMGroupManager.getInstance().loadAllGroups();
                    EMChatManager.getInstance().loadAllConversations();
                    //处理好友和群组
                    processContactsAndGroups();
                } catch (Exception e) {
                    e.printStackTrace();
                    //取好友或者群聊失败，不让进入主页面
                    runOnUiThread(new Runnable() {
                        public void run() {
//                            pd.dismiss();
                            dialogShowStyle.dialogDismiss();
                            MyApplication.getInstance().logout(null);
                            Toast.makeText(getApplicationContext(), R.string.login_failure_failed, Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }
                //更新当前用户的nickname 此方法的作用是在ios离线推送时能够显示用户nick
                boolean updatenick = EMChatManager.getInstance().updateCurrentUserNick(MyApplication.currentUserNick.trim());
                if (!updatenick) {
                    Log.e("LoginActivity", "update current user nick fail");
                }
                if (!LoginActivity.this.isFinishing())
//                    pd.dismiss();
                    dialogShowStyle.dialogDismiss();
                // 进入主页面
                if (loginTag.equals(""))
                    startActivity(new Intent(mContext, MainActivity.class));
                finish();
            }


            @Override
            public void onProgress(int progress, String status) {
            }

            @Override
            public void onError(final int code, final String message) {
//                loginFailure2Umeng(start,code,message);
//                if (!progressShow) {
//                    return;
//                }
                runOnUiThread(new Runnable() {
                    public void run() {
//                        pd.dismiss();
                        dialogShowStyle.dialogDismiss();
                        Toast.makeText(getApplicationContext(), getString(R.string.Login_failed) + message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void processContactsAndGroups() throws EaseMobException {
        // demo中简单的处理成每次登陆都去获取好友username，开发者自己根据情况而定
        List<String> usernames = EMContactManager.getInstance().getContactUserNames();
        System.out.println("----------------" + usernames.toString());
        EMLog.d("roster", "contacts size: " + usernames.size());
        Map<String, User> userlist = new HashMap<String, User>();
        for (String username : usernames) {
            User user = new User();
            user.setUsername(username);
            setUserHearder(username, user);
            userlist.put(username, user);
        }
        // 添加user"申请与通知"
        User newFriends = new User();
        newFriends.setUsername(Constant.NEW_FRIENDS_USERNAME);
        String strChat = getResources().getString(R.string.Application_and_notify);
        newFriends.setNick(strChat);

        userlist.put(Constant.NEW_FRIENDS_USERNAME, newFriends);
        // 添加"群聊"
        User groupUser = new User();
        String strGroup = getResources().getString(R.string.group_chat);
        groupUser.setUsername(Constant.GROUP_USERNAME);
        groupUser.setNick(strGroup);
        groupUser.setHeader("");
        userlist.put(Constant.GROUP_USERNAME, groupUser);

        // 存入内存
        MyApplication.getInstance().setContactList(userlist);
        // 存入db
        UserDao dao = new UserDao(LoginActivity.this);
        List<User> users = new ArrayList<User>(userlist.values());
        dao.saveContactList(users);

        //获取黑名单列表
        List<String> blackList = EMContactManager.getInstance().getBlackListUsernamesFromServer();
        //保存黑名单
        EMContactManager.getInstance().saveBlackList(blackList);

        // 获取群聊列表(群聊里只有groupid和groupname等简单信息，不包含members),sdk会把群组存入到内存和db中
        EMGroupManager.getInstance().getGroupsFromServer();
    }

    /**
     * 设置hearder属性，方便通讯中对联系人按header分类显示，以及通过右侧ABCD...字母栏快速定位联系人
     *
     * @param username
     * @param user
     */
    protected void setUserHearder(String username, User user) {
        String headerName = null;
        if (!TextUtils.isEmpty(user.getNick())) {
            headerName = user.getNick();
        } else {
            headerName = user.getUsername();
        }
        if (username.equals(Constant.NEW_FRIENDS_USERNAME)) {
            user.setHeader("");
        } else if (Character.isDigit(headerName.charAt(0))) {
            user.setHeader("#");
        } else {
            user.setHeader(HanziToPinyin.getInstance().get(headerName.substring(0, 1)).get(0).target.substring(0, 1).toUpperCase());
            char header = user.getHeader().toLowerCase().charAt(0);
            if (header < 'a' || header > 'z') {
                user.setHeader("#");
            }
        }
    }

    @Override
    public void mLocationFaile(String errorInfo) {
    }

    @Override
    public void mLocationXY(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}