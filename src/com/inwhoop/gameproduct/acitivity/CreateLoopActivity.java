package com.inwhoop.gameproduct.acitivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.CreateLoopInfo;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.RoundAngleImageView;
import org.jivesoftware.smackx.muc.MultiUserChat;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 创建圈子页面
 * @date 2014/5/10 14:55
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class CreateLoopActivity extends BaseActivity implements OnClickListener{

	private RelativeLayout headLayout;

	private RoundAngleImageView headImageView;

	private EditText  nameEditText, remarkEditText;

	private LocationClient locationClient = null;

	private Button subButton = null;
	
	private String name,remark;
	
	private double xlat,xlong;
	
	private int CAMERA = 1001; // 调用相机的返回码

	private int PICTURE = 1002; // 调用相册的返回码

//	private String picpath = "";   //用父类方法，这个去掉，换成父类传回的bitmap
    private Bitmap headBmp=null;

	private PopupWindow popupWindow = null;
	
	private CreateLoopInfo cinfo = null;
	
	private MultiUserChat userChat = null;

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
						| WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.create_loop_activity);
		mContext = CreateLoopActivity.this;
		init();
	}

	@Override
	public void init() {
		super.init();
		setStringTitle("创建圈子");
		setHeadLeftButton(R.drawable.back);
		headLayout = (RelativeLayout) findViewById(R.id.person_info_activity_head_img_rela);
		headImageView = (RoundAngleImageView) findViewById(R.id.person_info_activity_head_img);
//		addressTextView = (EditText) findViewById(R.id.address);
		nameEditText = (EditText) findViewById(R.id.editname);
		remarkEditText = (EditText) findViewById(R.id.editremark);
		subButton = (Button) findViewById(R.id.createbtn);
		subButton.setOnClickListener(this);
		headLayout.setOnClickListener(this);
		locationClient = new LocationClient(this); // 设置定位条件
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setAddrType("detail");
		option.setCoorType("gcj02");
		option.setScanSpan(1000000);
		option.disableCache(true);// 禁止启用缓存定位
		option.setPoiNumber(5); // 最多返回POI个数
		option.setPoiDistance(1000); // poi查询距离
		option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
		locationClient.setLocOption(option);
		locationClient.registerLocationListener(new MyLocationListenner());
		locationClient.start();
		locationClient.requestLocation();
//		initPopwindow();
	}

	private class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null)
				return;
			xlat = location.getLatitude();
			xlong = location.getLongitude();
		}

		@Override
		public void onReceivePoi(BDLocation poilocation) {

		}

	}

	private void submit() {
		String msg = checkMsg();
		if("".equals(msg)){
			showProgressDialog("等等哦！圈子正在创建中...");
            progressDialog.setCancelable(false);
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {	
						CreateLoopInfo info = new CreateLoopInfo();
						info.userid = UserInfoUtil.getUserInfo(mContext).id;
						info.circlename = name;
//						info.circleheadphoto = ""+Utils.imgToBase64(Utils.compressImage(picpath));
                        info.circleheadphoto = ""+Utils.imgToBase64(headBmp);
						info.circleremark = remark;
						info.xlat = xlat;
						info.ylong = xlong;
						msg.obj = JsonUtils.createLoop(info);
						msg.what = MyApplication.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = MyApplication.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}else{
			showToast(msg);
		}
	}

    private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("创建失败，请稍后再试~~");
				break;

			case MyApplication.READ_SUCCESS:
                JsonResultBean jrb = (JsonResultBean) msg.obj;
                if (jrb.isOk)
                    createOk((CreateLoopInfo) jrb.oneBean());
                showToast(jrb.msg);
                break;
            case RESULT_CUT_IMG:
                Bitmap bmp= (Bitmap) msg.obj;
                headImageView.setImageBitmap(bmp);
                headBmp=bmp;
                break;
			default:
				break;
			}

		};
	};

    private void createOk(CreateLoopInfo cinfo){
        Bundle bundle = new Bundle();
        bundle.putSerializable("info", cinfo);
        if (null != cinfo)
            Act.toAct(mContext, CreateLoopAfterActivity.class, bundle);
        finish();
    }

//	private void creatRoom(){
//        showProgressDialog("等等哦！圈子正在创建中...");
//        progressDialog.setCancelable(false);
//        new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				Message msg = new Message();
//				try{
//					userChat = XmppConnectionUtil.getInstance().createRoom(UserInfoUtil.getUserInfo(mContext).userid, ""+cinfo.id, "");
//					msg.what = MyApplication.READ_SUCCESS;
//				}catch(Exception e){
//					msg.what = MyApplication.READ_FAIL;
//				}
//				creatHandler.sendMessage(msg);
//			}
//		}).start();
//    }
//
//    private Handler creatHandler = new Handler(){
//    	public void handleMessage(android.os.Message msg) {
//            dismissProgressDialog();
//    		switch (msg.what) {
//			case  MyApplication.READ_SUCCESS:
//				if(null!=userChat){
//					ClientGroupServer.getInstance(mContext).addMultiuserchat(""+cinfo.id, cinfo.circlename,userChat);
//					createOk();
//				}else{
//					showToast("创建失败，请稍后再试~~");
//					delete();
//				}
//				break;
//
//			case MyApplication.READ_FAIL:
//				showToast("创建失败，请稍后再试~~");
//				delete();
//				break;
//
//			default:
//				break;
//			}
//    	};
//    };
//
//    private void delete(){
//    	new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				try{
//					JsonUtils.delteOpenfire(MyApplication.HOST+MyApplication.DELETE_CIRECLE, "circleid", ""+cinfo.id);
//				}catch(Exception e){
//
//				}
//			}
//		}).start();
//    }

	
	private String checkMsg(){
		String msg = "";
		name = nameEditText.getText().toString().trim();
		remark = remarkEditText.getText().toString().trim();
        if(null==headBmp){
            msg = "请设置圈子头像";
        }else if("".equals(name)){
			msg = "圈子名称不能为空";
		}
		else if("".equals(remark)){
			msg = "圈子备注不能为空";
		}
		return msg;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.createbtn:
			submit();
			break;
			
		case R.id.person_info_activity_head_img_rela:
            showCameraDialog(handler,true,200,200,true);
			break;

		default:
			break;
		}
	}
	
}