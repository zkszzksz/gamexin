package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GamelistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: GameCollectListActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 游戏收藏 ,必须登录后跳转而来
 * @date 2014-4-19 下午1:24:30
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GameCollectListActivity extends BaseActivity implements IXListViewListener {

    private XListView listView = null;

    private List<GameInfo> onePageList = new ArrayList<GameInfo>(); // 游戏信息

    private GamelistAdapter adapter = null; // listview的adapter
    private UserInfo userinfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collectgame_list_layout);
        mContext = GameCollectListActivity.this;
        userinfo = UserInfoUtil.getUserInfo(mContext);
        init();
    }

    @Override
    public void init() {
        super.init();
        setTitle(R.string.my_collect);
        setHeadLeftButton(R.drawable.back);
        listView = (XListView) findViewById(R.id.gamelistview);
        listView.setXListViewListener(this);
        listView.setPullLoadEnable(true);
        adapter = new GamelistAdapter(mContext, new ArrayList<GameInfo>());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0)return;
                Bundle b = new Bundle();
                b.putSerializable("bean", adapter.getAll().get(position-1));
                Act.toAct(mContext, GameInfosActivity.class, b);
            }
        });
        showProgressDialog("正在读取数据...");
        read();
    }
    
    private void read(){
         new Thread(new Runnable() {

             @Override
             public void run() {
                 Message msg = new Message();
                 try {
                     onePageList = JsonUtils.getGameByCollect("-1", userinfo.id);
                     msg.what = MyApplication.REFRESH_SUCCESS;
                 } catch (Exception e) {
                     msg.what = MyApplication.READ_FAIL;
                 }
                 addhandler.sendMessage(msg);
             }
         }).start();
    }

    @Override
    public void onRefresh() {
    	read();
    }

    @Override
    public void onLoadMore() {
        showProgressDialog("");
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    List<GameInfo> xxlist = adapter.getAll();
                    if (xxlist.size() > 0)
                        onePageList = JsonUtils.getGameByCollect(
                                xxlist.get(xxlist.size() - 1).gameid + "", userinfo.id);
                    msg.what = MyApplication.LOAD_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                addhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler addhandler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            listView.stopRefresh();
            listView.stopLoadMore();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    MyToast.showShort(mContext,"数据异常");
                    listView.setPullLoadEnable(false);
                    break;
                case MyApplication.REFRESH_SUCCESS:
                	if (onePageList.size() <= 9)
                        listView.setPullLoadEnable(false);
                    else
                        listView.setPullLoadEnable(true);

                    if (onePageList.size() == 0) {
                        MyToast.showShort(mContext, "暂无数据");
                        return;
                    }
                    adapter.getAll().clear();
                    adapter.add(onePageList);
                    onePageList.clear();
                    adapter.notifyDataSetChanged();
                    break;
                case MyApplication.LOAD_SUCCESS:
                    if (onePageList.size() <= 9)
                        listView.setPullLoadEnable(false);
                    adapter.add(onePageList);
                    onePageList.clear();
                    adapter.notifyDataSetChanged();
                    break;

                default:
                    break;
            }
        }

        ;
    };

}
