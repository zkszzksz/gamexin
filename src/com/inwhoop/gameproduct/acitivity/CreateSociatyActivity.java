package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.HorizontalListView;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Describe: TODO 创建公会 * * * ****** Created by ZK ********
 * @Date: 2014/06/12 15:44
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class CreateSociatyActivity extends BaseActivity implements
		View.OnClickListener {
	public static final int SEARCH_GAME_RESULT = 201;// 查询游戏返回

	private ImageView headIV;
	private EditText sociatyName, sociaty_jianjie_ET;
	private SimpleAdapter adapter;
	private List<Map<String, Object>> addGameList = new ArrayList<Map<String, Object>>(); // 保存的游戏，注意取数据时不要最后个
	private HorizontalListView listview;
	private Bitmap headBmp = null; // 保存的头像bitmap
	private boolean isFourGame = false;// 表示是不是已经有了4个游戏

	private SociatyBean sbean = null;;

	private MultiUserChat userChat = null;

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_sociaty);
		mContext = CreateSociatyActivity.this;

		initData();
	}

	private void initData() {
		init();
		setHeadLeftButton(R.drawable.back);
		setTitle(R.string.create_sociaty);

		headIV = (ImageView) findViewById(R.id.head_img);
		headIV.setOnClickListener(this);
		sociatyName = (EditText) findViewById(R.id.sociaty_name);
		sociaty_jianjie_ET = (EditText) findViewById(R.id.sociaty_jianjie_ET);
		findViewById(R.id.createbtn).setOnClickListener(this);

		addGameList.add(addResMap());
		setListViewData();
	}

	private void setListViewData() {
		listview = (HorizontalListView) findViewById(R.id.horizontal_listView);
		adapter = new SimpleAdapter(mContext, addGameList,
				R.layout.view_item_add_sociaty_game, new String[] { "img" },
				new int[] { R.id.img });
		adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
			@Override
			public boolean setViewValue(final View view, Object data,
					String textRepresentation) {
				if (view instanceof ImageView && data instanceof String) {
					ImageView iv = (ImageView) view;
					BitmapManager.INSTANCE.loadBitmap("" + data, iv,
							R.drawable.loading, true);
					return true;
				} else if (view instanceof ImageView && data instanceof Integer) {
					final ImageView iv = (ImageView) view;
					iv.setImageResource(R.drawable.add_bg_jianju);
					return true;
				}
				return false;
			}
		});
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Map<String, Object> bean = addGameList.get(position);
				if (null != bean.get("id")) {
					MyToast.showShort(mContext, "delete");
					if (addGameList.size() == 4
							&& null != addGameList.get(addGameList.size() - 1)
									.get("id")) {
						addGameList.remove(position);// 去掉加号图片
						addGameList.add(addResMap()); // 如果原先有4项，但是全部是游戏，则删后需要添加“+”图片
					} else {
						addGameList.remove(position);// 去掉加号图片
					}
					adapter.notifyDataSetChanged();
				} else {
					if (addGameList.size() >= 5) {
						MyToast.showShort(mContext, "入驻游戏数量不能超过4项");
						return;
					}
					addGamelogo();
				}
			}
		});
	}

	// TODO 跳转到搜索页面查询游戏
	private void addGamelogo() {
		Intent intent = new Intent(mContext, SearchGameActivity.class);
		Bundle b = new Bundle();
		b.putInt("tag", SEARCH_GAME_RESULT);
		intent.putExtras(b);
		startActivityForResult(intent, SEARCH_GAME_RESULT);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == SEARCH_GAME_RESULT
				&& resultCode == Activity.RESULT_OK && null != data) { // 搜索游戏后返回
			GameInfo bean = (GameInfo) data.getExtras().getSerializable("bean");
			for (int i = 0; i < addGameList.size() - 1; i++) {
				if (bean.gameid == (Integer) addGameList.get(i).get("id")) {
					MyToast.showShort(mContext, "相同游戏不能重复添加");
					return;
				}
			}
			addGameList.remove(addGameList.size() - 1);// 去掉加号图片
			addGameList.add(addImgMap(bean));
			if (addGameList.size() < 4) // 已有4个游戏的话不能添加更多
				addGameList.add(addResMap()); // 增加加号图片
			else
				isFourGame = true;
			adapter.notifyDataSetChanged();
		}
	}

	// TODO 游戏对象数据存入addGamelist
	private Map<String, Object> addImgMap(GameInfo bean) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("img", bean.gameicon);
		map.put("id", bean.gameid);// 保存id用于上传给服务器

		return map;
	}

	// 添加“+”图片
	private Map<String, Object> addResMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("img", R.drawable.loop_head_add_icon);
		return map;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.createbtn:
			SociatyBean upBean = checkSociaty();
			if (null != upBean) {
				createSociaty(upBean);
			}
			break;
		case R.id.head_img: // 头像，调用父类的照片方法
			showCameraDialog(handler, true, 200, 200, true);
			break;
		}
	}

	// TODO 调接口创建公会
	private void createSociaty(final SociatyBean upBean) {
		showProgressDialog("");
		progressDialog.setCancelable(false);
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.createSociaty(upBean);
					msg.what = MyApplication.SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	// TODO 检查并设置公会信息对象
	private SociatyBean checkSociaty() {
		SociatyBean bean = null;
		String msg = "";
		String ssName = sociatyName.getText().toString().trim();
		if (null == headBmp) {
			msg = "公会logo不要空哦~";
		} else if (ssName.equals("")) {
			msg = "公会名称不要空哦~";
		} else if (ssName.length() > 10) {
			msg = "公会名称过长";
		} else {
			bean = new SociatyBean();
			bean.guildname = ssName;
			bean.guildremark = sociaty_jianjie_ET.getText().toString();
			bean.userid = UserInfoUtil.getUserInfo(mContext).id;
			GameInfo gameBean = new GameInfo();
			for (int i = 0; (i < addGameList.size() - 1)
					|| (i < addGameList.size() && addGameList.size() == 4 && isFourGame); i++) {
				Map<String, Object> xx = addGameList.get(i);
				gameBean.gameid = (Integer) xx.get("id");
				bean.gamemsg.add(gameBean);

				bean.gameids .add( gameBean.gameid + "");
			}

			bean.gamemsg.clear();
			LogUtil.i("bean.gameids:" + bean.gameids);

			bean.guildheadphoto = ""
					+ Utils.imgToBase64(Utils.compressImage(headBmp));

			return bean;
		}
		MyToast.showShort(mContext, msg);
		return bean;
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case RESULT_IMG: // 接收相机返回图片路径 ,这里无用，用的裁剪
				headIV.setImageBitmap(Utils.compressImage("" + msg.obj));
				break;
			case RESULT_CUT_IMG: // 裁剪
				headBmp = Utils.compressImage((Bitmap) msg.obj);
				headIV.setImageBitmap(headBmp);
				break;
			case MyApplication.SUCCESS:
				JsonResultBean jrb = (JsonResultBean) msg.obj;
				 if(jrb.isOk){
					 sbean  = (SociatyBean) jrb.oneBean();
                     Bundle b = new Bundle();
					b.putSerializable("bean", sbean);
                     if (null != sbean)
                         Act.toAct(mContext, CreateSociatyAfterActivity.class, b);
					finish();
//					 creatRoom();
				 }
                showToast("" + jrb.msg);
                break;
			case MyApplication.FAIL:
				MyToast.showShort(mContext, "数据异常");
				break;
			}
		}
	};

//	private void creatRoom() {
//        showProgressDialog("公会正在创建中...");
//        progressDialog.setCancelable(false);
//        new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				Message msg = new Message();
//				try {
//					userChat = XmppConnectionUtil.getInstance().createRoom(
//							UserInfoUtil.getUserInfo(mContext).userid,
//							"" + sbean.id, "");
//					msg.what = MyApplication.READ_SUCCESS;
//				} catch (Exception e) {
//					msg.what = MyApplication.READ_FAIL;
//				}
//				creatHandler.sendMessage(msg);
//			}
//		}).start();
//	}
//
//	@SuppressLint("HandlerLeak")
//	private Handler creatHandler = new Handler() {
//		public void handleMessage(android.os.Message msg) {
//            dismissProgressDialog();
//            switch (msg.what) {
//			case MyApplication.READ_SUCCESS:
//				if (null != userChat) {
//					ClientGroupServer.getInstance(mContext).addMultiuserchat(
//							"" + sbean.id, sbean.guildname,userChat);
//				12	MyToast.showShort(mContext, "创建成功O(∩_∩)O~~");
//				32	Bundle b = new Bundle();
//				123	b.putSerializable("bean", sbean);
//				123	Act.toAct(mContext, CreateSociatyAfterActivity.class, b);
//				123	finish();
//				} else {
//					delete();
//					showToast("创建失败，请稍后再试~~");
//				}
//				break;
//			case MyApplication.READ_FAIL:
//				delete();
//				showToast("创建失败，请稍后再试~~");
//				break;
//
//			default:
//				break;
//			}
//		};
//	};
//
//	private void delete(){
//    	new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				try{
//					JsonUtils.delteOpenfire(MyApplication.HOST+MyApplication.DELETE_SOCIATY, "guildid", ""+sbean.id);
//				}catch(Exception e){
//
//				}
//			}
//		}).start();
//    }

}
