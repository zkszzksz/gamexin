package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import cn.sharesdk.framework.ShareSDK;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.LoopListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.LoopInfoBean;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.CustomList;
import com.inwhoop.gameproduct.view.PullToRefreshView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO  加入圈子（查询）  界面,根据act_tag的不同而判断什么界面
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/20 20:01
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SearchLoopActivity extends BaseActivity implements View.OnClickListener,
        PullToRefreshView.OnHeaderRefreshListener, PullToRefreshView.OnFooterRefreshListener {

    private static final char SEARCH_SUCCESS = 0x02;
    private static final char RECOMMEND_SUCCESS = 0x03;
    private static final char NEAR_SUCCESS = 0x04;

    private static final char TUIJIAN = 0x10;
    private static final char FUJIN = 0x11;
    private static final char SEARCH = 0x12;
    public static final int TAG_ACT_LOOP = 101;
    public static final int TAG_ACT_SOCIATY = 102;
    private char tag = TUIJIAN;//标识是自己搜索/推荐圈子/附近圈子

    private PullToRefreshView my_pull_view;  //刷新组件

    private RadioButton leftbtn, rightBtn;

    private List<LoopInfoBean> searchedList = new ArrayList<LoopInfoBean>();    //搜索后的圈子集合
    private List<LoopInfoBean> recommendList = new ArrayList<LoopInfoBean>();   //推荐的圈子集合
    private List<LoopInfoBean> nearList = new ArrayList<LoopInfoBean>();         //附近的圈子集合
    private List<LoopInfoBean> newList = new ArrayList<LoopInfoBean>();            //查询的圈子集合
    private LoopListAdapter adapter;
    private EditText imputEt;
    private String searchStr = "";//上此搜索的关键字。点了查询按钮后保存的。用于上拉刷新
    private LocationClient locationClient = null;
    private double xlat, ylong;  //定位的经纬度吧

    private int act_tag; //标识从哪个界面跳转而来

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_loop_or_sociaty);
        ShareSDK.initSDK(this);
        mContext = SearchLoopActivity.this;
        act_tag = getIntent().getIntExtra("tag", TAG_ACT_LOOP);

        initData();
        initLocation();

        recommend("-1");
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);

        my_pull_view = (PullToRefreshView) findViewById(R.id.add_loop_pull_refresh_view);
        my_pull_view.setOnHeaderRefreshListener(this);
        my_pull_view.setOnFooterRefreshListener(this);
        my_pull_view.mHeaderView.setVisibility(View.INVISIBLE);  //可以看不到上下，但是能滑着看
//        my_pull_view.mFooterView.setVisibility(View.INVISIBLE);

        CustomList listView = (CustomList) findViewById(R.id.all_loop_listview);
        adapter = new LoopListAdapter(mContext, act_tag);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle b = new Bundle();
                b.putSerializable("bean", adapter.getItemBean(position));
                if (act_tag == TAG_ACT_LOOP) {
                    Act.toAct(mContext, AddLoopSureActivity.class, b);
                } else { //跳转到公会详情，确认是否加入公会的界面
                    b.putInt("act_tag", SociatySureActivity.ACT_TAG_ADD_SOCIATY);
                    Act.toAct(mContext, SociatySureActivity.class, b);
                }
            }
        });

        findViewById(R.id.search_btn).setOnClickListener(this);
        imputEt = (EditText) findViewById(R.id.import_ET);
        imputEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH)
                    search();
                return true;
            }
        });
        leftbtn = (RadioButton) findViewById(R.id.leftBtn);
        leftbtn.setSelected(true);
        rightBtn = (RadioButton) findViewById(R.id.rightBtn);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.add_loop_radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.leftBtn:
                        leftbtn.setSelected(true);
                        rightBtn.setSelected(false);
                        if (tag == TUIJIAN && recommendList.size() > 0) {
                            return;
                        }
                        tag = TUIJIAN;
                        recommend("-1");
                        break;
                    case R.id.rightBtn:
                        leftbtn.setSelected(false);
                        rightBtn.setSelected(true);
                        if (tag == FUJIN && nearList.size() > 0) {
                            return;
                        }
                        tag = FUJIN;
                        near("-1", xlat, ylong);
                        break;
                }
            }
        });


        TextView pleaseImportTV = (TextView) findViewById(R.id.please_import_loop_name);
        TextView if_you_interestTV = (TextView) findViewById(R.id.if_you_interest);

        if (act_tag == TAG_ACT_LOOP) {
            setTitle(R.string.add_loop);
            pleaseImportTV.setText(R.string.please_import_loopname);
            if_you_interestTV.setText(R.string.interest_loop);
            leftbtn.setText(R.string.tuijiandequanzi);
            rightBtn.setText(R.string.fujindequanzi);
        } else {
            setTitle(R.string.add_sociaty);
            pleaseImportTV.setText(R.string.please_import_sociatyname);
            if_you_interestTV.setText(R.string.interest_sociaty);
            leftbtn.setText(R.string.tuijiandegonghui);
            rightBtn.setText(R.string.fujindegonghui);
            findViewById(R.id.add_loop_radioGroup).setVisibility(View.GONE);
        }

    }

    //TODO  定位
    private void initLocation() {
        locationClient = new LocationClient(this); // 设置定位条件
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setAddrType("detail");
        option.setCoorType("gcj02");
        option.setScanSpan(5000);
        option.disableCache(true);// 禁止启用缓存定位
        option.setPoiNumber(5); // 最多返回POI个数
        option.setPoiDistance(1000); // poi查询距离
        option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
        locationClient.setLocOption(option);
        locationClient.registerLocationListener(new MyLocationListenner());
        locationClient.start();
        locationClient.requestLocation();
    }

    private class MyLocationListenner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null)
                return;
            xlat = location.getLatitude();
            ylong = location.getLongitude();
        }

        @Override
        public void onReceivePoi(BDLocation poilocation) {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn:
                search();
                break;
        }
    }

    private void search() {
        String searchStr = imputEt.getText().toString().trim();
        if ("".equals(searchStr)) {
            MyToast.showShort(mContext, R.string.please_import);
            return;
        }
        tag = SEARCH;
        this.searchStr = searchStr;
        search("-1", searchStr);
    }


    @Override
    public void onHeaderRefresh(PullToRefreshView view) {
        my_pull_view.postDelayed(new Runnable() {
            @Override
            public void run() {
                //不做
                my_pull_view.onHeaderRefreshComplete();
            }
        }, 5);
    }

    @Override
    public void onFooterRefresh(PullToRefreshView view) {
        my_pull_view.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (tag == TUIJIAN) {
                        if (recommendList.size() >= 10) {
                            List<LoopInfoBean> mlist = adapter.getList();
                            newList = JsonUtils.recommendLoop(mlist.get(mlist.size() - 1).id + "");
                        }
                    } else if (tag == FUJIN) {
                        if (nearList.size() >= 10) {
                            List<LoopInfoBean> mlist = adapter.getList();
                            newList = JsonUtils.nearLoop(mlist.get(mlist.size() - 1).id + "", xlat + "", ylong + "");
                        }
                    } else if (tag == SEARCH) {
                        if (searchedList.size() >= 10)
                            newList = JsonUtils.searchLoop(
                                    searchedList.get(searchedList.size() - 1).id + "", searchStr);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e("exception,addLoopActivity:" + e.toString());
                }
                adapter.add(newList);
                newList.clear();
                my_pull_view.onFooterRefreshComplete();
            }
        }, 500);
    }

    //TODO  查询接口
    private void search(final String minId, final String searchStr) {
        showProgressDialog("");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (act_tag == TAG_ACT_LOOP) {
                        newList = JsonUtils.searchLoop(minId, searchStr);
                    } else {
                        msg.obj = JsonUtils.searchGuild(minId, searchStr);
                    }
                    for (LoopInfoBean bean:newList)
                        searchedList.add(bean);
                    msg.what = SEARCH_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    //TODO  推荐的圈子接口
    private void recommend(final String minId) {
        showProgressDialog("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (act_tag == TAG_ACT_LOOP) {
                        newList = JsonUtils.recommendLoop(minId);
                    } else {
                        newList = testAddList("测试推荐工会"); //测试数据
                    }
                    for (LoopInfoBean bean:newList)
                        recommendList.add(bean);
                    msg.what = RECOMMEND_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    //TODO 附近的圈子接口
    private void near(final String minId, final double xlat, final double ylong) {
        showProgressDialog("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (act_tag == TAG_ACT_LOOP) {
                        newList = JsonUtils.nearLoop(minId, xlat + "", ylong + "");
                    } else {

                    }
                    for (LoopInfoBean bean:newList)
                        nearList.add(bean);
                    msg.what = NEAR_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.FAIL:
                    MyToast.showShort(mContext, "数据异常");
                    break;

                case SEARCH_SUCCESS:
                    if (newList.size() > 0) {//如果有数据，隐藏下面的选择按钮和文字
                        findViewById(R.id.if_gone_layout).setVisibility(View.GONE);
                        addNewListData();
                        KeyBoard.HiddenInputPanel(mContext);
                    } else {
                        KeyBoard.showKeyBoard(imputEt);
                        MyToast.showShort(mContext, R.string.search_data_no_one);
                    }
                    break;
                case RECOMMEND_SUCCESS:
                case NEAR_SUCCESS:
                    addNewListData();
                    break;
            }
        }
    };

    private void addNewListData() {
        if (newList.size() == 0) {
            MyToast.showShort(mContext, R.string.search_data_no_one);
        }
        adapter.getList().clear();
        adapter.add(newList);
        newList.clear();
    }

    //TODO 测试数据，
    private List<LoopInfoBean> testAddList(String tag) {
        List<LoopInfoBean> list = new ArrayList<LoopInfoBean>();
        LoopInfoBean loopListInfo = null;
        for (int i = 0; i < 10; i++) {
            loopListInfo = new LoopInfoBean();
            loopListInfo.circleheadphoto = "http://img4.imgtn.bdimg.com/it/u=466308455,1349007185&fm=21&gp=0.jpg";
            loopListInfo.id = i;
            loopListInfo.circlename = tag + "";
            list.add(loopListInfo);
        }
        return list;
    }
}
