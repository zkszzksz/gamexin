package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.MypullListView;
import com.inwhoop.gameproduct.view.SelectLetterView;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Describe: TODO 转让会长
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/07/01 17:36
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MakeOverSociatyBossActivity extends BaseActivity implements
        View.OnClickListener,MypullListView.OnRefreshListener{
    private UserInfo userInfo = null;
    private List<FriendData> friendListData =new ArrayList<FriendData>();

    private SociatyBean sBean;

    private MyAdapter adapter;
    private String userids = "";
    private TextView nowletter;
    private SelectLetterView letterView;
    private MypullListView listView;
    private List<FriendData> list;

    @SuppressWarnings("deprecation")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite_members_activity);
        mContext = MakeOverSociatyBossActivity.this;
        SociatyBean sBean = (SociatyBean) getIntent().getSerializableExtra("bean");
        this.sBean = (null != sBean) ? sBean : new SociatyBean();
        userInfo = UserInfoUtil.getUserInfo(this);

        initView();
        getUserByGuild();
    }

    private void initView() {
        init();
        setHeadLeftButton(R.drawable.back);
        setStringTitle(getResources().getString(R.string.moke_over_boss));
//        setRightSecondBtText(getResources().getString(R.string.sure));
//        head_right_bt_2.setOnClickListener(this);

        nowletter = (TextView) findViewById(R.id.invite_members_activity_nowletter);
        letterView = (SelectLetterView) findViewById(R.id.invite_members_activity_letter);
        listView = (MypullListView) findViewById(R.id.invite_members_activity_listview);
        listView.setonRefreshListener(this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0) return;
                final FriendData f = adapter.getAll().get(position - 1);
                userids=f.userid;

                new AlertDialog.Builder(mContext)
                        .setTitle("确认转让给【" + f.nickname + "】会长?")
//                   .setMessage("message")
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sBean.userid = f.userid;
                                sBean.nickname = f.nickname;
                                transferGuild();
                            }
                        }).setNegativeButton("取消",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
    }

    private void read() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    list = getlist();
                    hhandler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler hhandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            dismissProgressDialog();
            listView.onRefreshComplete();
            letterView.setList(list);
            letterView.setPuListView(listView);
            letterView.setNowTextView(nowletter);
            adapter = new MyAdapter(mContext, list);
            listView.setAdapter(adapter);
            if (list.size()>0)
                letterView.setNoStr(list.get(0).letter);
        }
    };

    private List<FriendData> getlist() throws Exception {
        List<FriendData> list = new ArrayList<FriendData>();
        FriendData info = null;
        for (int i = 0; i < friendListData.size(); i++) {
            info = new FriendData();
            FriendData xxb = friendListData.get(i);
            info.userid = xxb.userid;
            info.userphoto = xxb.userphoto;
            info.nickname = xxb.nickname;
            if (!xxb.userid.equals(sBean.userid)) {
                list.add(info); //如果是会长，不放列表里
            }
        }
        List<FriendData> otherlist = new ArrayList<FriendData>();
        List<FriendData> pinlist = new ArrayList<FriendData>();
        for (int i = 0; i < list.size(); i++) {
            if (isChinese(list.get(i).nickname.charAt(0))) {
                pinlist.add(list.get(i));
            } else {
                otherlist.add(list.get(i));
            }
        }
        pinlist = getSortListFrompin(pinlist);
        otherlist = getSortListFrome(otherlist);
        list.clear();

        for (int i = 0; i < otherlist.size(); i++) {
            list.add(otherlist.get(i));
        }
        for (int i = 0; i < pinlist.size(); i++) {
            list.add(pinlist.get(i));
        }
        return list;
    }

    /**
     * 是否是汉字和字母
     * @Title: isChinese
     */
    public boolean isChinese(char a) {
        int v = (int) a;
        return (v >= 19968 && v <= 171941) || (v >= 65 && v <= 90) || (v >= 97 && v <= 122);
    }

    @Override
    public void onRefresh() {
        read();
    }

    /**
     * 对对非数字客户名进行排序
     * @throws net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination
     */
    private List<FriendData> getSortListFrompin(List<FriendData> list)
            throws BadHanyuPinyinOutputFormatCombination {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                try {
                    return getPinYin(arg0.nickname.toLowerCase()).compareTo(
                            getPinYin(arg1.nickname.toLowerCase()));
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = strTOup(getPinYin(list.get(i).nickname));
        }
        return list;
    }

    private String strTOup(String str) {
        return ("" + str.charAt(0)).toUpperCase();
    }

    //对对非数字客户名进行排序
    private List<FriendData> getSortListFrome(List<FriendData> list) {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                return arg0.nickname.compareTo(arg1.nickname);
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = "#";
        }
        return list;
    }

    /**
     * 将中文转换成拼音
     * @param -汉字
     */
    public static String getPinYin(String zhongwen)
            throws BadHanyuPinyinOutputFormatCombination {
        String zhongWenPinYin = "";
        char[] chars = zhongwen.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            String[] pinYin = PinyinHelper.toHanyuPinyinStringArray(chars[i],
                    getDefaultOutputFormat());
            // 当转换不是中文字符时,返回null
            if (pinYin != null) {
                zhongWenPinYin += pinYin[0];
            } else {
                zhongWenPinYin += chars[i];
            }
        }
        return zhongWenPinYin + "-" + zhongwen;
    }

    // 输出格式
    public static HanyuPinyinOutputFormat getDefaultOutputFormat() {
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);// 小写
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);// 没有音调数字
        format.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);// u显示
        return format;
    }

    @Override
    public void scrollitem(int first) {
        if (null != letterView && null != list && !letterView.isFlag() && list.size()>0) {
            letterView.setNoStr(list.get(first).letter);
        }
    }

    public void getUserByGuild() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    List<FriendData> xx = JsonUtils.getUserByGuildMem(sBean.guildid,"not_loop");
                    if (null != xx && xx.size() > 0) {
                        msg.what = MyApplication.SUCCESS;
                        msg.obj = xx;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    List<FriendData> list = (List<FriendData>) msg.obj;
                    for (FriendData friendData : list) {
                        friendListData.add(friendData);
                    }
                    read();
                    break;
                case MyApplication.FAIL:
                    MyToast.showShort(mContext,
                            getResources().getString(R.string.no_data));
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(mContext,
                            getResources().getString(R.string.getdata_fail),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_right_bt_2:
                invate();
                break;

            default:
                break;
        }
    }

    private void invate() {
        userids = "";
        for (int i = 0; i < adapter.getAll().size(); i++) {
            if (adapter.getAll().get(i).isselect) {
               LogUtil.i("邀请的ID为="+adapter.getAll().get(i).userid);
                userids = userids + adapter.getAll().get(i).userid + ",";
            }
        }
        if ("".equals(userids)) {
            showToast("请选择~~");
        } else {
            userids = userids.substring(0, userids.length() - 1);
            transferGuild();
        }
    }

    private void transferGuild() {
        showProgressDialog("");
        LogUtil.i("转让的ID为" + userids);
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try{
                    msg.obj = JsonUtils.transferGuild("" + sBean.guildid, userInfo.id, userids);
                    msg.what = MyApplication.READ_SUCCESS;
                }catch(Exception e){
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    MyToast.showShort(mContext,"网络错误,请稍后再试~~");
                    break;

                case MyApplication.READ_SUCCESS:
                   JsonResultBean jrb= (JsonResultBean) msg.obj;
                    if(jrb.isOk){
                        Bundle b = new Bundle();
                        sBean.userid = userids;
                        b.putSerializable("bean", sBean);
                        Intent intent = new Intent();
                        intent.putExtras(b);
                        setResult(300, intent);
//                        Act.toActClearTop(mContext,SociatySureActivity.class,b);
                        finish();
                    }
                    MyToast.showShort(mContext, "" + jrb.msg);

                    break;
            }
        }
    };
    class MyAdapter extends BaseAdapter {

        private List<FriendData> list;

        private LayoutInflater inflater;

        public MyAdapter(Context context, List<FriendData> list){
            this.list = list;
            inflater = LayoutInflater.from(context);
        }
        public List<FriendData> getAll() {
            return list;
        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup arg2) {
            Holder holder = null;
            if(convertView == null){
                holder = new Holder();
                convertView = inflater.inflate(R.layout.invite_member_item, null);
                holder.letterLinear = (LinearLayout) convertView.findViewById(R.id.invite_member_item_letter_linear);
                holder.letter = (TextView) convertView.findViewById(R.id.invite_member_item_letter);
                holder.photo = (ImageView) convertView.findViewById(R.id.invite_member_item_photos);
                holder.name = (TextView) convertView.findViewById(R.id.invite_member_item_name);
                holder.checkBox=(CheckBox)convertView.findViewById(R.id.invite_member_item_checkbox);

                holder.checkBox.setVisibility(View.GONE);//此界面不需要选择，直接item点击事件即可
                convertView.setTag(holder);
            }else{
                holder = (Holder) convertView.getTag();
            }
            if(position>=1){
                if(list.get(position).letter.equals(list.get(position-1).letter)){
                    holder.letterLinear.setVisibility(View.GONE);
                }else{
                    holder.letterLinear.setVisibility(View.VISIBLE);
                    holder.letter.setText(list.get(position).letter);
                }
            }else{
                holder.letterLinear.setVisibility(View.VISIBLE);
                holder.letter.setText(list.get(position).letter);
            }
            holder.checkBox.setTag("" + position);
            holder.checkBox
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton v,
                                                     boolean flag) {
                            Toast.makeText(mContext, "v:" + v.getTag() + flag,
                                    Toast.LENGTH_SHORT).show();
                            int pos = Integer.parseInt(v.getTag().toString());
                            list.get(pos).isselect = flag;
                        }
                    });
            BitmapManager.INSTANCE.loadBitmap(list.get(position).userphoto,
                    holder.photo, R.drawable.default_local, true);
            holder.name.setText(list.get(position).nickname);
//            LogUtil.i("getView的ID为"+list.get(position).userid);
//            LogUtil.i("getView的nickname为"+list.get(position).nickname);
            return convertView;
        }

        class Holder{
            TextView letter,name;
            public ImageView photo;
            public LinearLayout letterLinear;
            public CheckBox checkBox;
        }
    }
}
