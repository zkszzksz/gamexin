package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.MyToast;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO     更改个人资料中的昵称等子页面
 * @date 2014/5/10   12:47
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class ChangeInfoActivity extends BaseActivity implements View.OnClickListener {
    private EditText infoEdit;
    private TextView cancel;
    private String data = ""; //传过来的数据参数指示

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_info_activity);
        mContext = ChangeInfoActivity.this;
        String ss = getIntent().getStringExtra("changeInfo");
        data = null != ss ? ss : "";
        String content = getIntent().getStringExtra("content");
        String conStr = null != content ? content : "";

        initView();

        infoEdit.setText(conStr);
        infoEdit.setSelection(conStr.length());

        KeyBoard.showKeyBoard(infoEdit);
    }

    private void initView() {
        init();
        setStringTitle(setTitleShow());
        setHeadLeftButton(R.drawable.back);
        setRightSecondBtText(getResources().getString(R.string.save));
        getRightSecondBt().setOnClickListener(this);

        cancel = (TextView) findViewById(R.id.change_info_activity_cancel);
        infoEdit = (EditText) findViewById(R.id.change_info_activity_edit);
        infoEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_DONE)
                    saveOkClick();
                return true;
            }
        });
    }

    private String setTitleShow() {
        String str = "";
        if (data.equals("nick")) {
            str = (getResources().getString(R.string.change_nick));
        } else if (data.equals("address")) {
            str = (getResources().getString(R.string.change_address));
        } else if (data.equals("remark")) {
            str = (getResources().getString(R.string.person_state));
        }
        return str;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_right_second:    //完成才修改
                saveOkClick();
                break;
        }
    }

    private void saveOkClick() {
        if (data.equals("nick")) {
            String ss=infoEdit.getText().toString().trim();
            if (ss.length()>10){
                MyToast.showShort(mContext, "您的昵称太长啦，请重新输入！");
                return;
            } else if (ss.length()==0){
                MyToast.showShort(mContext,"您的昵称不能为空，请重新输入！");
                return;
            }

            Intent intent = new Intent();
            intent.putExtra("nick_info", ss);
            setResult(Activity.RESULT_OK, intent);
        } else if (data.equals("address")) {
            Intent intent = new Intent();
            intent.putExtra("address_info", infoEdit.getText().toString());
            setResult(Activity.RESULT_OK, intent);
        } else if (data.equals("remark")) {
            Intent intent = new Intent();
            intent.putExtra("remark_info", infoEdit.getText().toString());
            setResult(Activity.RESULT_OK, intent);
        }
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }
}