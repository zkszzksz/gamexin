package com.inwhoop.gameproduct.acitivity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.view.HackyViewPager;
import com.inwhoop.gameproduct.view.PhotoView;

/**
 * @Describe: TODO  显示图集    目前是游戏资讯.目前困难，代码里动态设置不了文字？
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/18 12:13
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class ShowIMGsActivity extends BaseActivity implements View.OnClickListener {

    private GameInfo gameInfo;

    private static final String ISLOCKED_ARG = "isLocked";

    private ViewPager mViewPager;
    private MenuItem menuLockItem;

    @Override //TODO  oncreate
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_imgs);
        mContext = ShowIMGsActivity.this;

        gameInfo = (GameInfo) getIntent().getSerializableExtra("bean");

        initData(savedInstanceState);
    }

    /**
     * TODO 初始化数据
     *
     * @param savedInstanceState
     */
    private void initData(Bundle savedInstanceState) {
        init();
        setStringTitle(gameInfo.gamename);
        getLeftBt().setOnClickListener(this);
        setHeadLeftButton(R.drawable.back);
        getLeftBt().setOnClickListener(this);

        mViewPager = (ViewPager) findViewById(R.id.show_imgs_viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter(gameInfo));

        if (savedInstanceState != null) {
            boolean isLocked = savedInstanceState.getBoolean(ISLOCKED_ARG, false);
            ((HackyViewPager) mViewPager).setLocked(isLocked);
        }
    }

    /**
     * TODO 切换适配器
     */
    class SamplePagerAdapter extends PagerAdapter {

        private int[] sDrawables = {R.drawable.home_banner_bg, R.drawable.home_banner_bg, R.drawable.home_banner_bg,
                R.drawable.home_banner_bg, R.drawable.home_banner_bg, R.drawable.home_banner_bg};

        private String[] uris;

        public SamplePagerAdapter(GameInfo gameInfo) {
            uris = new String[]{gameInfo.gamemainimage,
                    gameInfo.gamemainimage,
                    gameInfo.gamemainimage,
                    gameInfo.gamemainimage,
                    gameInfo.gamemainimage};
        }

        @Override
        public int getCount() {
//            return sDrawables.length;
            return uris.length;
        }


        private LinearLayout.LayoutParams LP_FF = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        private LinearLayout.LayoutParams LP_FW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        private LinearLayout.LayoutParams LP_WW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        @Override
        public View instantiateItem(ViewGroup container, int position) {

//            //聊天对白窗口需要滚动
//            RelativeLayout sv = new RelativeLayout(mContext);
//            sv.setLayoutParams(LP_FF);
//            LinearLayout layout = new LinearLayout(mContext);   //线性布局方式
//            layout.setOrientation(LinearLayout.VERTICAL); //控件对其方式为垂直排列
//            layout.setBackgroundColor(0xff00ffff);        //设置布局板的一个特殊颜色，这可以检验我们会话时候是否有地方颜色不正确！


            PhotoView photoView = new PhotoView(container.getContext());
//            photoView.setImageResource(sDrawables[position]);
//            photoView.setImageURI(uris[position]);//为何没有图片
//            photoView.setImageDrawable();
            photoView.setImageURIByBitmapManager(uris[position]);

            // Now just add 1PhotoView to ViewPager and return it
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


            ViewGroup.LayoutParams mLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
//            mLayoutParams.addRule(RelativeLayout.RIGHT_OF, ID_IMAGE_HEAD);
//            mLayoutParams.topMargin = 5;
//            mLayoutParams.setMargins(0, 200, 0, 0);

//            mLinearLayout.addView(mTextView, mLayoutParams);

            TextView tv = new TextView(container.getContext());    //普通聊天对话
//            //获应该是由其他程序提供
            tv.setText("我是伟大的测试数据: " + position);
//            tv.setGravity(Gravity.BOTTOM);
            tv.setTextColor(Color.BLUE);
//            tv.setLayoutParams(mLayoutParams);

            container.addView(tv,ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

//            tv.setBackgroundColor(Color.GRAY);
//            container.addView(tv);
//            container.addView(photoView);

//            //发送文件效果1，圆环进度条，也是ProgressBar默认的效果
//            setSendFile(layout, this, getCurrColor(), "我的照片.jpg");
//
//            //发送文件效果 2,矩行进度条，也是ProgressBar的风格设置成 style="?android:attr/progressBarStyleHorizontal"的效果
//            setSendFile2(layout, this, getCurrColor(), "我的照片.jpg");
//            for (int i = 0; i < 10; i++) {
//                setSendMsg(layout, this, getCurrColor(), i + "聊天内容在这里。。");
//            }
//            sv.addView(layout);   //把线性布局加入到ScrollView中
//            setContentView(sv);     //设置当前的页面为ScrollView


            return photoView;

        }

        class Holder {
            ImageView img, giftimg, actimg, codeimg;
            TextView title, type, state, count;
            RelativeLayout imgrela;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.viewpager_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menuLockItem = menu.findItem(R.id.menu_lock);
        toggleLockBtnTitle();
        menuLockItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                toggleViewPagerScrolling();
                toggleLockBtnTitle();
                return true;
            }
        });

        return super.onPrepareOptionsMenu(menu);
    }

    private void toggleViewPagerScrolling() {
        if (isViewPagerActive()) {
            ((HackyViewPager) mViewPager).toggleLock();
        }
    }

    private void toggleLockBtnTitle() {
        boolean isLocked = false;
        if (isViewPagerActive()) {
            isLocked = ((HackyViewPager) mViewPager).isLocked();
        }
        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
        if (menuLockItem != null) {
            menuLockItem.setTitle(title);
        }
    }

    private boolean isViewPagerActive() {
        return (mViewPager != null && mViewPager instanceof HackyViewPager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (isViewPagerActive()) {
            outState.putBoolean(ISLOCKED_ARG, ((HackyViewPager) mViewPager).isLocked());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_left:
                finish();
                break;

        }
    }
}
