package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import cn.sharesdk.framework.ShareSDK;
import com.baidu.location.LocationClient;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.SociatyListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.CustomList;
import com.inwhoop.gameproduct.view.PullToRefreshView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO  加入工会(查询公会)  界面,根据act_tag的不同而判断什么界面
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/20 20:01
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SearchSociatyActivity extends BaseActivity implements View.OnClickListener,
        PullToRefreshView.OnHeaderRefreshListener, PullToRefreshView.OnFooterRefreshListener {

    private static final char SEARCH_SUCCESS = 0x02;
    private static final char RECOMMEND_SUCCESS = 0x03;
    private static final char NEAR_SUCCESS = 0x04;

    private static final char TUIJIAN = 0x10;
    private static final char FUJIN = 0x11;
    private static final char SEARCH = 0x12;
    public static final int TAG_ACT_LOOP = 101;
    public static final int TAG_ACT_SOCIATY = 102;
    private char tag = TUIJIAN;//标识是自己搜索/推荐圈子/附近圈子

    private PullToRefreshView my_pull_view;  //刷新组件

//    private RadioButton leftbtn, rightBtn;

    private List<SociatyBean> searchedList = new ArrayList<SociatyBean>();    //搜索后的公会集合
    private List<SociatyBean> recommendList = new ArrayList<SociatyBean>();   //推荐的公会集合
    private List<SociatyBean> newList = new ArrayList<SociatyBean>();            //查询的公会集合
    private SociatyListAdapter adapter;
    private EditText imputEt;
    private String searchStr = "";//上此搜索的关键字。点了查询按钮后保存的。用于上拉刷新
    private LocationClient locationClient = null;
    private double xlat, ylong;  //定位的经纬度吧

    private int act_tag; //标识从哪个界面跳转而来

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_loop_or_sociaty);
        ShareSDK.initSDK(this);
        mContext = SearchSociatyActivity.this;
        act_tag = getIntent().getIntExtra("tag", TAG_ACT_LOOP);

        initData();

        recommend("-1");
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);

        my_pull_view = (PullToRefreshView) findViewById(R.id.add_loop_pull_refresh_view);
        my_pull_view.setOnHeaderRefreshListener(this);
        my_pull_view.setOnFooterRefreshListener(this);
        my_pull_view.mHeaderView.setVisibility(View.INVISIBLE);  //可以看不到上下，但是能滑着看
//        my_pull_view.mFooterView.setVisibility(View.INVISIBLE);

        CustomList listView = (CustomList) findViewById(R.id.all_loop_listview);
        adapter = new SociatyListAdapter(mContext);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle b = new Bundle();
                b.putSerializable("bean", adapter.getItemBean(position));
                if (act_tag == TAG_ACT_LOOP) {

                } else if (adapter.getItemBean(position).guildid != 0) { //跳转到公会详情，确认是否加入公会的界面
                    b.putInt("act_tag", SociatySureActivity.ACT_TAG_ADD_SOCIATY);
                    Act.toAct(mContext, SociatySureActivity.class, b);
                }
            }
        });

        findViewById(R.id.search_btn).setOnClickListener(this);
        imputEt = (EditText) findViewById(R.id.import_ET);
        imputEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH)
                    search();
                return true;
            }
        });

        TextView pleaseImportTV = (TextView) findViewById(R.id.please_import_loop_name);
        TextView if_you_interestTV = (TextView) findViewById(R.id.if_you_interest);

        if (act_tag == TAG_ACT_LOOP) {
        } else {
            setTitle(R.string.add_sociaty);
            pleaseImportTV.setText(R.string.please_import_sociatyname);
            if_you_interestTV.setText(R.string.interest_sociaty);
            findViewById(R.id.add_loop_radioGroup).setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn:
                search();
                break;
        }
    }

    private void search() {
        String searchStr = imputEt.getText().toString().trim();
        if ("".equals(searchStr)) {
            MyToast.showShort(mContext, R.string.please_import);
            return;
        }
        tag = SEARCH;
        this.searchStr = searchStr;
        search("-1", searchStr);
    }


    @Override
    public void onHeaderRefresh(PullToRefreshView view) {
        my_pull_view.postDelayed(new Runnable() {
            @Override
            public void run() {
                //不做
                my_pull_view.onHeaderRefreshComplete();
            }
        }, 5);
    }

    @Override
    public void onFooterRefresh(PullToRefreshView view) {
        my_pull_view.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (tag == TUIJIAN) {
                        if (recommendList.size() >= 10) {
                            List<SociatyBean> mlist = adapter.getList();
                            newList = JsonUtils.searchRecommendGuild(mlist.get(mlist.size() - 1).guildid + "");
                        }
                    } else if (tag == SEARCH) {
                        if (searchedList.size() >= 10)
                            newList = JsonUtils.searchGuild(
                                    searchedList.get(searchedList.size() - 1).guildid + "", searchStr);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e("exception,addSociatyActivity:" + e.toString());
                }
                adapter.add(newList);
                newList.clear();
                my_pull_view.onFooterRefreshComplete();
            }
        }, 500);
    }

    //TODO  查询接口
    private void search(final String minId, final String searchStr) {
        showProgressDialog("");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (act_tag == TAG_ACT_LOOP) {

                    } else {
                        newList = JsonUtils.searchGuild(minId, searchStr);
                    }
                    searchedList = newList;
                    msg.what = SEARCH_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    //TODO  推荐的公会接口
    private void recommend(final String minId) {
        showProgressDialog("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (act_tag == TAG_ACT_LOOP) {

                    } else {
//                        newList = testAddList("测试推荐工会"); //测试数据
                        newList = JsonUtils.searchRecommendGuild(minId);
                    }
                    recommendList = newList;
                    msg.what = RECOMMEND_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.FAIL:
                    MyToast.showShort(mContext, "数据异常");
                    break;

                case SEARCH_SUCCESS:
                    if (newList.size() > 0) {//如果有数据，隐藏下面的选择按钮和文字
                        findViewById(R.id.if_gone_layout).setVisibility(View.GONE);
                        addNewListData();
                        KeyBoard.HiddenInputPanel(mContext);
                    } else {
                        KeyBoard.showKeyBoard(imputEt);
                        MyToast.showShort(mContext, R.string.search_data_no_one);
                    }
                    break;
                case RECOMMEND_SUCCESS:
                case NEAR_SUCCESS:
                    addNewListData();
                    break;
            }
        }
    };

    private void addNewListData() {
        if (newList.size() == 0) {
            MyToast.showShort(mContext, R.string.search_data_no_one);
        }else {
        }
        adapter.getList().clear();
        adapter.add(newList);
        newList.clear();
    }

    //TODO 测试数据，
    private List<SociatyBean> testAddList(String tag) {
        List<SociatyBean> list = new ArrayList<SociatyBean>();
        SociatyBean loopListInfo = null;
        for (int i = 0; i < 10; i++) {
            loopListInfo = new SociatyBean();
            loopListInfo.guildheadphoto = "http://img4.imgtn.bdimg.com/it/u=466308455,1349007185&fm=21&gp=0.jpg";
            loopListInfo.guildid = i;
            loopListInfo.guildname = tag + "";
            list.add(loopListInfo);
        }
        return list;
    }
}
