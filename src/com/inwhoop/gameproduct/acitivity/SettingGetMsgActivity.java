package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.view.SlidButton;

/**
 * @Describe: TODO  推送设置
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/13 19:43
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SettingGetMsgActivity extends BaseActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_get_msg);
        mContext=SettingGetMsgActivity.this;

        initData();
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.tuisongshezhi);
        getLeftBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SlidButton msgBtn= (SlidButton) findViewById(R.id.tuisong_msg_btn);
        SlidButton lingBtn= (SlidButton) findViewById(R.id.tuisong_lingsheng_btn);
        SlidButton zhenBtn= (SlidButton) findViewById(R.id.tuisong_zhendong_btn);

        msgBtn.setOnChangedListener(new SlidButton.OnChangedListener() {
            @Override
            public void OnChanged(boolean checkState) {
                if (checkState){
                    Toast.makeText(mContext, "msgBtn打开了。。。", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext, "msgBtn关闭了。。。", Toast.LENGTH_SHORT).show();
                }
            }
        });
        lingBtn.setOnChangedListener(new SlidButton.OnChangedListener() {
            @Override
            public void OnChanged(boolean checkState) {
                if (checkState){
                    Toast.makeText(mContext, "lingBtn打开了。。。", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext, "lingBtn关闭了。。。", Toast.LENGTH_SHORT).show();
                }
            }
        });
        zhenBtn.setOnChangedListener(new SlidButton.OnChangedListener() {
            @Override
            public void OnChanged(boolean checkState) {
                if (checkState){
                    Toast.makeText(mContext, "zhenBtn打开了。。。", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext, "zhenBtn关闭了。。。", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }



}
