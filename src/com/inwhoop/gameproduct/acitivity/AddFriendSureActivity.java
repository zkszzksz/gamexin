package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;

/**
 * @Describe: TODO  确认添加好友
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/03 19:40
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class AddFriendSureActivity extends BaseActivity implements View.OnClickListener {
    private FriendData friendBean;
    private UserInfo user;
    private int tag;//标识从哪个界面跳转而来，是从验证消息界面过来的话是101
    
    private PopupWindow popupWindow = null;
    
    private String checkinfo = "";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend_sure);
        mContext=AddFriendSureActivity.this;
        friendBean= (FriendData) getIntent().getSerializableExtra("bean");
        this.tag=getIntent().getIntExtra("tag",0);
        user= UserInfoUtil.getUserInfo(mContext);

        initData();
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.yonghuziliao);

        View sureBtn = findViewById(R.id.sure_btn);
        sureBtn.setOnClickListener(this);
        if (tag==101)sureBtn.setVisibility(View.GONE);

        ImageView headImg = (ImageView) findViewById(R.id.person_info_activity_head_img);
        TextView nick = (TextView) findViewById(R.id.person_info_activity_nick);
        TextView gender = (TextView) findViewById(R.id.person_info_activity_gender);
        TextView birthday = (TextView) findViewById(R.id.person_info_activity_birthday);
        TextView location = (TextView) findViewById(R.id.person_info_activity_location);
        TextView personState = (TextView) findViewById(R.id.person_info_activity_person_state);

        BitmapManager.INSTANCE.loadBitmap(friendBean.userphoto,headImg,R.drawable.default_avatar,true);
        nick.setText(friendBean.nickname);
        gender.setText(friendBean.realSex());
        birthday.setText(friendBean.birthday);
        location.setText(friendBean.useraddress);
        personState.setText(friendBean.remark);
        initPopupwindow();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sure_btn:
            	showPopupwindow();
                break;
        }
    }

    //TODO  添加好友请求
    private void addFriendByUser(final String zid, final String bid,final String askremark) {
//        showProgressDialog("");
//        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (!"".equals(zid)){
                        msg.obj = JsonUtils.addFriendByUser(zid, bid ,askremark);
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = 110;
                    }
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.FAIL:
                    MyToast.showShort(mContext, "数据异常");
                    break;

                case MyApplication.SUCCESS:
                    KeyBoard.HiddenInputPanel(mContext);

                    JsonResultBean jrb = (JsonResultBean) msg.obj;
                    MyToast.showShort(mContext,jrb.msg+"");
                    break;
                case 110:
                    KeyBoard.HiddenInputPanel(mContext);
                    break;
            }
        }
    };
    
    @SuppressWarnings("deprecation")
	private void initPopupwindow(){
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.check_information_popupwindow, null);
		popupWindow = new PopupWindow(view, getWindowManager().getDefaultDisplay().getWidth()/4*3,
				LayoutParams.WRAP_CONTENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
//		popupWindow.setBackgroundDrawable(new BitmapDrawable()); //取消点击外部消失
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		final EditText checkEditText = (EditText) view.findViewById(R.id.checkinfo);
		Button cancel = (Button) view.findViewById(R.id.cancle);
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(null!=popupWindow){
					popupWindow.dismiss();
                    checkEditText.setText("");
                    addFriendByUser("","","");
                }
			}
		});
		Button sendButton = (Button) view.findViewById(R.id.send);
		sendButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(null!=popupWindow){
					popupWindow.dismiss();
				}
                showProgressDialog("");
                progressDialog.setCancelable(false);
                addFriendByUser(user.id,friendBean.id,checkEditText.getText().toString());
                checkEditText.setText("");
                KeyBoard.HiddenInputPanel(mContext);
            }
		});
		
    }
    
    private void showPopupwindow(){
    	popupWindow.showAtLocation(LayoutInflater.from(mContext).inflate(
				R.layout.activity_add_friend_sure, null), Gravity.CENTER, 0, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        KeyBoard.HiddenInputPanel(mContext);
        LogUtil.i("onResume");
    }
}
