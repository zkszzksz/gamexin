package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.entity.DetailsGift;
import com.inwhoop.gameproduct.entity.Messages;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;

import java.io.UnsupportedEncodingException;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 礼包详情页面
 * @date 2014/4/19 14:12
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GiftBagDetailsActivity extends BaseActivity implements
        View.OnClickListener, DialogLinsener {
    private String giftBagId = "";
    private WebView webView;
    private UserInfo userInfo;
    private Messages messages;
    private RelativeLayout exchanegGift;
    private DetailsGift detailsGiftData;
    private ImageView giftImage;
    private TextView giftName;
    private TextView giftNum;
    private int type = 0;
    private TextView exchanegText;
    private DialogShowStyle dialogShowStyle;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gift_bag_details_activity);
        mContext = this;
        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            giftBagId = bundle.getString("giftBagId");
            type = bundle.getInt("type");
        }
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.gift_bag_details);
        webView = (WebView) findViewById(R.id.gift_bag_details_activity_web);
        exchanegGift = (RelativeLayout) findViewById(R.id.gift_bag_details_activity_exchange);
        exchanegText = (TextView) findViewById(R.id.gift_bag_details_activity_text);
        if (type == 1) {
            exchanegText.setText(getResources().getString(R.string.ac_code));
        }
        giftImage = (ImageView) findViewById(R.id.gift_bag_details_activity_img);
        giftName = (TextView) findViewById(R.id.gift_bag_details_activity_name);
        giftNum = (TextView) findViewById(R.id.gift_bag_details_activity_num);
        getGiftBagDetails();
        exchanegGift.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gift_bag_details_activity_exchange:
                userInfo = UserInfoUtil.getUserInfo(GiftBagDetailsActivity.this);
                if (null == userInfo.id || userInfo.id.equals("")) {
                    buildDialog(this, R.string.alert, R.string.login_alert);
                    return;
                }
                if (getExchangeData(giftBagId).equals(giftBagId)) {
                    if (type == 1) {
                        Toast.makeText(this,
                                getResources().getString(R.string.have_get_code),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this,
                                getResources().getString(R.string.have_get_gift),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    getGiftBag();
                }
                break;
        }
    }

    // protected void isGotoLoginDialog() {
    // AlertDialog.Builder builder = new AlertDialog.Builder(this);
    // builder.setMessage("请先登录");
    // builder.setTitle("提示");
    //
    // builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
    // @Override
    // public void onClick(DialogInterface dialog, int which) {
    // dialog.dismiss();
    // Bundle bundle = new Bundle();
    // bundle.putString("LoginTag", "GiftBagDetailsActivity");
    // Intent intent = new Intent(GiftBagDetailsActivity.this,
    // LoginActivity.class);
    // intent.putExtras(bundle);
    // startActivity(intent);
    // }
    // });
    // builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
    // public void onClick(DialogInterface dialog, int which) {
    // dialog.dismiss();
    // }
    // });
    // builder.create().show();
    // }

    private void getGiftBagDetails() {
        dialogShowStyle = new DialogShowStyle(
                GiftBagDetailsActivity.this, "");
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/gamegiftservice_getGamegfitToHtml?giftBagId="
                            + giftBagId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    detailsGiftData = JsonUtils.getDetailsGiftData(jsonData);
                    if (null != detailsGiftData) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    setInfoDataShow(detailsGiftData);
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(GiftBagDetailsActivity.this,
                            getResources().getString(R.string.no_data),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(GiftBagDetailsActivity.this,
                            getResources().getString(R.string.getdata_fail),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void getGiftBag() {
        userInfo = UserInfoUtil.getUserInfo(this);
        dialogShowStyle = new DialogShowStyle(
                GiftBagDetailsActivity.this, getResources().getString(
                R.string.up_data));
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/usergiftRelationshipservice_receiveGiftByUser.action?giftBagId="
                            + giftBagId + "&userid=" + userInfo.id;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    messages = JsonUtils.getGiftExange(jsonData);
                    if (null != messages && !messages.isSuccess.equals("")) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mmHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mmHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    rememberExchange(giftBagId, giftBagId);
                    Toast.makeText(GiftBagDetailsActivity.this, messages.message,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(GiftBagDetailsActivity.this, messages.message,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(GiftBagDetailsActivity.this,
                            getResources().getString(R.string.getdata_fail),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public void rememberExchange(String giftBagId, String exchangeData) {
        SharedPreferences setting = getSharedPreferences(
                MyApplication.SETTING_INFO, 0);
        setting.edit().putString("Bag" + giftBagId, exchangeData).commit();
    }

    public String getExchangeData(String giftBagId) {
        SharedPreferences setting = getSharedPreferences(
                MyApplication.SETTING_INFO, 0);
        String data = setting.getString("Bag" + giftBagId, "");
        return data;
    }

    private void setInfoDataShow(DetailsGift detailsGiftData) {
        BitmapManager.INSTANCE
                .loadBitmap(detailsGiftData.imgUrl, giftImage,
                        R.drawable.default_local, true,
                        MyApplication.widthPixels, true);
        giftName.setText(detailsGiftData.giftName);
        giftNum.setText(detailsGiftData.spareCount);
        try {
            webView.loadDataWithBaseURL(
                    "fake://not/needed",
                    "<html><head><meta http-equiv='content-type' content='text/html;charset=utf-8'><style type=\"text/css\">img{ width:95%}</style><STYLE TYPE=\"text/css\"> BODY {margin: 3px 0px 0px 0px} </STYLE><BODY TOPMARGIN=5 LEFTMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0></head><body bgcolor=\"#ffffff\" line-height:150%>"
                            + new String(detailsGiftData.html.getBytes("utf-8"))
                            + "</body></html>", "text/html", "utf-8", "");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onSure() {
        Bundle bundle = new Bundle();
        bundle.putString("LoginTag", "GiftBagDetailsActivity");
        Intent intent = new Intent(GiftBagDetailsActivity.this,
                LoginActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onCancle() {
        // TODO Auto-generated method stub

    }

}