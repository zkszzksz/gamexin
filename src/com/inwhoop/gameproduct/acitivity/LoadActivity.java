package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMGroupManager;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.db.GameinfoDatabaseDBhelper;
import com.inwhoop.gameproduct.hxpackage.DemoHXSDKHelper;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.FaceConversionUtil;

/**
 * TODO 开始等待界面
 */
public class LoadActivity extends Activity {
    private Context mContext;
    private static final int sleepTime = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load);
        mContext=LoadActivity.this;
//        LoginOpenfireUtil.loginOpenfire(mContext);
    }

    @Override
    protected void onStart() {
        super.onStart();

        new Thread(new Runnable() {
            public void run() {
                GameinfoDatabaseDBhelper d = GameinfoDatabaseDBhelper
                        .getInstance(mContext);
                FaceConversionUtil.getInstace().getFileText(getApplication());

                if (DemoHXSDKHelper.getInstance().isLogined()) {
                    // ** 免登陆情况 加载所有本地群和会话
                    //不是必须的，不加sdk也会自动异步去加载(不会重复加载)；
                    //加上的话保证进了主页面会话和群组都已经load完毕
                    long start = System.currentTimeMillis();
                    EMGroupManager.getInstance().loadAllGroups();
                    EMChatManager.getInstance().loadAllConversations();
                    long costTime = System.currentTimeMillis() - start;
                    //等待sleeptime时长
                    if (sleepTime - costTime > 0) {
                        try {
                            Thread.sleep(sleepTime - costTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    //进入主页面
                    Act.toActClearTop(mContext, MainActivity.class);
                    finish();
                }else {
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {
                    }
                    Act.toActClearTop(mContext, MainActivity.class);//要去登录的
                    finish();
                }
            }
        }).start();

    }


}
