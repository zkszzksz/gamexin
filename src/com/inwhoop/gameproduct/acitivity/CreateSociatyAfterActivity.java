package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.onekeyshare.Share;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.view.HorizontalListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Describe: TODO   公会的创建成功
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/14 11:35
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class CreateSociatyAfterActivity extends BaseActivity implements View.OnClickListener {
    private SociatyBean bean=new SociatyBean();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_sociaty_after);
        mContext = CreateSociatyAfterActivity.this;
        bean = (SociatyBean) getIntent().getSerializableExtra("bean");
        ShareSDK.initSDK(this);
        initData();
    }
    
    private void initData() {
        init();
        setTitle(R.string.create_success);
        setHeadLeftButton(R.drawable.back);

        ImageView headIV = (ImageView) findViewById(R.id.head_img);
        BitmapManager.INSTANCE.loadBitmap(bean.guildheadphoto, headIV, R.drawable.loading, true);
        EditText sociatyName = (EditText) findViewById(R.id.sociaty_name);
        sociatyName.setFocusable(false);
        sociatyName.setText(bean.guildname);
        EditText sociaty_jianjie_ET = (EditText) findViewById(R.id.sociaty_jianjie_ET);
        sociaty_jianjie_ET.setFocusable(false);
        sociaty_jianjie_ET.setText(bean.guildremark);
        setListViewData();

        findViewById(R.id.mailayout).setOnClickListener(this);
        findViewById(R.id.inivatelayout).setOnClickListener(this);
        findViewById(R.id.tellfriend).setOnClickListener(this);
        findViewById(R.id.finishbtn).setOnClickListener(this);
    }

    private void setListViewData() {
        HorizontalListView listview = (HorizontalListView) findViewById(R.id.horizontal_listView);
        SimpleAdapter adapter = new SimpleAdapter(mContext, addImgMap(bean),
                R.layout.view_item_add_sociaty_game,
                new String[]{"img"},
                new int[]{R.id.img});
        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(final View view, Object data, String textRepresentation) {
                if (view instanceof ImageView && data instanceof String) {
                    ImageView iv = (ImageView) view;
                    BitmapManager.INSTANCE.loadBitmap("" + data, iv, R.drawable.loading, true);
                    return true;
                }
                return false;
            }
        });
        listview.setAdapter(adapter);
    }

    //TODO 游戏对象数据
    private List<Map<String, Object>> addImgMap(SociatyBean aabean) {
        List<Map<String, Object>> addGameList = new ArrayList<Map<String, Object>>();

        Map<String, Object> map ;
        for (GameInfo bean : aabean.gamemsg) {
            map = new HashMap<String, Object>();
            map.put("img", bean.gameicon);
            map.put("id", bean.gameid);//保存id用于上传给服务器
            addGameList.add(map);
        }
        return addGameList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mailayout:
            	Intent intent = new Intent(mContext,MailListActivity.class);
            	intent.putExtra("teltype", 2);
            	intent.putExtra("id", bean.guildid);
            	startActivity(intent);
                break;

            case R.id.inivatelayout:
                Bundle b = new Bundle();
                b.putString("Id", ""+bean.guildid);
                b.putString("Tag","Trade");
                Act.toAct(mContext, InviteMembersActivity.class,b);
                break;
            case R.id.tellfriend:
                Share.showShare(mContext, "邀请你加入【" + bean.guildname + "】公会", bean.guildheadphoto);
                break;
            case R.id.finishbtn:
                Bundle b2 = new Bundle();
                b2.putSerializable("bean",bean);
                b2.putBoolean("isCreateOk",true);
                Act.toAct(mContext, SociatySureActivity.class,b2);
                
                break;
        }
    }
}
