package com.inwhoop.gameproduct.acitivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;
import com.easemob.EMConnectionListener;
import com.easemob.EMError;
import com.easemob.chat.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.InviteMessage;
import com.inwhoop.gameproduct.fragment.*;
import com.inwhoop.gameproduct.hxpackage.Constant;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;
import com.inwhoop.gameproduct.xmpp.MypacketListener;
import com.inwhoop.gameproduct.xmpp.XmppConnectionUtil;

import java.util.UUID;

/**
 * 首页主体框架
 *
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: MainActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * @date 2014-4-2 下午2:26:26
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class MainActivity extends BaseFragmentActivity {

    public TabHost mTabHost;
    private TabWidget tabWidget;
    private LayoutInflater layoutInflater;
    // Tab选项卡的文字
    private String mTextviewArray[];
    // 存放Fragment界面
    private Class<?> fragmentArray[] = {HomeFragment.class,
            GiftBagPlatformFragment.class, MessageFragment.class,
            GameLoopFragment.class, PersonFragment.class};

    private HomeFragment hoFragment = null;

    private GiftBagPlatformFragment bagPlatformFragment = null;

    public MessageFragment messageFragment = null;

    private GameLoopFragment gameLoopFragment = null;

    private PersonFragment personFragment = null;

    // 定义数组来存放按钮图片
    private int drawableArray[] = {R.drawable.xml_tabhost_home,
            R.drawable.xml_tabhost_gift, R.drawable.xml_tabhost_msg,
            R.drawable.xml_tabhost_cicle1, R.drawable.xml_tabhost_mine};

    private FragmentManager mFm = null;

    private FragmentTransaction mFt;

    public static int old_pos = 2;// 上一次的选择 .写成静态公共，如登录后跳转较方便
    private NewMessageBroadcastReceiver msgReceiver;

    private boolean isMustLogin = false;//被登出或者删除环信后的标识

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.isLoginBack) {
            mTabHost.setCurrentTab(old_pos);
            MyApplication.isLoginBack = false;
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.getBoolean(Constant.ACCOUNT_REMOVED, false)) {
            // 防止被移除后，没点确定按钮然后按了home键，长期在后台又进app导致的crash
            // 三个fragment里加的判断同理
            MyApplication.getInstance().logout(null);
            isMustLogin = true;
        } else if (savedInstanceState != null && savedInstanceState.getBoolean("isConflict", false)) {
            // 防止被T后，没点确定按钮然后按了home键，长期在后台又进app导致的crash
            // 三个fragment里加的判断同理
            isMustLogin = true;
        }

        setContentView(R.layout.activity_main);
        mContext = MainActivity.this;
        MyApplication.context = mContext;
        MyApplication.widthPixels = getWindowManager().getDefaultDisplay()
                .getWidth();
        MyApplication.heightPixels = getWindowManager().getDefaultDisplay()
                .getHeight();
        MyApplication.packName = getPackageName();
        MyApplication.APP_PATH = Environment.getExternalStorageDirectory()
                .getPath()
                + "/Android/data/"
                + MyApplication.packName
                + "/gamecache/";
        mFm = getSupportFragmentManager();
        initView();

        langLight();

        initHx();
    }

    private void initHx() {
////		MobclickAgent.setDebugMode( true );
//        //--?--
//        MobclickAgent.updateOnlineConfig(this);
        if (getIntent().getBooleanExtra("conflict", false) && !isConflictDialogShow) {
            showConflictDialog();
        } else if (getIntent().getBooleanExtra(Constant.ACCOUNT_REMOVED, false) && !isAccountRemovedDialogShow) {
            showAccountRemovedDialog();
        }

        // 注册一个接收消息的BroadcastReceiver
        msgReceiver = new NewMessageBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(EMChatManager.getInstance().getNewMessageBroadcastAction());
        intentFilter.setPriority(3);
        registerReceiver(msgReceiver, intentFilter);
//
//        // 注册一个ack回执消息的BroadcastReceiver
        IntentFilter ackMessageIntentFilter = new IntentFilter(EMChatManager.getInstance().getAckMessageBroadcastAction());
        ackMessageIntentFilter.setPriority(3);
        registerReceiver(ackMessageReceiver, ackMessageIntentFilter);

        // 注册一个离线消息的BroadcastReceiver
        // IntentFilter offlineMessageIntentFilter = new
        // IntentFilter(EMChatManager.getInstance()
        // .getOfflineMessageBroadcastAction());
        // registerReceiver(offlineMessageReceiver, offlineMessageIntentFilter);

        // setContactListener监听联系人的变化等
//        EMContactManager.getInstance().setContactListener(new MyContactListener());
//        // 注册一个监听连接状态的listener
        EMChatManager.getInstance().addConnectionListener(new MyConnectionListener());
//        // 注册群聊相关的listener
        EMGroupManager.getInstance().addGroupChangeListener(new MyGroupChangeListener());
        // 通知sdk，UI 已经初始化完毕，注册了相应的receiver和listener, 可以接受broadcast了
        EMChat.getInstance().setAppInited();

    }

    private void langLight() {  //屏幕 常亮
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
// in onResume() call

        mWakeLock.acquire();
// in onPause() call
        mWakeLock.release();
    }


    /**
     * 初始化
     *
     * @Title: initView
     * @Description: TODO void
     */
    private void initView() {
        IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
        filter.addAction(MyApplication.AGAIN_CONNECT);
        registerReceiver(mRecever, filter);
        layoutInflater = LayoutInflater.from(this);
        mTextviewArray = getResources().getStringArray(R.array.tab);
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        mTabHost.setup();
        // 得到fragment的个数
        final int count = fragmentArray.length;

        TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                mFt = mFm.beginTransaction();
                if (null != hoFragment) {
                    mFt.hide(hoFragment);
                }
                if (null != bagPlatformFragment) {
                    mFt.hide(bagPlatformFragment);
                }
                if (null != messageFragment) {
                    mFt.hide(messageFragment);
                }
                if (null != gameLoopFragment) {
                    mFt.hide(gameLoopFragment);
                }
                if (null != personFragment) {
                    mFt.hide(personFragment);
                }
                if (tabId.equalsIgnoreCase(mTextviewArray[0])) {
                    setFragment(0);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[1])) {
                    setFragment(1);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[2])) {
                    setFragment(2);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[3])) {
                    setFragment(3);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[4])) {
                    setFragment(4);
                }
                mFt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                mFt.commit();
            }
        };
        mTabHost.setOnTabChangedListener(tabChangeListener);
        for (int i = 0; i < count; i++) {
            // 为每一个Tab按钮设置图标、文字和内容
            TabSpec tabSpec = mTabHost.newTabSpec(mTextviewArray[i])
                    .setIndicator(getTabItemView(i));
            tabSpec.setContent(new DummyTabContent(this));
            mTabHost.addTab(tabSpec);
            // 将Tab按钮添加进Tab选项卡中
            // mTabHost.addTab(tabSpec, fragmentArray[i], null);
            // // 设置Tab按钮的背景
            // mTabHost.getTabWidget().getChildAt(i)
            // .setBackgroundResource(R.drawable.home_btn_bg);
        }
        mTabHost.setCurrentTab(0);
    }

    private void setFragment(int pos) {
        if ((pos == 2 || pos == 3 || pos == 4)
                && UserInfoUtil.getUserInfo(mContext).id.equals("")) {
            Act.toAct(mContext, LoginActivity.class);
        } else
            old_pos = pos;

        switch (pos) {
            case 0:
                if (null == hoFragment) {
                    hoFragment = new HomeFragment();
                    mFt.add(R.id.realtabcontent, hoFragment, mTextviewArray[pos]);
                } else {
                    mFt.show(hoFragment);
                }
                break;
            case 1:
                if (null == bagPlatformFragment) {
                    bagPlatformFragment = new GiftBagPlatformFragment(mFm);
                    mFt.add(R.id.realtabcontent, bagPlatformFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(bagPlatformFragment);
                }
                break;
            case 2:
                if (null == messageFragment) {
                    messageFragment = new MessageFragment();
                    mFt.add(R.id.realtabcontent, messageFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(messageFragment);
                }
                break;
            case 3:
                if (null == gameLoopFragment) {
                    gameLoopFragment = new GameLoopFragment();
                    mFt.add(R.id.realtabcontent, gameLoopFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(gameLoopFragment);
                }
                break;
            case 4:
                if (null == personFragment) {
                    personFragment = new PersonFragment();
                    mFt.add(R.id.realtabcontent, personFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(personFragment);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 给Tab按钮设置图标和文字
     *
     * @param index tab下标
     *
     * @return View
     *
     * @Title: getTabItemView
     * @Description: TODO
     */

    private View getTabItemView(int index) {
        View view = layoutInflater.inflate(R.layout.tab_item_view, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
        imageView.setImageResource(drawableArray[index]);

        TextView textView = (TextView) view.findViewById(R.id.textview);
        textView.setText(mTextviewArray[index]);

        return view;
    }


    private boolean isFinished = false; // 返回键是否一定时间内第一次按，标识

    @SuppressWarnings("deprecation")
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isFinished) {
                try {
                    ClientGroupServer.client = null;
                    ClienConServer.client = null;
                    if (null != messageFragment && null != messageFragment.mRecever) {
                        unregisterReceiver(messageFragment.mRecever);
                    }
                    MyApplication.context = null;
                    MypacketListener.getInstance().loginout();
                    XmppConnectionUtil.getInstance().closeConnection();
                } catch (Exception e) {
                }
                System.exit(0);
            } else {
                MyToast.showShort(mContext, "再按一次返回键退出");
                new Thread() {
                    public void run() {
                        isFinished = true;
                        try {
                            Thread.sleep(2000);
                            isFinished = false;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
        return false;
    }

    public BroadcastReceiver mRecever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MyApplication.AGAIN_CONNECT)) {
                LoginOpenfireUtil.loginOpenfire(mContext);
                Intent intent1 = new Intent();
                intent1.setAction(MyApplication.CHAT_AGAIN_INIT);
                sendBroadcast(intent1);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mRecever);
        } catch (Exception e) {
        }
    }

    /**
     * 新消息广播接收者
     */
    private class NewMessageBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // 主页面收到消息后，主要为了提示未读，实际消息内容需要到chat页面查看

            String from = intent.getStringExtra("from");
            // 消息id
            String msgId = intent.getStringExtra("msgid");
            EMMessage message = EMChatManager.getInstance().getMessage(msgId);
            // 2014-10-22 修复在某些机器上，在聊天页面对方发消息过来时不立即显示内容的bug
            if (FriendChatActivity.activityInstance != null) {
                if (message.getChatType() == EMMessage.ChatType.GroupChat) {
                    if (message.getTo().equals(FriendChatActivity.activityInstance.getToChatUsername()))
                        return;
                } else {
                    if (from.equals(FriendChatActivity.activityInstance.getToChatUsername()))
                        return;
                }
            }

            // 注销广播接收者，否则在ChatActivity中会收到这个广播
            abortBroadcast();

            notifyNewMessage(message);

            // 刷新bottom bar消息未读数
//            updateUnreadLabel();
//            if (currentTabIndex == 0) {
//                // 当前页面如果为聊天历史页面，刷新此页面
//                if (chatHistoryFragment != null) {
//                    chatHistoryFragment.refresh();
//                }
//            }

        }
    }

    /**
     * 消息回执BroadcastReceiver
     */
    private BroadcastReceiver ackMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            abortBroadcast();

            String msgid = intent.getStringExtra("msgid");
            String from = intent.getStringExtra("from");

            EMConversation conversation = EMChatManager.getInstance().getConversation(from);
            if (conversation != null) {
                // 把message设为已读
                EMMessage msg = conversation.getMessage(msgid);

                if (msg != null) {

                    // 2014-11-5 修复在某些机器上，在聊天页面对方发送已读回执时不立即显示已读的bug
                    if (FriendChatActivity.activityInstance != null) {
                        if (msg.getChatType() == EMMessage.ChatType.Chat) {
                            if (from.equals(FriendChatActivity.activityInstance.getToChatUsername()))
                                return;
                        }
                    }

                    msg.isAcked = true;
                }
            }

        }
    };
//
//
//    /***
//     * 好友变化listener
//     *
//     */
//    private class MyContactListener implements EMContactListener {
//
//        @Override
//        public void onContactAdded(List<String> usernameList) {
//            // 保存增加的联系人
//            Map<String, User> localUsers = MyApplication.getInstance().getContactList();
//            Map<String, User> toAddUsers = new HashMap<String, User>();
//            for (String username : usernameList) {
//                User user = setUserHead(username);
//                // 添加好友时可能会回调added方法两次
//                if (!localUsers.containsKey(username)) {
//                    userDao.saveContact(user);
//                }
//                toAddUsers.put(username, user);
//            }
//            localUsers.putAll(toAddUsers);
//            // 刷新ui
//            if (currentTabIndex == 1)
//                contactListFragment.refresh();
//
//        }
//
//        @Override
//        public void onContactDeleted(final List<String> usernameList) {
//            // 被删除
//            Map<String, User> localUsers = MyApplication.getInstance().getContactList();
//            for (String username : usernameList) {
//                localUsers.remove(username);
//                userDao.deleteContact(username);
//                inviteMessgeDao.deleteMessage(username);
//            }
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    // 如果正在与此用户的聊天页面
//                    String st10 = getResources().getString(R.string.have_you_removed);
//                    if (ChatActivity.activityInstance != null && usernameList.contains(ChatActivity.activityInstance.getToChatUsername())) {
//                        Toast.makeText(MainActivity.this, ChatActivity.activityInstance.getToChatUsername() + st10, 1).show();
//                        ChatActivity.activityInstance.finish();
//                    }
//                    updateUnreadLabel();
//                    // 刷新ui
//                    if (currentTabIndex == 1)
//                        contactListFragment.refresh();
//                    else if(currentTabIndex == 0)
//                        chatHistoryFragment.refresh();
//                }
//            });
//
//        }
//
//        @Override
//        public void onContactInvited(String username, String reason) {
//            // 接到邀请的消息，如果不处理(同意或拒绝)，掉线后，服务器会自动再发过来，所以客户端不需要重复提醒
//            List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
//
//            for (InviteMessage inviteMessage : msgs) {
//                if (inviteMessage.getGroupId() == null && inviteMessage.getFrom().equals(username)) {
//                    inviteMessgeDao.deleteMessage(username);
//                }
//            }
//            // 自己封装的javabean
//            InviteMessage msg = new InviteMessage();
//            msg.setFrom(username);
//            msg.setTime(System.currentTimeMillis());
//            msg.setReason(reason);
//            Log.d(TAG, username + "请求加你为好友,reason: " + reason);
//            // 设置相应status
//            msg.setStatus(InviteMessage.InviteMesageStatus.BEINVITEED);
//            notifyNewIviteMessage(msg);
//
//        }
//
//        @Override
//        public void onContactAgreed(String username) {
//            List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
//            for (InviteMessage inviteMessage : msgs) {
//                if (inviteMessage.getFrom().equals(username)) {
//                    return;
//                }
//            }
//            // 自己封装的javabean
//            InviteMessage msg = new InviteMessage();
//            msg.setFrom(username);
//            msg.setTime(System.currentTimeMillis());
//            Log.d(TAG, username + "同意了你的好友请求");
//            msg.setStatus(InviteMessage.InviteMesageStatus.BEAGREED);
//            notifyNewIviteMessage(msg);
//
//        }
//
//        @Override
//        public void onContactRefused(String username) {
//            // 参考同意，被邀请实现此功能,demo未实现
//            Log.d(username, username + "拒绝了你的好友请求");
//        }
//
//    }

    private boolean isConflictDialogShow;
    private boolean isAccountRemovedDialogShow;

    /**
     * 显示帐号在别处登录dialog
     */
    private void showConflictDialog() {
        isMustLogin = true;
        isConflictDialogShow = true;
        MyApplication.getInstance().logout(null);
        if (!MainActivity.this.isFinishing()) {
            showToast("您的账号已在别处登录");
        }
    }


    /**
     * 帐号被移除的dialog
     */
    private void showAccountRemovedDialog() {
        isMustLogin = true;
        isAccountRemovedDialogShow = true;
        MyApplication.getInstance().logout(null);
        if (!MainActivity.this.isFinishing()) {
            showToast("您的账号已被移除");
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getIntent().getBooleanExtra("conflict", false) && !isConflictDialogShow) {
            showConflictDialog();
        } else if (getIntent().getBooleanExtra(Constant.ACCOUNT_REMOVED, false) && !isAccountRemovedDialogShow) {
            showAccountRemovedDialog();
        }
    }

    /**
     * 连接监听listener
     *
     */
    private class MyConnectionListener implements EMConnectionListener {

        @Override
        public void onConnected() {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
//                    if (null!=messageFragment)
//                    messageFragment.errorItem.setVisibility(View.GONE);
                }

            });
        }

        @Override
        public void onDisconnected(final int error) {
            final String st1 = getResources().getString(R.string.Less_than_chat_server_connection);
            final String st2 = getResources().getString(R.string.the_current_network);
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if(error == EMError.USER_REMOVED){
                        // 显示帐号已经被移除
                        showAccountRemovedDialog();
                    }else if (error == EMError.CONNECTION_CONFLICT) {
                        // 显示帐号在其他设备登陆dialog
                        showConflictDialog();
                    } else {
//                        if (null==messageFragment)return;
//
//                        messageFragment.errorItem.setVisibility(View.VISIBLE);
//                        if (NetUtils.hasNetwork(MainActivity.this))
//                            messageFragment.errorText.setText(st1);
//                        else
//                            messageFragment.errorText.setText(st2);

                    }
                }

            });
        }
    }

    /**
     * MyGroupChangeListener
     */
    private class MyGroupChangeListener implements GroupChangeListener {

        @Override
        public void onInvitationReceived(String groupId, String groupName, String inviter, String reason) {
            boolean hasGroup = false;
//			for (EMGroup group : EMGroupManager.getInstance().getAllGroups()) {
//				if (group.getGroupId().equals(groupId)) {
//					hasGroup = true;
//					break;
//				}
//			}
//			if (!hasGroup)
//				return;

            // 被邀请
            String st3 = getResources().getString(R.string.Invite_you_to_join_a_group_chat);
            EMMessage msg = EMMessage.createReceiveMessage(EMMessage.Type.TXT);
            msg.setChatType(EMMessage.ChatType.GroupChat);
            msg.setFrom(inviter);
            msg.setTo(groupId);
            msg.setMsgId(UUID.randomUUID().toString());
            msg.addBody(new TextMessageBody(inviter + st3));
            // 保存邀请消息
            EMChatManager.getInstance().saveMessage(msg);
            // 提醒新消息
            EMNotifier.getInstance(getApplicationContext()).notifyOnNewMsg();
        }

        @Override
        public void onInvitationAccpted(String groupId, String inviter, String reason) {

        }

        @Override
        public void onInvitationDeclined(String groupId, String invitee, String reason) {

        }

        @Override
        public void onUserRemoved(String groupId, String groupName) {
            // 提示用户被T了，demo省略此步骤
            // 刷新ui
        }

        @Override
        public void onGroupDestroy(String groupId, String groupName) {
            // 群被解散
            // 提示用户群被解散,demo省略
            // 刷新ui
        }

        @Override
        public void onApplicationReceived(String groupId, String groupName, String applyer, String reason) {
            // 用户申请加入群聊
            InviteMessage msg = new InviteMessage();
            msg.setFrom(applyer);
            msg.setTime(System.currentTimeMillis());
            msg.setGroupId(groupId);
            msg.setGroupName(groupName);
            msg.setReason(reason);
            Log.d("TAG", applyer + " 申请加入群聊：" + groupName);
            msg.setStatus(InviteMessage.InviteMesageStatus.BEAPPLYED);
//            notifyNewIviteMessage(msg);
            EMNotifier.getInstance(getApplicationContext()).notifyOnNewMsg();
        }

        @Override
        public void onApplicationAccept(String groupId, String groupName, String accepter) {
            String st4 = getResources().getString(R.string.Agreed_to_your_group_chat_application);
            // 加群申请被同意
            EMMessage msg = EMMessage.createReceiveMessage(EMMessage.Type.TXT);
            msg.setChatType(EMMessage.ChatType.GroupChat);
            msg.setFrom(accepter);
            msg.setTo(groupId);
            msg.setMsgId(UUID.randomUUID().toString());
            msg.addBody(new TextMessageBody(accepter + st4));
            // 保存同意消息
            EMChatManager.getInstance().saveMessage(msg);
            // 提醒新消息
            EMNotifier.getInstance(getApplicationContext()).notifyOnNewMsg();
        }

        @Override
        public void onApplicationDeclined(String groupId, String groupName, String decliner, String reason) {
            // 加群申请被拒绝，demo未实现
        }

    }
}
