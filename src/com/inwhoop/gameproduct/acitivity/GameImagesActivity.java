package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.view.HackyViewPager;
import com.inwhoop.gameproduct.view.PhotoView;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: GameImagesActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * @date 2014-4-26 上午11:06:17
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GameImagesActivity extends BaseActivity {

    private static final String ISLOCKED_ARG = "isLocked";

    private ViewPager viewPager = null;

    private LinearLayout textLayout = null;

    private TextView contentTextView = null;

//    private List<View> imgs = new ArrayList<View>();

    private LayoutInflater inflater = null;

    private boolean flag = true;

    private GameInfo info = null;

//    private MenuItem menuLockItem;

    private String textInfo = "";//下面要显示的信息


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_img_layout);
        mContext = GameImagesActivity.this;
        inflater = LayoutInflater.from(mContext);
        info = (GameInfo) getIntent().getSerializableExtra("bean");

        init(savedInstanceState);
    }

    public void init(Bundle savedInstanceState) {
        super.init();
        setTitle(R.string.gameimages);
        setHeadLeftButton(R.drawable.back);
        viewPager = (HackyViewPager) findViewById(R.id.gameviewpager);
        textLayout = (LinearLayout) findViewById(R.id.lin_layout);
        contentTextView = (TextView) findViewById(R.id.imgtv);
        RelativeLayout.LayoutParams parm = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, getWindowManager()
                .getDefaultDisplay().getHeight() / 4
        );
        parm.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        findViewById(R.id.scroll).setLayoutParams(parm);

        textLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInfoIfVisible();
            }
        });

//        if (savedInstanceState != null) {
//            boolean isLocked = savedInstanceState.getBoolean(ISLOCKED_ARG, false);
//            ((HackyViewPager) viewPager).setLocked(isLocked);
//        }
        viewPager.setAdapter(new MyAdapter());
        viewPager.setOnPageChangeListener(new MypageListener());
        if (info.imglist.size() > 0)
            textInfo = 1 + "/" + info.imglist.size() + "\n" +
                    info.imglist.get(0).imagetxt;
        contentTextView.setText(textInfo);
        setIsLockBtn();
    }


    private void setInfoIfVisible() {
        if (flag) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.dialog_exit);
            contentTextView.startAnimation(animation);
            flag = false;
        } else {
            contentTextView.setText(textInfo);
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.dialog_enter);
            contentTextView.startAnimation(animation);
            flag = true;
        }
    }

    class MypageListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int position) {
            textInfo = (position + 1) + "/" + info.imglist.size() + "\n" +
                    info.imglist.get(position).imagetxt;

            contentTextView.setText(textInfo);
        }
    }

    class MyAdapter extends PagerAdapter {

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return info.imglist.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return (arg0 == arg1);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            PhotoView photoView = new PhotoView(container.getContext());
            photoView.setImageURIByBitmapManager(info.imglist.get(position).image);

            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            return photoView;

        }
    }

    //以下注释为设置图片是否锁定而放缩，目前有bug滑动回来之后不能放缩了
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.viewpager_menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        menuLockItem = menu.findItem(R.id.menu_lock);
//        toggleLockBtnTitle();
//        menuLockItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                toggleViewPagerScrolling();
//                toggleLockBtnTitle();
//                return true;
//            }
//        });
//
//        return super.onPrepareOptionsMenu(menu);
//    }
//
    private void toggleViewPagerScrolling() {
        if (isViewPagerActive()) {
            ((HackyViewPager) viewPager).toggleLock();
        }
    }

    //
//    private void toggleLockBtnTitle() {
//        boolean isLocked = false;
//        if (isViewPagerActive()) {
//            isLocked = ((HackyViewPager) viewPager).isLocked();
//        }
//        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
//        if (menuLockItem != null) {
//            menuLockItem.setTitle(title);
//        }
//    }
//
    private boolean isViewPagerActive() {
        return (viewPager != null && viewPager instanceof HackyViewPager);
    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        if (isViewPagerActive()) {
//            outState.putBoolean(ISLOCKED_ARG, ((HackyViewPager) viewPager).isLocked());
//        }
//        super.onSaveInstanceState(outState);
//    }

    //设置右上角按钮，以使图片固定此页可最大化
    private void setIsLockBtn() {
        boolean isLocked = false;
        isLocked = ((HackyViewPager) viewPager).isLocked();
        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
        setRightSecondBtText(title);
        head_right_bt_2.setTextSize(15);
        toBtnTitle();

        getRightSecondBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViewPagerScrolling();
                toBtnTitle();
            }
        });
    }

    private void toBtnTitle() {
        boolean isLocked = false;
        if (isViewPagerActive()) {
            isLocked = ((HackyViewPager) viewPager).isLocked();
        }
        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
        setRightSecondBtText(title);
        head_right_bt_2.setTextSize(15);
    }
}
