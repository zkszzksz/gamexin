package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.view.RoundAngleImageView;
import com.inwhoop.gameproduct.view.WordwrapLayout;

import java.util.List;

/**
 * @Describe: TODO 修改公会的入驻游戏
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/23 14:03
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class UpdateSociatyInfoGamesActivity extends BaseActivity {

    public static final int SEARCH_GAME_RESULT = 201;//查询游戏返回
    private WordwrapLayout wrapLayout;
    private SociatyBean sociatyBean = new SociatyBean();
    private View addItem;//加号图片

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updata_s_games);
        mContext = UpdateSociatyInfoGamesActivity.this;
        SociatyBean bb = (SociatyBean) getIntent().getSerializableExtra("bean");
        if (null != bb)
            sociatyBean = bb;

        initData();

    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.ruzhuyouxi);
        setRightSecondBtText("完成");
        getRightSecondBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });

        wrapLayout = (WordwrapLayout) findViewById(R.id.gamelistlayout);


        initGameitem();
    }

    private void initGameitem() {
        int size = sociatyBean.gamemsg.size();
        for (int i = 0; i < size; i++) {
            addGameIcon(i);
        }

        if (size < 4) {
            addAndResImg();
        }
    }

    private void addGameIcon(int i) {
        final View item = LayoutInflater.from(mContext).inflate(
                R.layout.join_game_list_item, null);
        RoundAngleImageView img = (RoundAngleImageView) item
                .findViewById(R.id.gameimg);
        BitmapManager.INSTANCE.loadBitmap(
                sociatyBean.gamemsg.get(i).gameicon, img,
                R.drawable.loading, true);

        wrapLayout.addView(item);
        final int finalI = i;
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapLayout.removeView(item);
                sociatyBean.gamemsg.remove(finalI);
                if (sociatyBean.gamemsg.size() == 3)
                    addAndResImg();
            }
        });
    }

    //添加加号图片
    private void addAndResImg() {
        addItem = LayoutInflater.from(mContext).inflate(
                R.layout.join_game_list_item, null);
        RoundAngleImageView img = (RoundAngleImageView) addItem
                .findViewById(R.id.gameimg);
        img.setImageResource(R.drawable.add_bg_jianju);

        wrapLayout.addView(addItem);
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addGamelogo();
            }
        });
    }

    //TODO 跳转到搜索页面查询游戏
    private void addGamelogo() {
        Intent intent = new Intent(mContext, SearchGameActivity.class);
        Bundle b = new Bundle();
        b.putInt("tag", SEARCH_GAME_RESULT);
        intent.putExtras(b);
        startActivityForResult(intent, SEARCH_GAME_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SEARCH_GAME_RESULT && resultCode == Activity.RESULT_OK
                && null != data) { //搜索游戏后返回
            GameInfo bean = (GameInfo) data.getExtras().getSerializable("bean");
            List<GameInfo> sGameList = sociatyBean.gamemsg;
            for (int i = 0; i < sGameList.size(); i++) {
                if (bean.gameid == sGameList.get(i).gameid) {
                    MyToast.showShort(mContext, "相同游戏不能重复添加");
                    return;
                }
            }
            wrapLayout.removeView(addItem);
            sGameList.add(bean);
            addGameIcon(sGameList.size() - 1);
            if (sGameList.size() < 4)         //已有4个游戏的话不能添加更多
                addAndResImg();                //增加加号图片
        }
    }

    private void update() {
        showProgressDialog("修改中...");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    SociatyBean sb = new SociatyBean();
                    sb.guildid=sociatyBean.guildid;
                    sb. gameids= sociatyBean.gameids;
                    msg.obj = JsonUtils.updateSociaty(sb);

                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    showToast("失败了%>_<%，稍后再试吧");
                    break;

                case MyApplication.READ_SUCCESS:
                    Object[] obj= (Object[]) msg.obj;
                    if ((Boolean) obj[1]) {
                        showToast("修改成功O(∩_∩)O~~");
                        Intent intent = new Intent();
                        Bundle b=new Bundle();
                        b.putSerializable("bean",sociatyBean);
                        intent.putExtras(b);
                        setResult(101, intent);
                        finish();

                    } else {
                        showToast("修改失败%>_<%~~");
                    }
                    break;

                default:
                    break;
            }
        }
    };
}
