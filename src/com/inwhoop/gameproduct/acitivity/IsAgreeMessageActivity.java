package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.IsAgreeMsgAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.ApplyInfo;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 验证消息 是否同意界面 * * * ****** Created by ZK ********
 * @Date: 2014/04/08 16:09
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class IsAgreeMessageActivity extends BaseActivity {

    private static final char IS_AGREE_SUCCESS = 0x01;
    private static final char IS_AGREE_FAIL = 0x02;
    private IsAgreeMsgAdapter adapter;
    private UserInfo user;
    private int index = -1;
    private boolean isAgree;// 必须在点击时设置值
    private int noData = 0;//标识，如果目前3项都没数据，才提示暂无请求

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_is_agree_message);
        mContext = IsAgreeMessageActivity.this;
        user = UserInfoUtil.getUserInfo(mContext);

        initData();
        initNetData();

    }

    private void initNetData() {
        getRequestByUser(UserInfoUtil.getUserInfo(mContext).id);
        getSociatyApplylist(UserInfoUtil.getUserInfo(mContext).id);
        getInvateList(UserInfoUtil.getUserInfo(mContext).id);
    }

    // private void addTestData() {
    // for (int i = 0; i < 2; i++) {
    // AgreeMessage bean = new AgreeMessage();
    // bean.info = "测试申请xxx" + i + "#321";
    // bean.logo =
    // "http://www.it.com.cn/audio/mp4/img/2011/04/25/09/110425_cowon_c2_1.jpg";
    // if (i % 2 == 0) bean.sex = "女";
    // else bean.sex = "男";
    // adapter.addBean(bean);
    // }
    // }

    private void initData() {
        init();
        setTitle(R.string.agree_friend);
        setHeadLeftButton(R.drawable.back);

        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new IsAgreeMsgAdapter(mContext, handler,
                new ArrayList<ApplyInfo>());
        listView.setAdapter(adapter);
    }

    // TODO 10 处理请求（加为好友，拒绝加为好友）
    private void solveFriend(final int zid, final String userId,
                             final int solveState) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.solveFriend(zid, userId, solveState);
                    msg.what = IS_AGREE_SUCCESS;
                } catch (Exception e) {
                    msg.what = IS_AGREE_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private void solveSociaty(final int solveState, final String msgid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.handleSociatyApplyInfo(solveState, msgid);
                    msg.what = IS_AGREE_SUCCESS;
                } catch (Exception e) {
                    msg.what = IS_AGREE_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    /**
     * 解决邀请
     *
     * @param solveState
     * @param msgid
     */
    private void solveInvate(final int solveState, final String msgid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.handleInvateInfo(solveState, msgid);
                    msg.what = IS_AGREE_SUCCESS;
                } catch (Exception e) {
                    msg.what = IS_AGREE_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    // TODO 9 显示所有请求（加为好友）
    public void getRequestByUser(final String bid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getRequestByUser(bid);
                    msg.arg1 = 1;
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    public void getSociatyApplylist(final String bid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getSociatyApplyInfoList(bid);
                    msg.arg1 = 2;
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    public void getInvateList(final String bid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getInvateList(bid);
                    msg.arg1 = 3;
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            ApplyInfo applyInfo = (ApplyInfo) msg.getData().getSerializable("bean"); // 从adapter传回的数据

            switch (msg.what) {
                case MyApplication.READ_FAIL:

                    break;
                case MyApplication.READ_SUCCESS:
                    List<ApplyInfo> list = (List<ApplyInfo>) msg.obj;
                    if (list.size() == 0) {
                        noData++;
                        if (noData == 3) {
                            MyToast.showShort(mContext, "暂无请求");
                            noData = 0;
                        }
                        return;
                    }
                    for (ApplyInfo af : list) {
                        af.isF = msg.arg1;
                    }

                    adapter.addLists(list);
                    // addTestData();
                    break;

                case IS_AGREE_SUCCESS:
                    JsonResultBean jrb = (JsonResultBean) msg.obj;
                    MyToast.showShort(mContext, "" + jrb.msg);

                    if (index != -1) {
                        adapter.getLists().get(index).isManage = true;
                        adapter.getLists().get(index).isAgree = isAgree;
                        adapter.notifyDataSetChanged();
                        index = -1;
                    }
                    break;
                case IS_AGREE_FAIL:

                    break;

                case CLICK_INDEX_INFO: // 跳转个人信息
                    Bundle b = new Bundle();

                    ApplyInfo bb = applyInfo;
                    FriendData frd = new FriendData();
                    frd.nickname = bb.userName;
                    frd.userphoto = bb.userPhoto;
                    frd.nickname = bb.userName;
                    frd.sex = "" + bb.sex;
                    frd.birthday = bb.birthday;
                    frd.useraddress = bb.address;
                    frd.remark = bb.remark;

                    b.putSerializable("bean", frd);
                    b.putInt("tag", 101);
                    Act.toAct(mContext, AddFriendSureActivity.class, b);
                    break;
                case CLICK_INDEX_OK: // 同意
                    index = msg.arg1;
                    isAgree = true;
                    if (applyInfo.isF == 1) {
                        solveFriend(applyInfo.id, user.id, 1);
                    } else if (applyInfo.isF == 2) {
                        solveSociaty(1, applyInfo.ugid);
                    } else {
                        solveInvate(1, applyInfo.ugid);
                    }
                    break;
                case CLICK_INDEX_NOT_OK: // 拒绝
                    index = msg.arg1;
                    isAgree = false;
                    if (applyInfo.isF == 1) {
                        solveFriend(applyInfo.id, user.id, -1);
                    } else if (applyInfo.isF == 3) {
                        solveSociaty(2, applyInfo.ugid);
                    } else {
                        solveInvate(2, applyInfo.ugid);
                    }
                    break;
                default:
                    break;
            }

        }

        ;
    };

    // 点击索引：点击列表项；点击按钮；点击名字。
    protected final static int CLICK_INDEX_INFO = 10;
    protected final static int CLICK_INDEX_OK = 11;
    protected final static int CLICK_INDEX_NOT_OK = 12;
}
