package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

/**
 * Created by Administrator on 2014/7/8.
 */
public class ChangePwdActivity extends BaseActivity implements
		View.OnClickListener {
	private EditText oldEdit;
	private EditText newEdit;
	private EditText TnewEdit;
	private Button changeBtn;

	private String oldstr, newstr, surestr;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_pwd_activity);
		mContext = ChangePwdActivity.this;
		init();
		initView();
	}

	private void initView() {
		setHeadLeftButton(R.drawable.back);
		setTitle(R.string.change_pwd);
		oldEdit = (EditText) findViewById(R.id.change_pwd_activity_old);
		newEdit = (EditText) findViewById(R.id.change_pwd_activity_new);
		TnewEdit = (EditText) findViewById(R.id.change_pwd_activity_two_new);
		changeBtn = (Button) findViewById(R.id.change_pwd_activity_change);
		changeBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.change_pwd_activity_change:
			changePass();
			break;
		}
	}

	private void changePass() {
		if ("".equals(check())) {
			showProgressDialog("正在进行修改，请稍后~~");
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
                        msg.obj = JsonUtils.chagePass(
								UserInfoUtil.getUserInfo(mContext).id, oldstr,
								newstr);
						msg.what = MyApplication.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = MyApplication.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}else{
			showToast(check());
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("网络错误修改失败%>_<%~~");
				break;

			case MyApplication.READ_SUCCESS:
				JsonResultBean jrb = (JsonResultBean) msg.obj;
				if (jrb.isOk) {
					UserInfoUtil.clear(mContext);
                    Act.toActClearTop(mContext, MainActivity.class);
                    Bundle b=new Bundle();
                    b.putInt("changePwd",LoginActivity.CHANGE_PWD);
                    Act.toActClearTop(mContext, LoginActivity.class,b);
				}
                showToast(jrb.msg);
                break;

			default:
				break;
			}
		}
	};

	private String check() {
		oldstr = oldEdit.getText().toString().trim();
		newstr = newEdit.getText().toString().trim();
		surestr = TnewEdit.getText().toString().trim();
		if ("".equals(oldstr)) {
			return "旧密码不能为空";
		} else if ("".equals(newstr)) {
			return "新密码不能为空";
		} else if ("".equals(surestr)) {
			return "确认密码不能为空";
		} else if (!newstr.equals(surestr)) {
			return "两次输入的新密码不相同";
		} else if (newstr.length()<6) {
			return "密码不能小于6位";
		}
		return "";
	}

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }
}