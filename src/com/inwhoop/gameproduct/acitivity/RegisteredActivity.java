package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.daompl.MLocationLinsener;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.CheckFormatUtil;
import com.inwhoop.gameproduct.utils.DialogShowStyle;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;

/**
 * 用户注册页面-dingwenlong
 * Created by Administrator on 2014/4/4.
 */
public class RegisteredActivity extends BaseFragmentActivity implements View.OnClickListener, MLocationLinsener {
    private EditText username;
    private EditText email;
    private EditText passWord;
    private EditText re_PassWord;
    private Button registered;
    private RelativeLayout back;
    private DialogShowStyle dialogShowStyle;
    
    private boolean resgOpenfire = false;
    private double latitude =0;
    private double longitude =0;

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(RegisteredActivity.this);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registered_activity);
        //init();
        //setHeadLeftButton(R.drawable.back);
        initView();
        startLocation(this);
    }

    private void initView() {
        back=(RelativeLayout)findViewById(R.id.registered_activity_back);
        setBackClick();
        username = (EditText) findViewById(R.id.registered_activity_username);
        email = (EditText) findViewById(R.id.registered_activity_email);
        passWord = (EditText) findViewById(R.id.registered_activity_password);
        re_PassWord = (EditText) findViewById(R.id.registered_activity_re_password);
        registered = (Button) findViewById(R.id.registered_activity_registered);
        registered.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registered_activity_registered:
                 UserInfo userInfo =new UserInfo();
                userInfo.username = username.getText().toString().trim();
                userInfo.email = email.getText().toString().trim();
                userInfo.password = passWord.getText().toString().trim();

                if (userInfo.username.equals("")) {
                    Toast.makeText(this, this.getString(R.string.registered_username_toast), Toast.LENGTH_SHORT).show();
                }  else if (email.getText().toString().equals("")) {
                    Toast.makeText(this, this.getString(R.string.no_emails), Toast.LENGTH_SHORT).show();
                } else if (CheckFormatUtil.checkEmail(userInfo.email) == false) {
                    Toast.makeText(this, this.getString(R.string.no_email), Toast.LENGTH_SHORT).show();
                } else if (userInfo.password.equals("")) {
                    Toast.makeText(this, this.getString(R.string.registered_pwd_toast), Toast.LENGTH_SHORT).show();
                } else if (re_PassWord.getText().toString().equals("")) {
                    Toast.makeText(this, this.getString(R.string.registered_repwd_toast), Toast.LENGTH_SHORT).show();
                } else if (passWord.getText().toString().trim().length() < 6) {
                    Toast.makeText(this, this.getString(R.string.pwd_fail), Toast.LENGTH_SHORT).show();
                } else if (!userInfo.password
                        .equals(re_PassWord.getText().toString().trim())) {
                    Toast.makeText(this, this.getString(R.string.registered_toast), Toast.LENGTH_SHORT).show();
                } else {
                    userInfo.xlat=""+latitude;
                    userInfo.ylong=""+longitude;
                    userRegist(userInfo);
                }
                break;
        }
    }

    /*
    设置返回按钮的点击事件
     */
    private void setBackClick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void userRegist(final UserInfo userInfo){
        dialogShowStyle=new DialogShowStyle(RegisteredActivity.this,"正在提交数据，请稍后");
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getRegistData(userInfo);
                    msg.what = MyApplication.SUCCESS;
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            System.out.println("结果为+++==" + msg.what);
            switch (msg.what) {
                case MyApplication.SUCCESS:
//                	registOpenfire();
                    JsonResultBean jb = (JsonResultBean) msg.obj;
                    if (jb.isOk) {
                        finish();
                    }
                    showToast("" + jb.msg);
                    break;
                case MyApplication.ERROR:
                    dialogShowStyle.dialogDismiss();
                    Toast.makeText(RegisteredActivity.this, getResources().getString(R.string.regist_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    
//    private void registOpenfire(){
//    	new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				Message msg = new Message();
//				try{
//					userInfo.openfi123repassword = pass123Word.getText().toString().trim();
//					resgOpenfire = XmppConnectionUtil.getInstance().regist(userInfo.chatId, userInfo.openfir123epassword);
//					if (resgOpenfire) {
//                        msg.what = MyApplication.SUCCESS;
//                    } else {
//                        msg.what = MyApplication.FAIL;
//                    }
//				}catch(Exception e){
//					 msg.what = MyApplication.FAIL;
//				}
//				handler.sendMessage(msg);
//			}
//		}).start();
//    }
//
//    public Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            System.out.println("结果为+++==" + msg.what);
//            dialogShowStyle.dialogDismiss();
//            switch (msg.what) {
//                case MyApplication.SUCCESS:
//                    UserInfoUtil.rememberUserInfo(RegisteredActivity.this, userInfo);
//                    LoginOpenfireUtil.loginOpenfire(RegisteredActivity.this);
//                    Toast.makeText(RegisteredActivity.this, getResources().getString(R.string.regist_success), Toast.LENGTH_SHORT).show();
//                    Act.toActClearTop(RegisteredActivity.this,MainActivity.class);
//                    finish();
//                    break;
//                case MyApplication.FAIL:
//                	 delete();
//                    Toast.makeText(RegisteredActivity.this, "注册失败", Toast.LENGTH_SHORT).show();
//                    break;
//            }
//        }
//    };
//
//    private void delete(){
//    	new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				try{
//					JsonUtils.delteOpenfire(MyApplication.HOST+MyApplication.DELETE_USER, "userid", ""+userInfo.id);
//				}catch(Exception e){
//					e.printStackTrace();
//				}
//			}
//		}).start();
//    }

    @Override
    public void mLocationFaile(String errorInfo) {
    }

    @Override
    public void mLocationXY(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}