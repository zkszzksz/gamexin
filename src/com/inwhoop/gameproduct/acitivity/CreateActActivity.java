package com.inwhoop.gameproduct.acitivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.SociatyActBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.utils.Utils;

/**
 * @Describe: TODO     创建活动
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/11 16:46
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class CreateActActivity extends BaseActivity implements View.OnClickListener {

    private ImageView imgIV;
    private EditText titleEt, contentEt, startEt, endEt;
    private int oYear, oMonth, oDay;//开始时间
    private int eYear, eMonth, eDay;//结束时间
    private Bitmap imgBmp = null;  //活动图片bitmap
    private SociatyBean sociatyBean = new SociatyBean();

    @SuppressWarnings("deprecation")
    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_act);
        mContext = CreateActActivity.this;
        SociatyBean bean = (SociatyBean) getIntent().getSerializableExtra("bean");
        if (null != bean) sociatyBean = bean;
        initData();
        setInitialTime();
    }

    private void setInitialTime() {
        Time time = new Time("GMT+8");
        time.setToNow();
        oYear = eYear = time.year;
        oMonth = eMonth = time.month + 1;  //获取当前月份-1，这个要注意
        oDay = eDay = time.monthDay;
        startEt.setText(oYear + "-" + oMonth + "-" + oDay);
        endEt.setText(oYear + "-" + oMonth + "-" + oDay);
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.create_act);
        setRightSecondBtText(getResources().getString(R.string.fabu));
        getRightSecondBt().setOnClickListener(this);

        titleEt = (EditText) findViewById(R.id.create_title_ET);
        contentEt = (EditText) findViewById(R.id.create_content_ET);
        imgIV = (ImageView) findViewById(R.id.add_img);
        imgIV.setOnClickListener(this);
        startEt = (EditText) findViewById(R.id.start_time_et);
        endEt = (EditText) findViewById(R.id.end_time_et);
        findViewById(R.id.start_time_IV).setOnClickListener(this);
        findViewById(R.id.end_time_IV).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_right_second:  //发布
                createSociatyAct();
                break;
            case R.id.add_img:
                showCameraDialog(handler, true, 200, 160, true);
                break;
            case R.id.start_time_IV:
                showTimeView(0);
                break;
            case R.id.end_time_IV:
                showTimeView(1);
                break;
        }
    }

    /**
     * 创建公会活动
     *
     * @param
     *
     * @return void
     *
     * @Title: createSociatyAct
     * @Description: TODO
     */
    private void createSociatyAct() {
        if (!check()) return;
        showProgressDialog("公会活动创建中...");
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    SociatyActBean bean = new SociatyActBean();
                    bean.actname = titleEt.getText().toString().trim();
                    bean.actcontent = contentEt.getText().toString().trim();
                    bean.actimage = Utils.imgToBase64(imgBmp);
                    bean.begintime = startEt.getText().toString();
                    bean.endtime = endEt.getText().toString();
                    bean.guildid = "" + sociatyBean.guildid;
                    bean.userid = UserInfoUtil.getUserInfo(mContext).id;
                    msg.obj = JsonUtils.createSociatyAct(bean);
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private boolean check() {
        String title = titleEt.getText().toString().trim();
        String content = contentEt.getText().toString().trim();

        String showStr = "";
        if ("".equals(title)) {
            showStr = "活动标题不能为空哦~";
        } else if ("".equals(content)) {
            showStr = "活动内容不能为空哦~";
        } else if (null == imgBmp) {
            showStr = "活动图片不能为空，否则无法展示";
        } else if (!checkTime()) {
            showStr = "开始时间大于结束时间啦~";
        } else {
            return true;
        }
        MyToast.showShort(mContext, showStr);
        return false;
    }

    private boolean checkTime() {
        if (oYear < eYear) {
        } else if (oYear == eYear && oMonth < eMonth) {
        } else if (oMonth == eMonth && oDay <= eDay) {
        } else {
            return false;
        }
        return true;
    }

    /**
     * @param tag 0表示开始的时间，1表示结束的时间
     */
    private void showTimeView(int tag) {
        if (tag == 0) {
            new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    oYear = year;
                    oMonth = monthOfYear + 1;
                    oDay = dayOfMonth;

                    String data_01 = new StringBuilder()
                            .append(year)
                            .append("-")
//                            .append((monthOfYear + 1) < 10 ? "" + (monthOfYear + 1) //原先代码有少10的话前面添"0"
//                                    : (monthOfYear + 1))
                            .append(oMonth)
                            .append("-")
                            .append((dayOfMonth < 10) ? "" + dayOfMonth : dayOfMonth).toString();

                    startEt.setText(data_01);
                }
            }, oYear, oMonth - 1, oDay).show();
        } else {
            new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    eYear = year;
                    eMonth = monthOfYear + 1;
                    eDay = dayOfMonth;

                    String data_01 = new StringBuilder()
                            .append(year)
                            .append("-")
//                            .append((monthOfYear + 1) < 10 ? "" + (monthOfYear + 1)
//                                    : (monthOfYear + 1)).append("-")
                            .append(eMonth)
                            .append("-")
                            .append((dayOfMonth < 10) ? "" + dayOfMonth : dayOfMonth).toString();

                    endEt.setText(data_01);
                }
            }, eYear, eMonth - 1, eDay).show();
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case RESULT_IMG: //接收相机返回图片路径
                    imgBmp = Utils.compressImage("" + msg.obj);
                    imgIV.setImageBitmap(imgBmp);
                    break;
                case RESULT_CUT_IMG:
                    imgBmp = (Bitmap) msg.obj;
                    imgIV.setImageBitmap(imgBmp);
                    break;
                case MyApplication.READ_SUCCESS:
                  JsonResultBean jrb = (JsonResultBean) msg.obj;
                    if (jrb.isOk) {
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("shuaxin", true);
                        intent.putExtras(bundle);
                        setResult(200, intent);
                        finish();
                    }
                    showToast("" + jrb.msg);
                    break;
                case MyApplication.READ_FAIL:
                    showToast("创建失败%>_<%~~");
                    break;
            }
        }
    };
}
