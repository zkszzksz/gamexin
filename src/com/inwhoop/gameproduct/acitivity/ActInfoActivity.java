package com.inwhoop.gameproduct.acitivity;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.webkit.WebView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.ActInfo;
import com.inwhoop.gameproduct.entity.Actvitieslist;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.Utils;

/**  
 * @Project: MainActivity
 * @Title: ActInfoActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-5-8 下午8:26:49
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ActInfoActivity extends BaseActivity{
	
	private WebView webView = null;
	
	private Actvitieslist act = null;
	
	private ActInfo info = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_layout);
		mContext = ActInfoActivity.this;
		act = (Actvitieslist) getIntent().getSerializableExtra("info");
		init();
	}
	
	@Override
	public void init() {
		super.init();
		setTitle(R.string.actinfo);
		setHeadLeftButton(R.drawable.back);
		webView = (WebView) findViewById(R.id.actwebview);
		read();
	}
	
	private void read() {
		showProgressDialog("正呼哧哈哧加载中...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					info = JsonUtils.getActinfo(act.actId);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if(null!=info){
					Utils.LoadcontentWeb(mContext, webView, info.actHtml);
				}
				break;

			default:
				break;
			}

		};
	};
	
}
