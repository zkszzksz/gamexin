package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.easemob.chat.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.ChatlistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.db.HandleFriendchatNoReadRecordDB;
import com.inwhoop.gameproduct.db.HandleFriendchatRecordDB;
import com.inwhoop.gameproduct.db.HandleFriendchatlistRecordDB;
import com.inwhoop.gameproduct.entity.FriendChatinfo;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.TimeRender;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.utils.Utils;
import com.inwhoop.gameproduct.view.FaceRelativeLayout;
import com.inwhoop.gameproduct.view.TouchSayButton;
import com.inwhoop.gameproduct.view.TouchSayButton.OnRecordListener;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.SingleChatPacketExtension;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * @Project: MainActivity
 * @Title: FriendChatActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 单聊
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-6-14 上午10:47:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class FriendChatActivity extends BaseActivity implements
		IXListViewListener, OnClickListener, OnRecordListener {
    public static final int CHATTYPE_SINGLE = 1;

    private String usernick; // 聊天对象用户昵称

	private String username; // 聊天对象  openfire/环信  的账号

	private String userphoto;// 聊天对象的头像

	private XListView listView = null;

	private Button sendButton = null; // 发送按钮

	private TouchSayButton audioButton = null; // 按住说话按钮

	private ImageView recordImageView = null; // 录音按钮

	private EditText msgEditText = null; // 输入框

	private boolean isRecording = false;

	private ImageView carmerImageView = null; // 调用照相机

	private List<FriendChatinfo> chatList = new ArrayList<FriendChatinfo>();

	private String msgcontent = ""; // 消息内容

	// private Chat newchat = null; // chat聊天对象

	private ChatlistAdapter adapter = null; // 适配器

	private FriendChatinfo chatinfo = null;

	private HandleFriendchatRecordDB db = null;

	private HandleFriendchatNoReadRecordDB ndb = null;

	private HandleFriendchatlistRecordDB fdb = null;

	private int page = 1; // 用于聊天记录分页

	private FaceRelativeLayout faceRelativeLayout = null;

    public static FriendChatActivity activityInstance = null;

    private NewMessageBroadcastReceiver receiver;

    private GroupListener groupListener;

    private EMConversation conversation;

    @SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
						| WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.friend_chat_layout);
		mContext = FriendChatActivity.this;
        activityInstance = this;
        db = new HandleFriendchatRecordDB(mContext);
		ndb = new HandleFriendchatNoReadRecordDB(mContext);
		fdb = new HandleFriendchatlistRecordDB(mContext);
		username = getIntent().getStringExtra("username");
		usernick = getIntent().getStringExtra("usernick");
		userphoto = getIntent().getStringExtra("userphoto");
		init();
	}

	@Override
	public void init() {
		super.init();
		setStringTitle("" + usernick);
		setHeadLeftButton(R.drawable.back);
		listView = (XListView) findViewById(R.id.chatlist);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(false);
		sendButton = (Button) findViewById(R.id.sendbtn);
		sendButton.setOnClickListener(this);
		recordImageView = (ImageView) findViewById(R.id.record_img);
		recordImageView.setOnClickListener(this);
		recordImageView.setVisibility(View.VISIBLE);
		audioButton = (TouchSayButton) findViewById(R.id.audiobtn);
		audioButton.setOnRecordListener(this);
		carmerImageView = (ImageView) findViewById(R.id.carmer_bg);
		carmerImageView.setOnClickListener(this);
		faceRelativeLayout = (FaceRelativeLayout) findViewById(R.id.faceRelativeLayout);
		listView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				faceRelativeLayout.hide();
				return false;
			}
		});
		IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
		filter.addAction(MyApplication.CHAT_AGAIN_INIT);
		registerReceiver(mRecever, filter);
		msgEditText = (EditText) findViewById(R.id.sendmsg);
		// 监听输入框的内容
		msgEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
				if (s.length() > 0) {
					sendButton.setVisibility(View.VISIBLE);
					recordImageView.setVisibility(View.GONE);
					audioButton.setVisibility(View.GONE);
					msgEditText.setVisibility(View.VISIBLE);
				} else {
					sendButton.setVisibility(View.GONE);
					recordImageView.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});
		initListviewData();

        conversation = EMChatManager.getInstance().getConversation(username);
        // 把此会话的未读数置为0
        conversation.resetUnreadMsgCount();
        // 注册接收消息广播
        receiver = new NewMessageBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(EMChatManager.getInstance().getNewMessageBroadcastAction());
        // 设置广播的优先级别大于Mainacitivity,这样如果消息来的时候正好在chat页面，直接显示消息，而不是提示消息未读
        intentFilter.setPriority(5);
        registerReceiver(receiver, intentFilter);

        // 注册一个ack回执消息的BroadcastReceiver
        IntentFilter ackMessageIntentFilter = new IntentFilter(EMChatManager.getInstance().getAckMessageBroadcastAction());
        ackMessageIntentFilter.setPriority(5);
        registerReceiver(ackMessageReceiver, ackMessageIntentFilter);
        // 注册一个消息送达的BroadcastReceiver
        IntentFilter deliveryAckMessageIntentFilter = new IntentFilter(EMChatManager.getInstance().getDeliveryAckMessageBroadcastAction());
        deliveryAckMessageIntentFilter.setPriority(5);
        registerReceiver(deliveryAckMessageReceiver, deliveryAckMessageIntentFilter);

        // 监听当前会话的群聊解散被T事件
        groupListener = new GroupListener();
        EMGroupManager.getInstance().addGroupChangeListener(groupListener);

        // show forward message if the message is not null
        String forward_msg_id = getIntent().getStringExtra("forward_msg_id");
        if (forward_msg_id != null) {
            // 显示发送要转发的消息
//            forwardMessage(forward_msg_id);
        }
	}

	/**
	 * 初始化listview
	 * 
	 * @Title: initListview
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void initListviewData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				chatList = db.getUserBygroupid(username,
						UserInfoUtil.getUserInfo(mContext).id, page);
				List<FriendChatinfo> nlist = ndb.getUserBygroupid(username,
						mContext);
				for (int i = 0; i < nlist.size(); i++) {
					chatList.add(nlist.get(i));
					db.saveGmember(nlist.get(i));
				}
				if (null != nlist && nlist.size() > 0) {
					ndb.deleteGmember(UserInfoUtil.getUserInfo(mContext).id,
							username);
				}
				handler.sendEmptyMessage(0);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			page++;
			adapter = new ChatlistAdapter(mContext, chatList, listView);
			listView.setAdapter(adapter);
			listView.setSelection(listView.getCount() - 1);
			ClienConServer.getInstance(mContext).isChat = true;
			ClienConServer.getInstance(mContext).set(adapter.getChatlist(),
					adapter, listView, username);
		};
	};

	@Override
	public void onRefresh() {
		try {
			chatList = db.getUserBygroupid(username,
					UserInfoUtil.getUserInfo(mContext).id, page);
			page++;
			adapter.addChatinfo(chatList);
			adapter.notifyDataSetChanged();
		} catch (Exception e) {
		}
		listView.stopRefresh();
	}

	@Override
	public void onLoadMore() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sendbtn:
			sendMsgcontext();
			break;

		case R.id.record_img:
			if (isRecording) {
				isRecording = false;
				msgEditText.setVisibility(View.VISIBLE);
				audioButton.setVisibility(View.GONE);
				recordImageView.setBackgroundResource(R.drawable.btn_vol);
			} else {
				isRecording = true;
				msgEditText.setVisibility(View.GONE);
				audioButton.setVisibility(View.VISIBLE);
				recordImageView.setBackgroundResource(R.drawable.btn_keybord);
			}
			faceRelativeLayout.hide();
			break;

		case R.id.carmer_bg:
			showCameraDialog(imgpathHandler, true, 200, 200, true);
			break;

		default:
			break;
		}
	}

	/** 调用系统照相机 */
	@SuppressLint("HandlerLeak")
	private Handler imgpathHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (RESULT_CUT_IMG == msg.what) {
				Bitmap bt = (Bitmap) msg.obj;
				@SuppressWarnings("static-access")
				String name = new DateFormat().format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA)) + ".jpg";
				FileOutputStream b = null;
				File file = new File(Environment.getExternalStorageDirectory()
						+ "/gameproduct/camera");
				file.mkdirs();// 创建文件夹
				String fileName = Environment.getExternalStorageDirectory()
						+ "/gameproduct/camera/" + name;
				try {
					b = new FileOutputStream(fileName);
					bt.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				sendImg(fileName);
			}
		};
	};

	private void sendImg(final String imgpath) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (imgpath.length() > 0) {
					msgcontent = Utils.imgToBase64(Utils.compressImage(imgpath));
					Message msg = new Message();
					try {
						UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
						sendMsg("", userinfo.username, 2, msgcontent, 0,
								userinfo.userphoto);
						msg.what = MyApplication.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = MyApplication.READ_FAIL;
					}
					imgHandler.sendMessage(msg);
				}
			}
		}).start();
	}

	private Handler imgHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyApplication.READ_SUCCESS:
				UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
				chatinfo = new FriendChatinfo();
				chatinfo.content = msgcontent;
				chatinfo.userid = userinfo.id;
				chatinfo.usernick = userinfo.username;
				chatinfo.username = username;
				chatinfo.userheadpath = userinfo.userphoto;
				chatinfo.time = TimeRender.getStandardDate();
				chatinfo.isMymsg = true;
				chatinfo.msgtype = 2;
				chatinfo.audiopath = "";
				chatinfo.audiolength = 0;
				db.saveGmember(chatinfo);
				if (!fdb.isExist(username, 1, TimeRender.getStandardDate(),
						userinfo.id)) {
					fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
							userinfo.id, userphoto, username);
				} else {
//					fdb.UpdateChatagetTime(username, 1, userinfo.id,
//							TimeRender.getStandardDate(), userphoto, usernick);
				}
				adapter.addChatinfo(chatinfo);
				adapter.notifyDataSetChanged();
				listView.setSelection(listView.getCount() - 1);
				break;

			case MyApplication.READ_FAIL:
				showToast("断线了%>_<%~~");
                Utils.againLogin(mContext);
                break;

			default:
				break;
			}
		};
	};

	/**
	 * 录音之后进行的处理
	 */
	@Override
	public void onRecord(String filepath, long time) {
		if (time < 1) {
			showToast("亲，您录音时间太短了~~");
			return;
		}
		UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
		try {
			sendMsg(filepath,
					userinfo.username,
					3,
					Utils.encodeBase64File(Environment
							.getExternalStorageDirectory() + filepath),
					(int) time, userinfo.userphoto);
		} catch (Exception e) {
			showToast("断线了%>_<%~~");
            Utils.againLogin(mContext);
            return;
		}
		chatinfo = new FriendChatinfo();
		chatinfo.content = msgcontent;
		chatinfo.userid = userinfo.id;
		chatinfo.usernick = userinfo.username;
		chatinfo.username = username;
		chatinfo.userheadpath = userinfo.userphoto;
		chatinfo.time = TimeRender.getStandardDate();
		chatinfo.isMymsg = true;
		chatinfo.msgtype = 3;
		chatinfo.audiopath = filepath;
		chatinfo.audiolength = (int) time;
		db.saveGmember(chatinfo);
		if (!fdb.isExist(username, 1, TimeRender.getStandardDate(), userinfo.id)) {
			fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
					userinfo.id, userphoto, username);
		} else {
//			fdb.UpdateChatagetTime(username, 1, userinfo.id,
//					TimeRender.getStandardDate(), userphoto, usernick);
		}
		adapter.addChatinfo(chatinfo);
		adapter.notifyDataSetChanged();
		listView.setSelection(listView.getCount() - 1);
	}

	/**
	 * 发送文字信息
	 * 
	 * @Title: sendMsgcontext
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void sendMsgcontext() {
		msgcontent = msgEditText.getText().toString().trim();
		if ("".equals(msgcontent)) {
			showToast("消息内容不能为空");
		} else {
            EMMessage message = EMMessage.createSendMessage(EMMessage.Type.TXT);
            // 如果是群聊，设置chattype,默认是单聊
//            if (chatType == CHATTYPE_GROUP)
//                message.setChatType(EMMessage.ChatType.GroupChat);
            TextMessageBody txtBody = new TextMessageBody(msgcontent);
            // 设置消息body
            message.addBody(txtBody);
            // 设置要发给谁,用户username或者群聊groupid
            message.setReceipt(username);
            // 把messgage加到conversation中
            conversation.addMessage(message);
            // 通知adapter有消息变动，adapter会根据加入的这条message显示消息和调用sdk的发送方法
            adapter.notifyDataSetChanged();
            listView.setSelection(listView.getCount() - 1);
            msgEditText.setText("");

            setResult(RESULT_OK);

		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onDestroy() {
		super.onDestroy();
        activityInstance = null;
        EMGroupManager.getInstance().removeGroupChangeListener(groupListener);
        // 注销广播
        try {
            unregisterReceiver(receiver);
            receiver = null;
        } catch (Exception e) {
        }
        try {
            unregisterReceiver(ackMessageReceiver);
            ackMessageReceiver = null;
            unregisterReceiver(deliveryAckMessageReceiver);
            deliveryAckMessageReceiver = null;
        } catch (Exception e) {
        }
        ClienConServer.getInstance(mContext).isChat = false;
	}

	private void sendMsg(String audiopath, String usernick, int msgtype,
			String msgcontent, int audiolength, String headpath)
			throws Exception {
		SingleChatPacketExtension pack = new SingleChatPacketExtension();
		pack.setAudiopath(audiopath);
		pack.setUsernick(usernick);
		pack.setMsgtype(msgtype);
		pack.setContent(msgcontent);
		pack.setAudiolength(audiolength);
		pack.setSendtime(TimeRender.getStandardDate());
		pack.setHeadpath(headpath);
		ClienConServer.getInstance(mContext).getChatManager(username)
				.sendMessage(pack.toXML());
	}

	public BroadcastReceiver mRecever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(MyApplication.CHAT_AGAIN_INIT)) {
				ClienConServer.getInstance(mContext).isChat = true;
				ClienConServer.getInstance(mContext).set(adapter.getChatlist(),
						adapter, listView, username);
			}
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}

    public String getToChatUsername() {
        return username;
    }


    //--------------环信------------//
    /**
     * 消息广播接收者
     *
     */
    private class NewMessageBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // 记得把广播给终结掉
            abortBroadcast();

            String username = intent.getStringExtra("from");
            String msgid = intent.getStringExtra("msgid");
            // 收到这个广播的时候，message已经在db和内存里了，可以通过id获取mesage对象
            EMMessage message = EMChatManager.getInstance().getMessage(msgid);
            // 如果是群聊消息，获取到group id
            if (message.getChatType() == EMMessage.ChatType.GroupChat) {
                username = message.getTo();
            }
            if (!username.equals(username)) {
                // 消息不是发给当前会话，return
//     123           notifyNewMessage(message);
                return;
            }
//            conversation =
//            EMChatManager.getInstance().getConversation(toChatUsername);
//            通知adapter有新消息，更新ui
            adapter.notifyDataSetChanged();
            listView.setSelection(listView.getCount() - 1);

        }
    }

    /**
     * 消息回执BroadcastReceiver
     */
    private BroadcastReceiver ackMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            abortBroadcast();

            String msgid = intent.getStringExtra("msgid");
            String from = intent.getStringExtra("from");
            EMConversation conversation = EMChatManager.getInstance().getConversation(from);
            if (conversation != null) {
                // 把message设为已读
                EMMessage msg = conversation.getMessage(msgid);
                if (msg != null) {
                    msg.isAcked = true;
                }
            }
            adapter.notifyDataSetChanged();

        }
    };

    /**
     * 消息送达BroadcastReceiver
     */
    private BroadcastReceiver deliveryAckMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            abortBroadcast();

            String msgid = intent.getStringExtra("msgid");
            String from = intent.getStringExtra("from");
            EMConversation conversation = EMChatManager.getInstance().getConversation(from);
            if (conversation != null) {
                // 把message设为已读
                EMMessage msg = conversation.getMessage(msgid);
                if (msg != null) {
                    msg.isDelivered = true;
                }
            }

            adapter.notifyDataSetChanged();
        }
    };


    /**
     * 监测群组解散或者被T事件
     *
     */
    class GroupListener extends GroupReomveListener {

        @Override
        public void onUserRemoved(final String groupId, String groupName) {
            runOnUiThread(new Runnable() {
                String st13 = getResources().getString(R.string.you_are_group);
                public void run() {
                    if (username.equals(groupId)) {
                        Toast.makeText(mContext, st13, 1).show();
//                        if (GroupDetailsActivity.instance != null)
//                            GroupDetailsActivity.instance.finish();
                        finish();
                    }
                }
            });
        }

        @Override
        public void onGroupDestroy(final String groupId, String groupName) {
            // 群组解散正好在此页面，提示群组被解散，并finish此页面
            runOnUiThread(new Runnable() {
                String st14 = getResources().getString(R.string.the_current_group);
                public void run() {
                    if (username.equals(groupId)) {
                        Toast.makeText(mContext, st14, 1).show();
//                        if (GroupDetailsActivity.instance != null)
//                            GroupDetailsActivity.instance.finish();
                        finish();
                    }
                }
            });
        }

    }

}
