package com.inwhoop.gameproduct.acitivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.onekeyshare.Share;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.RoundAngleImageView;
import com.inwhoop.gameproduct.view.WordwrapLayout;

/**
 * @Describe: TODO 工会详情页面，根据是否是申请界面跳转而来/已有公会资料查看act_tag不同
 * <p/>   //目前申请界面无需跳转而来
 * * * * ****** Created by ZK ********
 * @Date: 2014/06/10 17:30
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SociatyInfoActivity extends BaseActivity implements
        View.OnClickListener {
    public static final int ACT_TAG_ADD_SOCIATY = 101; // 表示从申请页面跳转而来
    private static final char EXIT_OK = 0x01;
    private static final char EXIT_WRONG = 0x02;

    private int act_tag;// 标识从哪个页面跳转而来
    private WordwrapLayout wrapLayout;
    private TextView sociatynameTextView = null;
    private SociatyBean sociatyBean = new SociatyBean();

    private ImageView userheadimg;

    private Bitmap headBmp = null; // 保存的头像bitmap

    private boolean isSociatyBoss = false;
    private UserInfo thisUser;


    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sociaty_info);
        mContext = SociatyInfoActivity.this;
        ShareSDK.initSDK(this);
        thisUser = UserInfoUtil.getUserInfo(mContext);
        act_tag = getIntent().getIntExtra("act_tag", 0);
        SociatyBean bb = (SociatyBean) getIntent().getSerializableExtra("bean");
        if (null != bb)
            sociatyBean = bb;

        initData();

        initSociatyData();
    }

    private void initData() {
        if (thisUser.id.equals("" +
                sociatyBean.userid)) isSociatyBoss = true;
        else isSociatyBoss = false;

        init();
        setTitle(R.string.gonghuiziliao);
        setHeadLeftButton(R.drawable.back);
        setRightSecondBt(R.drawable.share);
        head_left.setOnClickListener(this);
        getRightSecondBt().setOnClickListener(this);

        View yaoqing = findViewById(R.id.invisit_number_layout);
        yaoqing.setOnClickListener(this);
        View daoru = findViewById(R.id.daoru_number_layout);
        daoru.setOnClickListener(this);
        Button applyButton = (Button) findViewById(R.id.aplybutton);
        applyButton.setOnClickListener(this);
        Button get_out_btn = (Button) findViewById(R.id.get_out_btn);
        get_out_btn.setOnClickListener(this);
        findViewById(R.id.loop_head_layout).setOnClickListener(this);
        userheadimg = (ImageView) findViewById(R.id.head_img);
        ImageView headimg = (ImageView) findViewById(R.id.headupdate);
        ImageView nameimg = (ImageView) findViewById(R.id.nameimg);
        View zhuanrangBtn = findViewById(R.id.zhuanrang_btn);
        zhuanrangBtn.setOnClickListener(this);

        if (!isSociatyBoss) {
            headimg.setVisibility(View.GONE);
            nameimg.setVisibility(View.GONE);
            findViewById(R.id.gameviews_img).setVisibility(View.GONE);
            zhuanrangBtn.setVisibility(View.GONE);

        } else if (sociatyBean.personnum > 1) {
            zhuanrangBtn.setVisibility(View.VISIBLE);
        }

        wrapLayout = (WordwrapLayout) findViewById(R.id.gamelistlayout);
        findViewById(R.id.games_layout).setOnClickListener(this);

        if (act_tag == ACT_TAG_ADD_SOCIATY) {
            applyButton.setVisibility(View.VISIBLE);
            get_out_btn.setVisibility(View.GONE);
            yaoqing.setVisibility(View.GONE);
            daoru.setVisibility(View.GONE);
        }
    }

    private void initSociatyData() {
        BitmapManager.INSTANCE.loadBitmap(sociatyBean.guildheadphoto,
                userheadimg, R.drawable.loading,
                true);
        sociatynameTextView = ((TextView) findViewById(R.id.socaity_name));
        sociatynameTextView.setOnClickListener(this);
        sociatynameTextView.setText(sociatyBean.guildname);
        ((TextView) findViewById(R.id.socaity_num))
                .setText(sociatyBean.personnum + "人");

        initGameitem();

    }

    private void initGameitem() {
        for (int i = 0; i < sociatyBean.gamemsg.size(); i++) {
            final View item = LayoutInflater.from(mContext).inflate(
                    R.layout.join_game_list_item, null);
            RoundAngleImageView img = (RoundAngleImageView) item
                    .findViewById(R.id.gameimg);
            BitmapManager.INSTANCE.loadBitmap(
                    sociatyBean.gamemsg.get(i).gameicon, img,
                    R.drawable.loading, true);

            wrapLayout.addView(item);
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.head_left:
                Intent intent1 = new Intent();
                bundle.putSerializable("bean", sociatyBean);
                intent1.putExtras(bundle);
                setResult(300, intent1);
                finish();
                break;
            case R.id.head_right_second:   //分享
                Share.showShare(mContext, "邀请你加入【" + sociatyBean.guildname + "】公会", sociatyBean.guildheadphoto);
                break;
            case R.id.invisit_number_layout:   //邀请成员
                bundle.putString("Tag", "Trade");
                bundle.putString("Id", sociatyBean.guildid + "");
                Act.toAct(mContext, InviteMembersActivity.class, bundle);
                break;
            case R.id.daoru_number_layout:   //导入通讯录
                bundle.putInt("teltype", 2);
                bundle.putInt("id", sociatyBean.guildid);
                Act.toAct(mContext, MailListActivity.class, bundle);
                break;
            case R.id.get_out_btn: //退出公会
                showIsAgreeToast(0);
                break;
            case R.id.zhuanrang_btn://转让公会
                if (isSociatyBoss && sociatyBean.personnum > 1) {
                    Intent intent = new Intent(mContext,
                            MakeOverSociatyBossActivity.class);
                    bundle.putSerializable("bean", sociatyBean);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 102);
                }
                break;
            case R.id.aplybutton:  //目前无用的 申请加入 按钮
                MyToast.showShort(mContext, "申请加入公会按钮不在此界面");
                break;
            case R.id.loop_head_layout:  //头像栏
                if (isSociatyBoss)
                    showCameraDialog(headHandler, true, 200, 200, true);
                break;
            case R.id.socaity_name: //公会名称
                if (isSociatyBoss) {
                    Intent intent = new Intent(mContext,
                            UpdateSociatyInfoActivity.class);
                    intent.putExtra("title", "昵称");
                    intent.putExtra("id", sociatyBean.guildid);
                    intent.putExtra("content", sociatyBean.guildname);
                    intent.putExtra("msgtype", 1);
                    startActivityForResult(intent, 100);
                }
                break;
            case R.id.games_layout:  //修改入驻游戏
                if (isSociatyBoss) {
                    Intent intent = new Intent(mContext,
                            UpdateSociatyInfoGamesActivity.class);
                    intent.putExtra("bean", sociatyBean);
                    startActivityForResult(intent, 101);
                }
                break;
        }
    }

    private void showIsAgreeToast(int tag) {
        String str = "";
        if (tag == 0) { //退出公会
            if (isSociatyBoss) {
                str = getResources().getString(R.string.boss_get_out_sociaty);
            } else {
                str = "确认退出该公会？";
            }
        }
//        else if (tag == 1) {//转让公会
//            str = "确认转让公会？";
//        }
        new AlertDialog.Builder(mContext)
                .setTitle(str)
//                   .setMessage("message")
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitForGuild(sociatyBean.guildid,
                                thisUser.id, isSociatyBoss);
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    /**
     * 退出/解散 工会
     *
     * @param guildid
     * @param userid
     * @param isSociatyBoss 是否是会长
     */
    private void exitForGuild(final int guildid, final String userid, final boolean isSociatyBoss) {
        showProgressDialog("");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (isSociatyBoss)
                        msg.obj = JsonUtils.dissolveguild(guildid, userid);
                    else
                        msg.obj = JsonUtils.exitSociaty(guildid, userid);
                    msg.what = EXIT_OK;
                } catch (Exception e) {
                    msg.what = EXIT_WRONG;
                    LogUtil.e("exception,exitGuild:" + e.toString());
                }
                headHandler.sendMessage(msg);
            }
        }).start();
    }


    private Handler headHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case RESULT_CUT_IMG: // 裁剪
                    headBmp = (Bitmap) msg.obj;
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            Message msg = new Message();
                            try {
                                SociatyBean name = new SociatyBean();
                                name.guildid = sociatyBean.guildid;
                                name.userid = thisUser.id;
                                name.guildheadphoto = ""
                                        + Utils.imgToBase64(Utils
                                        .compressImage(headBmp));
                                msg.obj = JsonUtils.updateSociaty(name);
                                msg.what = MyApplication.READ_SUCCESS;
                            } catch (Exception e) {
                                msg.what = MyApplication.READ_FAIL;
                            }
                            headHandler.sendMessage(msg);
                        }
                    }).start();
                    break;

                case MyApplication.READ_SUCCESS:
                    JsonResultBean jrb11 = (JsonResultBean) msg.obj;
                    if (jrb11.isOk) {
                        userheadimg.setImageBitmap(headBmp);
                        if (null != jrb11.oneBean())
                            sociatyBean.guildheadphoto = ((SociatyBean) jrb11.oneBean()).guildheadphoto;
                    }
                    showToast("" + jrb11.msg);
                    break;

                case MyApplication.READ_FAIL:
                    showToast("失败了%>_<%，稍后再试吧");
                    break;

                case EXIT_OK:
                    JsonResultBean jrb = (JsonResultBean) msg.obj;
                    if (jrb.isOk)
                        Act.toActClearTop(mContext, MainActivity.class);
                    MyToast.showShort(mContext, "" + jrb.msg);
                    break;
                case EXIT_WRONG:
                    showToast("失败了%>_<%，稍后再试吧");
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            if (null != data) {
                String content = data.getStringExtra("content");
                sociatynameTextView.setText(content);
                sociatyBean.guildname = content;
            }
        } else if (resultCode == 101 && null != data) {
            sociatyBean = (SociatyBean) data.getSerializableExtra("bean");
            wrapLayout.removeAllViews();
            initGameitem();
        } else if (resultCode == 102 && null != data) {
            sociatyBean = (SociatyBean) data.getSerializableExtra("bean");
            initData();
            initSociatyData();
        } else if (resultCode == 300 && null != data) {
            sociatyBean = (SociatyBean) data.getSerializableExtra("bean");
            initData();
            initSociatyData();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent1 = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("bean", sociatyBean);
            intent1.putExtras(bundle);
            setResult(300, intent1);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }
}
