package com.inwhoop.gameproduct.acitivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 游戏圈中的添加子页面
 * @date 2014/4/13 12:57
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GameLoopAddActivity extends BaseActivity implements
		View.OnClickListener, DialogLinsener {
	private RelativeLayout back;
	private TextView back_text;
	private TextView title;
	private RelativeLayout addFriend;
	private RelativeLayout nearMan;
	private RelativeLayout addLoop;
	private RelativeLayout createLoop;

	private LocationClient locationClient = null;

	private double xlat, xlong;

	private boolean flag = true, uf = false;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = GameLoopAddActivity.this;
		setContentView(R.layout.game_loop_add_activity);
		initView();
	}

	private void initView() {
		back = (RelativeLayout) findViewById(R.id.head_left);
		back.setVisibility(View.VISIBLE);
		back_text = (TextView) findViewById(R.id.head_left_button);
		back_text.setBackgroundResource(R.drawable.back);
		title = (TextView) findViewById(R.id.head_title);
		title.setText(R.string.add);
		addFriend = (RelativeLayout) findViewById(R.id.game_loop_add_activity_add_friend);
		nearMan = (RelativeLayout) findViewById(R.id.game_loop_add_activity_near_man);
		addLoop = (RelativeLayout) findViewById(R.id.game_loop_add_activity_add_loop);
		createLoop = (RelativeLayout) findViewById(R.id.game_loop_add_activity_create_loop);
		back.setOnClickListener(this);
		addFriend.setOnClickListener(this);
		nearMan.setOnClickListener(this);
		addLoop.setOnClickListener(this);
		createLoop.setOnClickListener(this);

		findViewById(R.id.add_sociaty_layout).setOnClickListener(this);
		findViewById(R.id.create_sociaty_layout).setOnClickListener(this);

		location();
	}

	private void location() {
		locationClient = new LocationClient(this); // 设置定位条件
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setAddrType("detail");
		option.setCoorType("gcj02");
		option.setScanSpan(5000000);
		option.disableCache(true);// 禁止启用缓存定位
		option.setPoiNumber(5); // 最多返回POI个数
		option.setPoiDistance(1000); // poi查询距离
		option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
		locationClient.setLocOption(option);
		locationClient.registerLocationListener(new MyLocationListenner());
		locationClient.start();
		locationClient.requestLocation();
	}

	private class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null) {
				// showToast("获取数据失败~~");
				return;
			}
			xlat = location.getLatitude();
			xlong = location.getLongitude();
			read(xlat, xlong);
		}

		@Override
		public void onReceivePoi(BDLocation poilocation) {

		}
	}

	private void read(final double xlat, final double xlong) {
		if (!flag) {
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					uf = JsonUtils.uploadLocation(
							UserInfoUtil.getUserInfo(mContext).id, xlat, xlong);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("获取数据失败~~");
				break;

			case MyApplication.READ_SUCCESS:
				if (uf) {
					flag = false;
				}

				break;

			default:
				break;
			}
		};
	};

	@Override
	public void onClick(View v) {
		Bundle b = new Bundle();
		switch (v.getId()) {
		case R.id.head_left:
			finish();
			break;
		case R.id.game_loop_add_activity_add_friend:
			Act.toAct(mContext, AddFriendActivity.class);
			break;
		case R.id.game_loop_add_activity_near_man:
			startActivity(new Intent(mContext, NearPeopleActivity.class));
			break;
		case R.id.game_loop_add_activity_add_loop:
			b.putInt("tag", SearchLoopActivity.TAG_ACT_LOOP);
			Act.toAct(mContext, SearchLoopActivity.class, b);
			break;
		case R.id.game_loop_add_activity_create_loop:
			if (isGotoLoginDialog()) {
				startActivity(new Intent(mContext, CreateLoopActivity.class));
			}
			break;
		case R.id.add_sociaty_layout: // 加入工会
			b.putInt("tag", SearchSociatyActivity.TAG_ACT_SOCIATY);
			Act.toAct(mContext, SearchSociatyActivity.class, b);
			break;
		case R.id.create_sociaty_layout: // 创建工会
			Act.toAct(mContext, CreateSociatyActivity.class);
			break;
		}
	}

	protected boolean isGotoLoginDialog() {
		UserInfo userInfo = UserInfoUtil.getUserInfo(mContext);
		if (!"".equals(userInfo.id)) {
			return true;
		}

		buildDialog(this, R.string.alert, R.string.login_alert);

		return false;
	}

	@Override
	public void onSure() {
		Bundle bundle = new Bundle();
		 // bundle.putString("LoginTag", actName);
		 Intent intent = new Intent(mContext, LoginActivity.class);
		 intent.putExtras(bundle);
		 startActivity(intent);
	}

	@Override
	public void onCancle() {
		// TODO Auto-generated method stub

	}
}