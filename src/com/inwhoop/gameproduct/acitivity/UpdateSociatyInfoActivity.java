package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

/**
 * @Project: MainActivity
 * @Title: UpdateSociatyInfoActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-6-20 下午4:26:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class UpdateSociatyInfoActivity extends BaseActivity implements
		OnClickListener {

	private String title = "";

	private String content = "";

	private int msgtype = 1;
	
	private int id = 0;

	private EditText contentEditText = null;
	
	private InputMethodManager inputManager = null;
    private UserInfo user;

    @SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upadate_sociaty_info_layout);
		mContext = UpdateSociatyInfoActivity.this;
		title = getIntent().getStringExtra("title");
		content = getIntent().getStringExtra("content");
		msgtype = getIntent().getIntExtra("msgtype", 1);
		id = getIntent().getIntExtra("id", 0);

        user= UserInfoUtil.getUserInfo(mContext);
		init();

        KeyBoard.showKeyBoard(contentEditText);
	}

	@Override
	public void init() {
		super.init();
		setStringTitle("" + title);
		setHeadLeftButton(R.drawable.back);
		setRightSecondBtText("完成");
		head_right_second.setOnClickListener(this);
		contentEditText = (EditText) findViewById(R.id.contentedit);
		contentEditText.setText("" + content);
        contentEditText.setSelection(content.length());
        contentEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_NULL:
                        break;
                    case EditorInfo.IME_ACTION_SEND:
                        break;
                    case EditorInfo.IME_ACTION_DONE:
                        Update();
                        break;
                    case EditorInfo.IME_ACTION_GO:
                        break;
                }
                return true;
            }
        });}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.head_right_second:
			Update();
			break;

		default:
			break;
		}
	}

	private void Update() {
		content = contentEditText.getText().toString().trim();
		if ("".equals(content)) {
			showToast("请填写修改内容");
			return;
		}
		showProgressDialog("修改中...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
                try {
                    SociatyBean sb = new SociatyBean();
                    sb.guildid = id;
                    sb.userid = user.id;
                    switch (msgtype) {
                        case 1:
                            sb.guildname = content;
                            break;
                        case 2:
                            sb.guildremark = content;
                            break;
                        case 3:
                            sb.guildment = content;
                            break;
                        default:
                            msg.what = MyApplication.READ_FAIL;
                            break;
                    }
                    msg.obj = JsonUtils.updateSociaty(sb);
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("失败了%>_<%，稍后再试吧");
				break;

			case MyApplication.READ_SUCCESS:
                JsonResultBean jrb= (JsonResultBean) msg.obj;
				if(jrb.isOk){
					Intent intent = new Intent();
					switch (msgtype) {
					case 1:  //修改昵称
						intent.putExtra("content", content);
						setResult(100, intent);
						finish();
						break;
						
					case 2:  //修改简介
						intent.putExtra("content", content);
						setResult(100, intent);
						finish();
						break;
						
					case 3:  //修改公告
						intent.putExtra("content", content);
						setResult(200, intent);
						finish();
						break;

					default:
						break;
					}
				}
                showToast("" + jrb.msg);
                break;

			default:
				break;
			}
		};
	};

	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {
		super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
//		inputManager = (InputMethodManager) contentEditText
//				.getContext().getSystemService(
//						Context.INPUT_METHOD_SERVICE);
//		inputManager.hideSoftInputFromWindow(
//				contentEditText.getWindowToken(), 0);
	}
	
	

}
