package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.entity.JsonResultBean;
import com.inwhoop.gameproduct.entity.NetInvateBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.MypullListView;
import com.inwhoop.gameproduct.view.SelectLetterView;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO  邀请成员界面
 * @date 2014/6/3 13:56
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class InviteMembersActivity extends BaseActivity implements
        OnClickListener, MypullListView.OnRefreshListener {
    private UserInfo userInfo = null;
    private String path = MyApplication.APP_PATH + "friend/";
    private List<FriendData> friendListData;

    private boolean flag = false;
    private MyAdapter adapter;
    //    private DialogShowStyle dialogShowStyle;
    private TextView nowletter;
    private SelectLetterView letterView;
    private MypullListView listView;
    private List<FriendData> list;
    private String inviteTag = "";
    private String inviteId = "";

    @SuppressWarnings("deprecation")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite_members_activity);
        mContext = InviteMembersActivity.this;
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            inviteTag = bundle.getString("Tag");
            inviteId = bundle.getString("Id");
        }
        //circleid = getIntent().getIntExtra("circleid", 0);
        init();
        initView();
    }

    private void initView() {
        userInfo = UserInfoUtil.getUserInfo(this);
        setHeadLeftButton(R.drawable.back);
        setStringTitle(getResources().getString(R.string.invite_friend));
        setRightSecondBtText(getResources().getString(R.string.sure));
        getRightSecondBt().setOnClickListener(this);

        nowletter = (TextView) findViewById(R.id.invite_members_activity_nowletter);
        letterView = (SelectLetterView) findViewById(R.id.invite_members_activity_letter);
        listView = (MypullListView) findViewById(R.id.invite_members_activity_listview);
        listView.setonRefreshListener(this);
//        dialogShowStyle=new DialogShowStyle(this,getResources().getString(R.string.load_data));
//        dialogShowStyle.dialogShow();
        getLocationFriendListData();
    }

    private void read() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    list = getlist();
                    hhandler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    @SuppressLint("HandlerLeak")
    private Handler hhandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            dismissProgressDialog();
            listView.onRefreshComplete();
            letterView.setList(list);
            letterView.setPuListView(listView);
            letterView.setNowTextView(nowletter);
            adapter = new MyAdapter(InviteMembersActivity.this, list);
            listView.setAdapter(adapter);
            if (null != list && list.size() > 0) {
                letterView.setNoStr(list.get(0).letter);
            }
        }

        ;
    };

    private List<FriendData> getlist() throws Exception {
        List<FriendData> list = new ArrayList<FriendData>();
        FriendData info = null;
        for (int i = 0; i < friendListData.size(); i++) {
            info = new FriendData();
            info.id = friendListData.get(i).id;
            info.userphoto = friendListData.get(i).userphoto;
            info.nickname = friendListData.get(i).nickname;
            list.add(info);
        }
        List<FriendData> otherlist = new ArrayList<FriendData>();
        List<FriendData> pinlist = new ArrayList<FriendData>();
        for (int i = 0; i < list.size(); i++) {
            if (isChinese(list.get(i).nickname.charAt(0))) {
                pinlist.add(list.get(i));
            } else {
                otherlist.add(list.get(i));
            }
        }
        pinlist = getSortListFrompin(pinlist);
        otherlist = getSortListFrome(otherlist);
        list.clear();

        for (int i = 0; i < otherlist.size(); i++) {
            list.add(otherlist.get(i));
        }
        for (int i = 0; i < pinlist.size(); i++) {
            list.add(pinlist.get(i));
        }
        return list;
    }

    /**
     * 是否是汉字和字母
     *
     * @param @param  a
     * @param @return
     *
     * @return boolean
     *
     * @Title: isChinese
     * @Description: TODO
     */
    public boolean isChinese(char a) {
        int v = (int) a;
        return (v >= 19968 && v <= 171941) || (v >= 65 && v <= 90) || (v >= 97 && v <= 122);
    }

    @Override
    public void onRefresh() {
        read();
    }

    /**
     * 对对非数字客户名进行排序
     *
     * @return
     *
     * @throws BadHanyuPinyinOutputFormatCombination
     */
    private List<FriendData> getSortListFrompin(List<FriendData> list)
            throws BadHanyuPinyinOutputFormatCombination {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                try {
                    return getPinYin(arg0.nickname.toLowerCase()).compareTo(
                            getPinYin(arg1.nickname.toLowerCase()));
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = strTOup(getPinYin(list.get(i).nickname));
        }
        return list;
    }

    private String strTOup(String str) {
        return ("" + str.charAt(0)).toUpperCase();
    }

    /**
     * 对对非数字客户名进行排序
     *
     * @return
     */
    private List<FriendData> getSortListFrome(List<FriendData> list) {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                return arg0.nickname.compareTo(arg1.nickname);
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = "#";
        }
        return list;
    }

    /**
     * 将中文转换成拼音
     *
     * @param -汉字
     *
     * @return
     */
    public static String getPinYin(String zhongwen)
            throws BadHanyuPinyinOutputFormatCombination {
        String zhongWenPinYin = "";
        char[] chars = zhongwen.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            String[] pinYin = PinyinHelper.toHanyuPinyinStringArray(chars[i],
                    getDefaultOutputFormat());
            // 当转换不是中文字符时,返回null
            if (pinYin != null) {
                zhongWenPinYin += pinYin[0];
            } else {
                zhongWenPinYin += chars[i];
            }
        }

        return zhongWenPinYin + "-" + zhongwen;
    }

    /**
     * 输出格式
     *
     * @return
     */
    public static HanyuPinyinOutputFormat getDefaultOutputFormat() {
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);// 小写
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);// 没有音调数字
        format.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);// u显示
        return format;
    }

    @Override
    public void scrollitem(int first) {
        if (null != letterView && null != list && list.size() > 0 && !letterView.isFlag()) {
            letterView.setNoStr(list.get(first).letter);
        }
    }

    public void getLocationFriendListData() {
        showProgressDialog(getResources().getString(R.string.load_data));
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String jsonString = FileReadWriteUtil.readFileSdcardFile(
                            path, "friendListInfo" + userInfo.id);
                    if (jsonString.equals("")) {
                        String str = MyApplication.GET_MY_FRIEND + "?userid=" + userInfo.id;
                        jsonString = SyncHttp.Get(MyApplication.HOST, str);
                    }
                    friendListData = JsonUtils.getFriendListData(jsonString);
                    if (null != friendListData && friendListData.size() > 0) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                   /* List<String> list = new ArrayList<String>();
                    for (FriendData friendData : friendListData) {
                        list.add(friendData.userId);
                    }*/
                    read();
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(InviteMembersActivity.this,
                            getResources().getString(R.string.no_data),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(InviteMembersActivity.this,
                            getResources().getString(R.string.getdata_fail),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_right_second:
                invate();
                break;

            default:
                break;
        }
    }

    private void invate() {
        final StringBuilder userids = new StringBuilder();

        if (null == adapter || null == adapter.getAll()) {
            showToast("快去添加好友吧~~");
            return;
        }
        for (int i = 0; i < adapter.getAll().size(); i++) {
            FriendData b = adapter.getAll().get(i);
            if (b.isselect) {
                System.out.println("邀请好友的ID为111=" + b.id);
                userids.append(",").append(b.id);
            }
        }
        if ("".equals(userids)) {
            showToast("请选择好友~~");
        } else {
            final String str = userids.substring(1);
            showProgressDialog("好友邀请中...");
            new Thread(new Runnable() {

                @Override
                public void run() {
                    Message msg = new Message();
                    try {
                        if (!inviteTag.equals("") && inviteTag.equals("Loop")) {
                            msg.obj = JsonUtils.invateLoopFriends(str, inviteId);
                        } else if (!inviteTag.equals("") && inviteTag.equals("Trade")) {
                            msg.obj = JsonUtils.invateTradeFriends(new NetInvateBean(inviteId,str, "", userInfo.id));
                        }
                        msg.what = MyApplication.READ_SUCCESS;
                    } catch (Exception e) {
                        msg.what = MyApplication.READ_FAIL;
                    }
                    handler.sendMessage(msg);
                }
            }).start();
        }
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    showToast("网络错误,再邀请一次吧~~");
                    break;

                case MyApplication.READ_SUCCESS:
                    JsonResultBean jrb = (JsonResultBean) msg.obj;
                    if (jrb.isOk) {
                        finish();
                    }
                    showToast("" + jrb.msg);
                    break;

                default:
                    break;
            }
        }
    };

    class MyAdapter extends BaseAdapter {

        private List<FriendData> list;

        private LayoutInflater inflater;

        public MyAdapter(Context context, List<FriendData> list) {
            this.list = list;
            inflater = LayoutInflater.from(context);
        }

        public List<FriendData> getAll() {
            return list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup arg2) {
            Holder holder = null;
            if (convertView == null) {
                holder = new Holder();
                convertView = inflater.inflate(R.layout.invite_member_item, null);
                holder.letterLinear = (LinearLayout) convertView.findViewById(R.id.invite_member_item_letter_linear);
                holder.letter = (TextView) convertView.findViewById(R.id.invite_member_item_letter);
                holder.photo = (ImageView) convertView.findViewById(R.id.invite_member_item_photos);
                holder.name = (TextView) convertView.findViewById(R.id.invite_member_item_name);
                holder.checkBox = (CheckBox) convertView.findViewById(R.id.invite_member_item_checkbox);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            if (position >= 1) {
                if (list.get(position).letter.equals(list.get(position - 1).letter)) {
                    holder.letterLinear.setVisibility(View.GONE);
                } else {
                    holder.letterLinear.setVisibility(View.VISIBLE);
                    holder.letter.setText(list.get(position).letter);
                }
            } else {
                holder.letterLinear.setVisibility(View.VISIBLE);
                holder.letter.setText(list.get(position).letter);
            }
            holder.checkBox.setTag("" + position);
            holder.checkBox
                    .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton v,
                                                     boolean flag) {
                           /* Toast.makeText(mContext, "v:" + v.getTag() + flag,
                                    Toast.LENGTH_SHORT).show();*/
                            int pos = Integer.parseInt(v.getTag().toString());
                            list.get(pos).isselect = flag;
                        }
                    });
            BitmapManager.INSTANCE.loadBitmap(list.get(position).userphoto,
                    holder.photo, R.drawable.default_local, true);
            holder.name.setText(list.get(position).nickname);
            System.out.println("getView的ID为" + list.get(position).id);
            System.out.println("getView的userName为" + list.get(position).nickname);
            return convertView;
        }

        class Holder {
            TextView letter, name;
            public ImageView photo;
            public LinearLayout letterLinear;
            public CheckBox checkBox;
        }

    }
}