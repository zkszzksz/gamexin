package com.inwhoop.gameproduct.acitivity;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.easemob.chat.EMMessage;
import com.easemob.util.EasyUtils;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.daompl.MLocationLinsener;
import com.inwhoop.gameproduct.utils.CommonUtils;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.view.CustomProgressDialog;

/**
 * FragmentActivity基类
 * @Project: GameProduct
 * @Title: BaseFragmentActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 *
 * @author ylligang118@126.com
 * @date 2014-4-2 下午4:30:14
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class BaseFragmentActivity extends FragmentActivity {
	public Context context;

	/** 标题栏左侧按钮或者文字 */
	private RelativeLayout head_left = null;
	private TextView head_left_bt = null;
	/** 标题栏第一个按钮，相对靠左一点的按钮 */
	private RelativeLayout head_right_first = null;
	private TextView head_right_bt_1 = null;
	/** 标题栏最右边的按钮 */
	private RelativeLayout head_right_second = null;
	private TextView head_right_bt_2 = null;
	/** 标题 */
	private TextView titleView = null;

	protected Context mContext = null;

//	public ProgressDialog progressDialog;// 加载进度框
	
	public CustomProgressDialog progressDialog = null;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        context = this;
        mContext=this;
	}

	/**
	 * 控件初始化
	 *
	 * @Title: init
	 * @Description: TODO

	 */
	public void init() {
	    head_left = (RelativeLayout) findViewById(R.id.head_left);
        head_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        head_left_bt = (TextView) findViewById(R.id.head_left_button);
        head_right_first = (RelativeLayout) findViewById(R.id.head_right_first);
        head_right_second = (RelativeLayout) findViewById(R.id.head_right_second);
        head_right_bt_2 = (TextView) findViewById(R.id.head_right_bt_2);
        head_right_bt_1 = (TextView) findViewById(R.id.head_right_bt_1);
        titleView = (TextView) findViewById(R.id.head_title);
	}

	/**
	 * 设置左边按钮显示图片
	 *
	 * @Title: setHeadLeftButton
	 * @Description: TODO
	 * @param imgId
	 *            void
	 */
	public void setHeadLeftButton(int imgId) {
		head_left.setVisibility(View.VISIBLE);
		head_left_bt.setBackgroundResource(imgId);
	}

	/**
	 * 设置左边显示文字
	 *
	 * @Title: setHeadLeftText
	 * @Description: TODO
	 * @param textId
	 *            void
	 */
	public void setHeadLeftText(int textId) {
		head_left.setVisibility(View.VISIBLE);
		head_left_bt.setText(textId);
	}

	/**
	 * 设置标题
	 *
	 * @Title: setTitle
	 * @Description: TODO
	 * @param textId
	 *            void
	 */
	public void setTitle(int textId) {
		titleView.setText(textId);
	}

    /**
     * 设置标题，字符串
     *
     * @Title: setTitle
     * @Description: TODO
     * @param titleStr
     */
    public void setTitleStr(String titleStr) {
        titleView.setText(titleStr);
    }

    /**
	 * 设置右边第一个按钮
	 *
	 * @Title: setRightFirstBt
	 * @Description: TODO
	 * @param imgId
	 *            void
	 */
	public void setRightFirstBt(int imgId) {
		head_right_first.setVisibility(View.VISIBLE);
		head_right_bt_1.setBackgroundResource(imgId);
	}

	/**
	 * 设置右边第二个按钮
	 *
	 * @Title: setRightSecondBt
	 * @Description: TODO
	 * @param imgId
	 *            void
	 */
	public void setRightSecondBt(int imgId) {
		head_right_second.setVisibility(View.VISIBLE);
		head_right_bt_2.setBackgroundResource(imgId);
	}

	/**
	 * 获取左边按钮
	 *
	 * @Title: getLeftBt
	 * @Description: TODO
	 * @return RelativeLayout
	 */
	public RelativeLayout getLeftBt() {
		return head_left;
	}

	/**
	 * 获取右边第一个按钮
	 *
	 * @Title: getRightFirstBt
	 * @Description: TODO
	 * @return RelativeLayout
	 */
	public RelativeLayout getRightFirstBt() {
		return head_right_first;
	}

	/**
	 * 获取右边第二个按钮
	 *
	 * @Title: getRightSecondBt
	 * @Description: TODO
	 * @return RelativeLayout
	 */
	public RelativeLayout getRightSecondBt() {
		return head_right_second;
	}

	/**
	 * 显示进度框
	 *
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialog(String msg) {
		if (progressDialog == null){
			progressDialog = CustomProgressDialog.createDialog(this);
		}
	    	progressDialog.setMessage(msg);
	    	progressDialog.setCancelable(true);
		
		
    	progressDialog.show();
	}

	/**
	 * 关闭进度框
	 *
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 显示toast
	 *
	 * @Title: showToast
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}



	/**
	 * 自定义Dialog
	 * 
	 * @Title: buildDialog
	 * @Description: TODO
	 * @param @param dialogLinsener 自定义接口
	 * @param @param title 标题
	 * @param @param messgae 信息
	 * @param @return
	 * @return Dialog
	 */
	public void buildDialog(final DialogLinsener dialogLinsener,
			int title, int messgae) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.custom_dialog, null);
		Button ok = (Button) view.findViewById(R.id.dialog_button_ok);
		Button cancle = (Button) view.findViewById(R.id.dialog_button_cancle);
		TextView titleView=(TextView) view.findViewById(R.id.dialog_title);
		TextView msgView=(TextView) view.findViewById(R.id.dialog_message);
		titleView.setText(title);
		msgView.setText(messgae);
		final Dialog dialog = new Dialog(mContext, R.style.dialog_more_style);
		dialog.setContentView(view);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				dialogLinsener.onSure();
			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				dialogLinsener.onCancle();
			}
		});
		
	}

    private static final int notifiId = 11;
    protected NotificationManager notificationManager;

    /**
     * 当应用在前台时，如果当前消息不是属于当前会话，在状态栏提示一下
     * 如果不需要，注释掉即可
     * @param message
     */
    protected void notifyNewMessage(EMMessage message) {
        //如果是设置了不提醒只显示数目的群组(这个是app里保存这个数据的，demo里不做判断)
        //以及设置了setShowNotificationInbackgroup:false(设为false后，后台时sdk也发送广播)
        if(!EasyUtils.isAppRunningForeground(this)){
            return;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(getApplicationInfo().icon)
                .setWhen(System.currentTimeMillis()).setAutoCancel(true);

        String ticker = CommonUtils.getMessageDigest(message, this);
        String st = getResources().getString(R.string.expression);
        if(message.getType() == EMMessage.Type.TXT)
            ticker = ticker.replaceAll("\\[.{2,3}\\]", st);
        //设置状态栏提示
        mBuilder.setTicker(message.getFrom()+": " + ticker);

        //必须设置pendingintent，否则在2.3的机器上会有bug
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notifiId, intent, PendingIntent.FLAG_ONE_SHOT);
        mBuilder.setContentIntent(pendingIntent);

        Notification notification = mBuilder.build();
        notificationManager.notify(notifiId, notification);
        notificationManager.cancel(notifiId);
    }

    private MLocationLinsener mLocationLinsener;
    private LocationClient locationClient;
    /**
     * 开始定位
     *
     * @param
     * @return void
     * @Title: startLocation
     * @Description: TODO
     */
    public void startLocation(MLocationLinsener mLocationLinsener) {
        initMap();
        this.mLocationLinsener = mLocationLinsener;
        locationClient.start();
    }

    /**
     * 初始化地图数据
     * @return void
     */
    private void initMap() {
        locationClient = new LocationClient(this); // 设置定位条件
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setAddrType("detail");
        option.setCoorType("gcj02");
        option.setScanSpan(5000);
        option.disableCache(true);// 禁止启用缓存定位
        option.setPoiNumber(5); // 最多返回POI个数
        option.setPoiDistance(1000); // poi查询距离
        option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
        locationClient.setLocOption(option);
        locationClient.registerLocationListener(new MyLocationListener());
        locationClient.start();
        locationClient.requestLocation();
    }

    /**
     * 实现实位回调监听
     */
    private class MyLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null) {
                dismissProgressDialog();
                LogUtil.e("获取地址失败~~");
                return;
            }
            double xlat = location.getLatitude();
            double xlong = location.getLongitude();
            mLocationLinsener.mLocationXY(xlat, xlong);
            LogUtil.i("location,x,y,MyLocationListenner" + xlat + "," + xlong);
            if (null != locationClient) {
                locationClient.stop();
            }
        }

        @Override
        public void onReceivePoi(BDLocation poilocation) {
        }
    }
}
