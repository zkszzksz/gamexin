package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.entity.DetailsDue;
import com.inwhoop.gameproduct.entity.Messages;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;

import java.io.UnsupportedEncodingException;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO       预定游戏详细
 * @date 2014/5/8   19:40
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class DueGameDetailsActivity extends BaseFragmentActivity implements View.OnClickListener, DialogLinsener{
    private WebView webView;
    private RelativeLayout dueGame;
    private ImageView image;
    private UserInfo userInfo;
    private Messages messages;
    private String gameId="";
    private DetailsDue detailsDueData;
    private DialogShowStyle dialogShowStyle;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.due_game_details_activity);
        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            gameId = bundle.getString("gameid");
        }

        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.get_due_game);
        webView = (WebView) findViewById(R.id.due_game_details_activity_web);
        dueGame = (RelativeLayout) findViewById(R.id.due_game_details_activity_exchange);
        image = (ImageView) findViewById(R.id.due_game_details_activity_img);
        dueGame.setOnClickListener(this);
        getGiftBagDetails();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.due_game_details_activity_exchange:
                userInfo = UserInfoUtil.getUserInfo(DueGameDetailsActivity.this);
                if (null == userInfo.id || userInfo.id.equals("")) {
                	buildDialog(this, R.string.alert, R.string.login_alert);

                    return;
                }
                if (getExchangeData(gameId).equals(gameId)) {
                    Toast.makeText(this, getResources().getString(R.string.have_get_due), Toast.LENGTH_SHORT).show();
                } else {
                    getDueGame();
                }
                break;
        }
    }


    private void getGiftBagDetails() {
        dialogShowStyle = new DialogShowStyle(DueGameDetailsActivity.this, "");
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/usergameRelationshipservice_getSchGameinfo?gameid=" + gameId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    detailsDueData = JsonUtils.getDetailsDueData(jsonData);
                    if (null != detailsDueData) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    setInfoDataShow(detailsDueData);
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(DueGameDetailsActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(DueGameDetailsActivity.this, getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void getDueGame() {
        userInfo = UserInfoUtil.getUserInfo(this);
        dialogShowStyle = new DialogShowStyle(DueGameDetailsActivity.this, getResources().getString(R.string.up_data));
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/usergameRelationshipservice_schGameByUser?gameid="+gameId+"&userid="+userInfo.id;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    messages = JsonUtils.getGiftExange(jsonData);
                    if (null != messages && !messages.isSuccess.equals("")) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mmHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mmHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    rememberExchange(gameId, gameId);
                    Toast.makeText(DueGameDetailsActivity.this, messages.message, Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(DueGameDetailsActivity.this, messages.message, Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(DueGameDetailsActivity.this, getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public void rememberExchange(String giftBagId, String exchangeData) {
        SharedPreferences setting = getSharedPreferences(MyApplication.SETTING_INFO, 0);
        setting.edit().putString("Due"+giftBagId, exchangeData)
                .commit();
    }

    public String getExchangeData(String giftBagId) {
        SharedPreferences setting = getSharedPreferences(MyApplication.SETTING_INFO, 0);
        String data = setting.getString("Due"+giftBagId, "");
        return data;
    }

    private void setInfoDataShow(DetailsDue detailsDue) {
        System.out.println("预定游戏的图片地址为"+detailsDue.imgUrl);
        BitmapManager.INSTANCE.loadBitmap(detailsDue.imgUrl, image,
                R.drawable.default_local, true, MyApplication.widthPixels, true);
        try {
            webView.loadDataWithBaseURL(
                    "fake://not/needed",
                    "<html><head><meta http-equiv='content-type' content='text/html;charset=utf-8'><style type=\"text/css\">img{ width:95%}</style><STYLE TYPE=\"text/css\"> BODY {margin: 3px 0px 0px 0px} </STYLE><BODY TOPMARGIN=5 LEFTMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0></head><body bgcolor=\"#f5f0ec\" line-height:150%>"
                            + new String(detailsDue.schTxt.getBytes("utf-8"))
                            + "</body></html>", "text/html", "utf-8", ""
            );
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

	@Override
	public void onSure() {
		Bundle bundle = new Bundle();
		bundle.putString("LoginTag", "DueGameDetailsActivity");
		Intent intent = new Intent(DueGameDetailsActivity.this,
				LoginActivity.class);
		intent.putExtras(bundle);
		startActivity(intent);
		
	}

	@Override
	public void onCancle() {
		// TODO Auto-generated method stub
		
	}

}