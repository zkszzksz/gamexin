package com.inwhoop.gameproduct.utils;

import android.net.Uri;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static com.inwhoop.gameproduct.application.MyApplication.*;

/**
 * json解析工具
 * 
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: JsonUtils.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * @date 2014-4-8 下午2:33:47
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class JsonUtils {
    public static SyncHttp http = new SyncHttp();
    public static GsonBuilder gsonBuilder = new GsonBuilder();
    public static Gson gson = gsonBuilder.create();

    /**
     * 通用post请求
     *
     * @param getStr     请求接口名 ,域名已有
     * @param paramPairs 你存的参数对象
     * @param clazz      需要解析得到的对象，可传null
     * @param <T>        对象必须继承BaseBean
     * @return ｛成功否，提示信息，List<T>}，所以如果是单独一个对象，需要get(0)
     * @throws Exception
     */
    private static <T extends BaseBean> JsonResultBean getNetPost(String getStr,
                                                            List<Parameter> paramPairs, Class<T> clazz) throws Exception {
        paramPairs.add((new Parameter("uid", MyApplication.my_uid)));
        @SuppressWarnings("static-access")
        String result = http.httpPost(MyApplication.HOST + getStr, paramPairs);
        System.out.println("=======result========" + getStr + "===" + result);
        return getObjects(clazz, result);
    }

    /**
     * 通用get请求
     *
     * @param getUrl 请求接口名 ,域名已有
     * @param getStr 符号【？】之后的拼接起来的请求字符串
     * @param clazz  需要解析得到的对象，可传null
     * @param <T>    对象必须继承BaseBean
     * @return ｛成功否，提示信息，List<T>}，所以如果是单独一个对象，需要get(0)
     * @throws Exception
     */
    private static <T extends BaseBean> JsonResultBean getNetGet(String getUrl,
                                                           String getStr, Class<T> clazz) throws Exception {
        String result = http.httpGet(MyApplication.HOST + getUrl, getStr
                + "&uid=" + MyApplication.my_uid );
        LogUtil.i("result," + getUrl  + ":" + result);
        System.out.println("result=== " + getUrl + " ==  " + result);
        return getObjects(clazz, result);
    }

    /**
     * 通用解析
     *
     * @param clazz
     * @param result
     * @return Object[3]
     * @throws JSONException
     * @Title: getObjects
     * @Description: TODO
     */
    public static <T extends BaseBean> JsonResultBean getObjects(Class<T> clazz,
                                                           String result) throws JSONException {
        JSONObject obj = new JSONObject(result);
        JsonResultBean jsonResultBean = new JsonResultBean();
        try {
            jsonResultBean.msg = Uri.decode(obj.getString("msg"));
            if (obj.has("isSucess") && obj.get("isSucess").getClass() == Boolean.class)
                jsonResultBean.isOk = obj.getBoolean("isSucess");

            if (null == clazz){
            }else if (obj.has("result") && obj.get("result").getClass() == JSONArray.class) {
                JSONArray array = obj.getJSONArray("result");
                for (int i = 0; i < array.length(); i++) {
                    T bean = gson.fromJson(array.getJSONObject(i)
                            .toString(), clazz);
                    jsonResultBean.list.add(bean);
                }
            } else if (obj.has("result") && obj.get("result").getClass() == JSONObject.class) {
                T bean = gson.fromJson(obj.getJSONObject("result")
                        .toString(), clazz);
                jsonResultBean.list.add(bean);
            }
        } catch (Exception e) {
            System.out.println("e:" + e.toString());
        }

         return jsonResultBean;
    }

    /**
     * 用来遍历对象属性和属性值,添加到 List<Parameter> 但是父对象里的东西就遍历不了, 所以可以遍历了子类对象，然后手动添加其他的参数
     * ，详询ZK
     */
    public static List<Parameter> readClassAttr(BaseBean tb) throws Exception {
        Field[] fields = tb.getClass().getDeclaredFields();
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        for (Field field : fields) {
            field.setAccessible(true);
            String name = field.getName();
            String va = field.get(tb).toString();
            if (!TextUtils.isEmpty(va))
                paramPairs.add(new Parameter(name, va));
        }
        return paramPairs;
    }

	/**
	 * 获取首页banner信息
	 * 
	 * @param @return
	 * @return List<GameInfo>
	 * @throws Exception
	 * @Title: getHomeBannerList
	 * @Description: TODO
	 */
	public static List<GameInfo> getHomeBannerList() throws Exception {
		return getNetGet(HOME_BANNER,"",GameInfo.class).list;
	}

	/**
	 * 获取首页活动热门信息
	 * 
	 * @param @param hlist
	 * @param @param alist
	 * @param @return
	 * @param @throws Exception
	 * @return List<GameInfo>
	 * @Title: getHomeHotAndActList
	 * @Description: TODO
	 */
	public static void getHomeHotAndActList(List<GameInfo> hlist,
			List<Actvitieslist> alist) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpPost(MyApplication.HOST
				+ MyApplication.HOME_HOTANDACT, null);
		JSONObject obj = new JSONObject(jsonStr);
		JSONObject data1 = obj.getJSONObject("data1");
		JSONObject hotdata = data1.getJSONObject("data");
		JSONObject data2 = obj.getJSONObject("data2");
		JSONObject actdata = data2.getJSONObject("data");
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		if (hotdata.getBoolean("isSucess")) {
			JSONArray jsonArray = hotdata.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				if (info.gamemainimage.indexOf(",") != -1) {
					info.gamemainimage = MyApplication.HOST
							+ info.gamemainimage
									.substring(0, info.gamemainimage.indexOf(","));
				} else {
					info.gamemainimage = MyApplication.HOST+ info.gamemainimage;
				}
				hlist.add(info);
			}
		}
		if (actdata.getBoolean("isSucess")) {
			JSONArray jsonArray = actdata.getJSONArray("result");
			Actvitieslist info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Actvitieslist.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				alist.add(info);
			}
		}
	}

	/**
	 * 获取游戏详情-游戏资讯  ，列表
	 * 
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Gameinformation>
	 * @Title: getGameinformationlist
	 * @Description: TODO
	 */
	public static List<Gameinformation> getGameinformationlist(int gameId,
			int minid) throws Exception {
        return getNetGet(GAMEINFORMATION, "gameid=" + gameId + "&minid="
                + minid + "&limit=" + 10, Gameinformation.class).list;
    }

	/**
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Gameinfogiftinfo>
	 * @Title: getGamegiftlist
	 * @Description: TODO 13.游戏详情-活动礼包
	 */
	public static List<Gameinfogiftinfo> getGamegiftlist(int gameId, int minid)
			throws Exception {
		List<Gameinfogiftinfo> list = new ArrayList<Gameinfogiftinfo>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GAMEINFOGIFT, "ga123meid=" + gameId + "&minId="
				+ minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Gameinfogiftinfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Gameinfogiftinfo.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 获取全部活动
	 * 
	 * @param @param typeid
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Actvitieslist>
	 * @Title: getActlist
	 * @Description: TODO
	 */
	public static List<Actvitieslist> getActlist(int typeid, int minid)
			throws Exception {
		List<Actvitieslist> list = new ArrayList<Actvitieslist>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.HOT_ALLGAME, "typeid=" + typeid + "&minid="
				+ minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Actvitieslist info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Actvitieslist.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				list.add(info);
			}
		}
		return list;
	}

    /*
     * 用户登录
     */
    public static JsonResultBean getLoginData(UserInfo user) throws Exception {
        return getNetPost(USER_LOGIN, readClassAttr(user), UserInfo.class);
    }


	/*
	 * 获取礼包平台列表数据
	 */
	public static List<ActivtyCode> getActivityCode(String jsonString)
			throws Exception {
		List<ActivtyCode> list = new ArrayList<ActivtyCode>();
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			ActivtyCode activtyCode = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				activtyCode = gson.fromJson(json.toString(), ActivtyCode.class);
				if (activtyCode.imgUrl.indexOf(",") != -1) {
					activtyCode.imgUrl = MyApplication.HOST
							+ activtyCode.imgUrlPosition
							+ activtyCode.imgUrl.substring(0,
									activtyCode.imgUrl.indexOf(","));
				} else {
					activtyCode.imgUrl = MyApplication.HOST
							+ activtyCode.imgUrlPosition + activtyCode.imgUrl;
				}
				list.add(activtyCode);
			}
		}
		return list;

	}

	/*
	 * 获取预定游戏列表数据
	 */
	public static List<DueGame> getDueGameData(String jsonString)
			throws Exception {
		List<DueGame> list = new ArrayList<DueGame>();
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			DueGame dueGame = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				dueGame = gson.fromJson(json.toString(), DueGame.class);
				if (dueGame.imgUrl.indexOf(",") != -1) {
					dueGame.imgUrl = MyApplication.HOST
							+ dueGame.imgUrlPosition
							+ dueGame.imgUrl.substring(0,
									dueGame.imgUrl.indexOf(","));
				} else {
					dueGame.imgUrl = MyApplication.HOST
							+ dueGame.imgUrlPosition + dueGame.imgUrl;
				}
				list.add(dueGame);
			}
		}
		return list;

	}

	/**
	 * TODO 11.获取游戏详情接口。注意因为改变不需要传userid，不传的话写0
	 * 
	 * @throws Exception
	 */
	public static GameInfo getGameinfoByRec(String userId,int gameid) throws Exception {
        return (GameInfo) getNetGet(GAME_INFO_CONTENT,"userid=" + userId + "&gameid="
                + gameid,GameInfo.class).oneBean();
	}

	/*
	 * 领取礼包
	 */
	public static Messages getGiftExange(String jsonString) throws Exception {
		System.out.println("jsonString=  " + jsonString);
		JSONObject jsonObject = new JSONObject(jsonString);
		Messages messages = new Messages();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		messages.isSuccess = jsonObject1.getString("isSucess");
		messages.message = jsonObject1.getString("message");
		return messages;
	}

	/*
	 * 获取礼包详情页面数据
	 */
	public static DetailsGift getDetailsGiftData(String jsonString)
			throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		DetailsGift detailsGift = new DetailsGift();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		detailsGift.isSuccess = jsonObject1.getString("isSucess");
		detailsGift.message = jsonObject1.getString("message");
		String result = jsonObject1.getString("result");
		JSONObject jsonObject2 = new JSONObject(result);
		detailsGift.giftName = jsonObject2.getString("gi123ftName");
		detailsGift.html = jsonObject2.getString("html");
		detailsGift.imgUrlPosition = jsonObject2.getString("imgUrlP123osition");
		detailsGift.imgUrl = MyApplication.HOST + detailsGift.imgUrlPosition
				+ jsonObject2.getString("imgU123rl");
		detailsGift.spareCount = jsonObject2.getString("spareC123ount");
		return detailsGift;
	}

	/*
	 * 获取礼包详情页面数据
	 */
	public static DetailsDue getDetailsDueData(String jsonString)
			throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		DetailsDue detailsDue = new DetailsDue();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		detailsDue.isSucess = jsonObject1.getString("isSucess");
		detailsDue.message = jsonObject1.getString("message");
		String result = jsonObject1.getString("result");
		JSONObject jsonObject2 = new JSONObject(result);
		detailsDue.gameId = jsonObject2.getString("gam123eid");
		detailsDue.schTxt = jsonObject2.getString("sch213Txt");
		detailsDue.imgUrlPosition = jsonObject2.getString("imgUrlP213osition");
		detailsDue.imgUrl = MyApplication.HOST + detailsDue.imgUrlPosition
				+ jsonObject2.getString("im123gUrl");
		return detailsDue;
	}

	/*
	 * 获取游戏资讯详情
	 */
	public static GameNews getGameNewsContent(String newsid) throws Exception {
        return (GameNews) getNetGet(GET_NEWS_CONTENT, "newsid=" + newsid, GameNews.class).oneBean();
    }

//	/**
//	 * 热门的全部游戏通过typeid获取游戏列表
//	 *
//	 * @param @param typeid
//	 * @param @param minid
//	 * @param @return
//	 * @return List<GameInfo>
//	 * @throws Exception
//	 * @Title: getGamelistBytypeid
//	 * @Description:
//	 */
//	public static List<GameInfo> getGamelistBytypeid(int typeid, int minid,
//			int plattypeid) throws Exception {
//		List<GameInfo> list = new ArrayList<GameInfo>();
//		SyncHttp http = new SyncHttp();
//		@SuppressWarnings("static-access")
//		String jsonStr = http.httpGet(MyApplication.HOST
//				+ MyApplication.HOT_ALLGAME, "typeid=" + typeid + "&minid="
//				+ minid + "&plattypeid=" + plattypeid);
//		JSONObject jsonObject = new JSONObject(jsonStr);
//		JSONObject obj = jsonObject.getJSONObject("data");
//		if (obj.getBoolean("isSucess")) {
//			GsonBuilder gsonBuilder = new GsonBuilder();
//			Gson gson = gsonBuilder.create();
//			JSONArray jsonArray = obj.getJSONArray("result");
//			GameInfo info = null;
//			for (int i = 0; i < jsonArray.length(); i++) {
//				JSONObject json = jsonArray.getJSONObject(i);
//				info = gson.fromJson(json.toString(), GameInfo.class);
//				info.gamemainimage = MyApplication.HOST + info.gamemainimage;
//				list.add(info);
//			}
//		}
//		return list;
//	}

	/**
	 * 获取推荐游戏
	 * @Title: getRecommendGamelistBytypeid 
	 * @Description: TODO
	 * @param @param typeid
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception     
	 * @return List<GameInfo>
	 */
	public static List<GameInfo> getRecommendGamelistBytypeid(int typeid,
			int minid) throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(
				MyApplication.HOST
				+ MyApplication.RECOMMEND_GAMELIST, "type=" + typeid
				+ "&minId=" + minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				info.gamemainimage = MyApplication.HOST + info.gamemainimage;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 首页获取分类推荐游戏
	 * 
	 * @param @ty123pe 类型  1|网游 2|手游 3|页游 4|单机
	 * @param limit
     * @param minid
     * @return List<GameInfo>
	 * @Title: getHomegamelist
	 * @Description: TODO
	 */
	public static List<GameInfo> getHomegamelist(int platId, int limit, String minid) throws Exception {
        return getNetGet(HOME_GAME_MODE, "type=" + platId + "&limit=" + limit + "&minid=" + minid, GameInfo.class).list;
    }

    /**
     * 更多 全部游戏的接口
     *
     * @param @ty123pe 类型  1|网游 2|手游 3|页游 4|单机
     * @param limit
     * @param minid
     * @return List<GameInfo>
     * @Title: getHomegamelist
     * @Description: TODO
     */
    public static List<GameInfo> getAllGamelist(int platId, int limit, String minid) throws Exception {
        return getNetGet(ALL_GAME_LIST, "type=" + platId + "&limit=" + limit + "&minid=" + minid, GameInfo.class).list;
    }
	/**
	 * 获取活动详情
	 * 
	 * @param @param actid
	 * @param @return
	 * @param @throws Exception
	 * @return ActInfo
	 * @Title: getActinfo
	 * @Description: TODO
	 */
	public static ActInfo getActinfo(int actid) throws Exception {
		SyncHttp http = new SyncHttp();
		ActInfo info = null;
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.ACTINFO, "game123id=" + actid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONObject json = obj.getJSONObject("result");
			info = gson.fromJson(json.toString(), ActInfo.class);
		}
		return info;
	}

	/*
	 * 用户注册
	 */
	public static JsonResultBean getRegistData(UserInfo user) throws Exception {
        return getNetPost(USER_REGISTER, readClassAttr(user), UserInfo.class);
    }

	/**
	 * TODO 14.游戏详情-收藏 -接口
	 * 
	 * @param gameid
	 * @param userid
	 * @return 返回后台给的数据，如 【收藏失败，已收藏】
	 * @throws Exception
	 */
	public static JsonResultBean recGameinfoByUser(String gameid, String userid)
			throws Exception {
		return getNetGet(REC_GAME_INFO_BY_USER, "userid=" + userid
                + "&gameid=" + gameid,null);
	}

	/**
	 * TODO 21、个人收藏游戏的列表，显示该用户的所有收藏游戏的列表
	 * 
	 * @param minId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public static List<GameInfo> getGameByCollect(String minId, String userId)
			throws Exception {
		return getNetGet(GET_GAME_BY_COLLECT, "minid=" + minId
                + "&userid=" + userId,GameInfo.class).list;
	}

	/**
	 * TODO 20.搜索游戏
	 * 
	 * @param searchStr
	 * @param minId   接口文档里，这个没用
	 * @return
	 * @throws Exception
	 */
	public static List<GameInfo> getGameBySearch(String searchStr, String minId)
			throws Exception {
		return getNetGet(GET_GAME_BY_SEARCH, "minid=" + minId
                + "&search=" + searchStr,GameInfo.class).list;
	}

//	/**
//	 * 获取游戏图集
//	 *
//	 * @param @param gameid
//	 * @param @return
//	 * @param @throws Exception
//	 * @return List<GameImagesInfo>
//	 * @Title: getGameImages
//	 * @Description: TODO
//	 */
//	public static List<GameImagesInfo> getGameImages(int gameid)
//			throws Exception {
//		List<GameImagesInfo> list = new ArrayList<GameImagesInfo>();
//		SyncHttp http = new SyncHttp();
//		@SuppressWarnings("static-access")
//		String jsonStr = http.httpGet(MyApplication.HOST
//				+ MyApplication.GAMEIMAGES, "game123Id=" + gameid);
//		JSONObject jsonObject = new JSONObject(jsonStr);
//		JSONObject obj = jsonObject.getJSONObject("data");
//		if (obj.getBoolean("isSucess")) {
//			GsonBuilder gsonBuilder = new GsonBuilder();
//			Gson gson = gsonBuilder.create();
//			JSONArray jsonArray = obj.getJSONArray("result");
//			GameImagesInfo info = null;
//			for (int i = 0; i < jsonArray.length(); i++) {
//				JSONObject json = jsonArray.getJSONObject(i);
//				info = gson.fromJson(json.toString(), GameImagesInfo.class);
//				info.imgUrl = MyApplication.HOST + info.imgUrlPosition
//						+ info.imgUrl;
//				list.add(info);
//			}
//		}
//		return list;
//	}

	/**
	 * TODO 1-1 搜索圈子接口
	 * 
	 * @param searchStr
	 * @return
	 * @throws Exception
	 */
	public static List<LoopInfoBean> searchLoop(String minId, String searchStr)
			throws Exception {
		return getNetGet(GET_LOOP_LIST_BY_SEARCH, "minid=" + minId
                + "&circlename=" + searchStr +"&limit=10",LoopInfoBean.class).list;
	}

	/**
	 * TODO 推荐的圈子接口
	 * 
	 * @param minId
	 * @throws Exception
	 */
	public static List<LoopInfoBean> recommendLoop(String minId)
			throws Exception {
		return getNetGet(RECOMMEND_CIRCLE_LIST, "minid=" + minId
                + "&recommend=1",LoopInfoBean.class).list;
	}

	/**
	 * TODO 附近的圈子接口
	 * 
	 * @param minId
	 * @return
	 * @throws Exception
	 */
	public static List<LoopInfoBean> nearLoop(String minId, String xlat,
			String ylong) throws Exception {
        return getNetGet(GET_NEAR_CIRCLE_LIST, "minid=" + minId
                + "&xlat=" + xlat + "&ylong=" + ylong +"&limit=10", LoopInfoBean.class).list;
    }

	/*
	 * 获取我的礼包数据
	 */
	public static List<MyGift> getGiftData(String jsonStr) throws Exception {
		List<MyGift> list = new ArrayList<MyGift>();
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			MyGift myGift = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				myGift = gson.fromJson(json.toString(), MyGift.class);
				myGift.imgUrl = MyApplication.HOST + myGift.imgUrlPosition
						+ myGift.imgUrl;
				list.add(myGift);
			}
		}
		return list;
	}

	/*
	 * 获取好友数据
	 */
	public static List<FriendData> getFriendListData(String jsonString)
			throws Exception {
        return getObjects(FriendData.class, jsonString).list;
    }

	/**
	 * 创建圈子
	 * 
	 * @param  info
	 */
	public static JsonResultBean createLoop(CreateLoopInfo info)
			throws Exception {
		return getNetPost(CREATE_LOOP,readClassAttr(info),CreateLoopInfo.class);
	}

	/**
	 * TODO 加入圈子
	 * 
	 * @param circleid
	 * @param userId
	 * @return message加入怎样的信息，加入成功
	 * @throws Exception
	 */
	public static JsonResultBean joinCircle(String circleid, String userId)
			throws Exception {
		return getNetGet(JOIN_CIRCLE, "circleid=" + circleid
                + "&userid=" + userId,null);
	}

	/**
	 * TODO 关键字搜索朋友
	 * 
	 * @param minid   初始不传或者0
	 * @param searchStr
	 * @return
	 * @throws Exception
	 */
	public static List<FriendData> getAllFriendByUser(String minid,
			String searchStr) throws Exception {
        JsonResultBean bb = getNetGet(GET_ALL_USER_BY_TEMP,
                "minid=" + minid + "&keyword=" + searchStr,FriendData.class);
		return bb.list;
	}

	/*
	 * 获取用户所在圈子列表数据
	 */
	public static List<LoopInfoBean> geLoopListData(String userid)
			throws Exception {
        return getNetGet(GET_MY_LOOP, "userid=" + userid, LoopInfoBean.class).list;
    }

	/**
	 * TODO 查询通讯录
	 * 
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static List<MailListInfo> getMaillist(String userid, String teltype)
			throws Exception {
		List<MailListInfo> list = new ArrayList<MailListInfo>();
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_MAILLIST, "userid=" + userid + "&teltype="
				+ teltype);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			MailListInfo friendData = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				friendData = gson.fromJson(json.toString(), MailListInfo.class);
				list.add(friendData);
			}
		}
		return list;
	}

	/**
	 * 上传通讯录信息
	 * 
	 * @param @param userid
	 * @param @param tel
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: UploadMaillist
	 * @Description: TODO
	 */
	public static boolean UploadMaillist(String userid, String tel)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http
				.httpGet(MyApplication.HOST + MyApplication.UPLOAD_MAIL,
						"userid=" + userid + "&tel=" + tel);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * 邀请好友到圈子
	 * 
	 * @param @param userids
	 * @param @param circleid
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Description: TODO
	 */
	public static JsonResultBean invateLoopFriends(String userids, String circleid)
			throws Exception {
		return getNetGet(INVATE_FRIENDS, "circleid=" + circleid
                + "&userid=" + userids,null);
	}

    /**
     * TODO 邀请朋友来xx公会
     * @throws Exception
     */
	public static JsonResultBean invateTradeFriends(NetInvateBean nib)
			throws Exception {
		return getNetPost(INVATE_TRADE_FRIENDS, readClassAttr(nib), null);
	}

	/**
	 * TODO 添加好友接口
	 * 
	 * @param zid
	 *            自己的id
	 * @param bid
	 *            想添加的对方的id
	 * @param askremark
     * @return 第一个是信息，第二个是是否成功的布尔值 .其实可以用Message这个bean对象
	 * @throws Exception
	 */
	public static JsonResultBean addFriendByUser(String zid, String bid, String askremark)
			throws Exception {
        return getNetGet(ADD_FRIEND_BY_USER, "zid=" + zid + "&bid=" + bid
                + "&askremark=" + askremark, null);
	}

	/**
	 * TODO 9.显示所有请求（加为好友）
	 * 
	 * @param bid
	 * @return
	 * @throws Exception
	 */
	public static List<ApplyInfo> getRequestByUser(String bid) throws Exception {
		return getNetGet(GET_REQUEST_BY_USER,"userid="+bid+"&page=1",ApplyInfo.class).list;
	}

	/**
	 * TODO 10 处理请求（加为好友，拒绝加为好友）
	 * 
	 * @param zid
	 * @param userId
	 * @param solveState
	 * @return 第一个是信息，第二个是是否成功的布尔值,
	 */
	public static JsonResultBean solveFriend(int zid, String userId, int solveState)
			throws Exception {
		return getNetGet(SOLVE_FRIEND, "zid=" + zid + "&bid=" + userId
                + "&solvestate=" + solveState,null);
	}

	/**
	 * 当用户访问指定页面的时候 就给用户绑定新的坐标地址
	 * 
	 * @param @param userid
	 * @param @param xlat
	 * @param @param ylong
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: uploadLocation
	 * @Description: TODO
	 */
	public static boolean uploadLocation(String userid, double xlat,
			double ylong) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.UPLOAD_LOCATION, "xlat=" + xlat + "&ylong="
				+ ylong + "&userid=" + userid);
		JSONObject obj = new JSONObject(jsonString);
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * 获取周围附近的用户
	 * 
	 * @param @param userid
	 * @param @param xlat
	 * @param @param ylong
	 * @param @return
	 * @param @throws Exception
	 * @return List<FriendData>
	 * @Title: getNearPeopleList
	 * @Description: TODO
	 */
    public static List<FriendData> getNearPeopleList(String userid,
                                                     double xlat, double ylong) throws Exception {
        return getNetGet(NEAR_PEOPLE_LIST, "xlat=" + xlat + "&ylong="
                + ylong + "&userid=" + userid, FriendData.class).list;
    }

	/**
	 * 用户自定义修改自己的被搜索的状态，1为可以被搜索到，0为搜索不到
	 * 
	 * @param @param userid
	 * @param @param islook
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: updateLocationState
	 * @Description: TODO
	 */
	public static boolean updateLocationState(String userid, int islook)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.UPDATE_LOCATION_STATE, "islook=" + islook
				+ "&userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonString);
		if (jsonObject.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * TODO 创建公会
	 * 
	 * @param upBean
	 * @return
	 * @throws Exception
	 */
	public static JsonResultBean createSociaty(SociatyBean upBean) throws Exception {
		return getNetPost(CREATE_SOCIATY,readClassAttr(upBean),SociatyBean.class);
	}

	/**
	 * 修改公会资料
	 * 
	 * @param @param objs
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 * @Title: updateSociaty
	 * @Description: TODO
	 */
	public static JsonResultBean updateSociaty(SociatyBean sb) throws Exception {
		return getNetPost(UPDATE_SOCIATY_INFO,readClassAttr(sb),SociatyBean.class);
	}

	/**
	 * @param @param objs
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 * @Title: updateSociaty
	 * @Description: TODO 22 修改圈子,目前无返回整个圈子信息
	 */
	public static Object[] updateLoop(Object objs) throws Exception {
		LoopInfoBean cinfo = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(objs);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("circleJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		String result = http.httpPost(MyApplication.HOST
				+ MyApplication.UPDATE_CIRCLE, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		// if (obj.getBoolean("isSucess")) {
		// JSONObject json = obj.getJSONObject("result");
		// cinfo = gson.fromJson(json.toString(), LoopInfoBean.class);
		// }
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess };
		return objects;
	}

	/**
	 * TODO 15-1模糊查询公会
	 * 
	 * @param minId
	 * @param searchStr
	 * @return
	 * @throws Exception
	 */
	public static List<SociatyBean> searchGuild(String minId, String searchStr)
			throws Exception {
        return getNetGet(SEARCH_GUILD_LIST, "minid=" + minId + "&guildname=" + searchStr, SociatyBean.class).list;
    }

	/**
	 * TODO 15.得到公会集合，可被重复调用
	 * 
	 * @param jsonString
	 * @return
	 * @throws JSONException
	 */
	private static List<SociatyBean> getSociatyBeans(String jsonString)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GUILD_LIST_BY_TEMP, jsonString);

		List<SociatyBean> list = new ArrayList<SociatyBean>();
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			SociatyBean bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				bean = gson.fromJson(json.toString(), SociatyBean.class);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * TODO 15-2 查询自己属于的公会（类型Long）
	 * 
	 * @param minId
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static List<SociatyBean> searchMyGuild(String minId, int userid)
			throws Exception {
        return getNetGet(MY_GUILD_LIST, "minid=" + minId + "&userid=" + userid, SociatyBean.class).list;
    }

	/**
	 * TODO 15-3 推荐的公会（类型int）
	 * 
	 * @param minId
	 * @return
	 * @throws Exception
	 */
	public static List<SociatyBean> searchRecommendGuild(String minId)
			throws Exception {
		return getNetGet(RECOMMEND_GUILD_LIST,"minid=" + minId + "&recommend=" + 1,SociatyBean.class).list;
	}

	/**
	 * TODO 15-4 单个查询某个公会详情的信息(Long)
	 * 
	 * @param guildid
	 * @return
	 * @throws Exception
	 */
	public static SociatyBean searchThisGuild(String guildid) throws Exception {
        return (SociatyBean) getNetGet(GET_GUILD,
                "guildid=" + guildid,SociatyBean.class).oneBean();
	}

	/**
	 * TODO 15-5 (1-5)单个查询某个圈子的信息
	 * 
	 * @param circleid
	 * @return
	 * @throws Exception
	 */
	public static LoopInfoBean getCircle(String circleid) throws Exception {
		return (LoopInfoBean) getNetGet(GET_CIRCLE, "circleid=" + circleid,LoopInfoBean.class).oneBean();
	}

	/**
	 * TODO 16.申请加入公会
	 * 
	 * @param userId
	 * @param guildid
	 * @param askremark
	 * @return
	 * @throws Exception
	 */
	public static JsonResultBean askForGuild(String userId, int guildid,
			String askremark) throws Exception {
		return getNetGet(ASK_FOR_GUILD, "userid=" + userId + "&guildid="
                + guildid + "&askremark=" +
                askremark,null);
	}

	/**
	 * TODO 19，获取公会成员 ，圈子成员
	 * 还有俩参数minid和limit数量，这里没传
	 * @param guildid
	 * @return
	 * @throws Exception
	 */
	public static List<FriendData> getUserByGuildMem(int guildid,String tag) throws Exception {
        if (tag.equals("Loop"))
            return getNetGet(GET_USER_BY_LOOP,"circleid="+ guildid,FriendData.class).list;
        else
            return getNetGet(GET_USER_BY_GUILD,"guildid="+ guildid,FriendData.class).list;
    }

	/**
	 * 退出公会
	 * 
	 * @param guildid 公会id
	 * @param  userid 用户id
	 */
    public static JsonResultBean exitSociaty(int guildid, String userid)
            throws Exception {
        return getNetGet(EXIT_SOCIATY, "guildid=" + guildid + "&userid="
                + userid, null);
    }

    /**
     * 解散公会（只有会长）
     *
     * @param guildid 公会id
     * @param  userid 用户id
     */
    public static JsonResultBean dissolveguild(int guildid, String userid)
            throws Exception {
        return getNetGet(DISSOLVE_SOCIATY, "guildid=" + guildid + "&userid="
                + userid, null);
    }

	/**
	 * 创建公会活动
	 * 
	 * @param @param objs
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 * @Title: createSociaty
	 * @Description: TODO
	 */
	public static JsonResultBean createSociatyAct(SociatyActBean bean) throws Exception {
		return getNetPost(CREATE_SOCIATY_ACT,readClassAttr(bean),SociatyActBean.class);
	}

	/**
	 * 查询公会活动
	 * 
	 * @param guildid
	 * @param minid
	 * @return
	 * @return List<SociatyActBean>
	 * @Title: getSociatyActList
	 * @Description: TODO
	 */
	public static List<SociatyActBean> getSociatyActList(int guildid,
			String minid) throws Exception {
		return getNetGet(SELECT_SOCIATY_ACT, "guildid=" + guildid
                + "&minId=" + minid,SociatyActBean.class).list;
	}

	/**
	 * 获取公会申请列表
	 * 
	 * @param @param userid
	 * @param @return
	 * @param @throws Exception
	 * @return List<SociatyApplyInfo>
	 * @Title: getSociatyApplyInfoList
	 * @Description: TODO
	 */
	public static List<ApplyInfo> getSociatyApplyInfoList(String userid)
			throws Exception {
		return getNetGet(SELECT_SOCIATY_APPLYLIST, "userid=" + userid,ApplyInfo.class).list;
	}

	/**
	 * 用户查询自己被邀请公会的所有记录
	 * 
	 * @Title: getInvateList
	 * @Description: TODO
	 * @param @param userid
	 * @param @return
	 * @param @throws Exception
	 * @return List<ApplyInfo>
	 */
	public static List<ApplyInfo> getInvateList(String userid) throws Exception {
        return getNetGet(GET_INVAITE_SOCIATY, "userid=" + userid, ApplyInfo.class).list;
    }

	/**
	 * 处理别人来申请公会的消息
	 * @param solveState 1为接受，2为拒绝
	 * @param msgid
     * @return boolean
	 * @Title: handleSociatyApplyInfo
	 * @Description: TODO
	 */
	public static JsonResultBean handleSociatyApplyInfo(int solveState, String msgid) throws Exception {
		return getNetGet(HANDLE_SOCIATY_APPLYINFO,  "state=" + solveState+"&ugid=" + msgid,null);
	}

	/**
	 * 用户处理公会邀请，接受或者拒绝
	 * 
	 * @Title: handleInvateInfo
     * @param solveState 1为通过 2为拒绝
     * @param msgid   这条信息的id
     */
	public static JsonResultBean handleInvateInfo( int solveState, String msgid) throws Exception {
		return getNetGet(HANDLE_INVATE_SOCIATY, "state=" + solveState+"&ugid=" + msgid,null);
	}

	/**
	 * TODO 退出圈子
	 * 
	 * @param circleid
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static Object exitLoop(int circleid, int userid) throws Exception {
		return getNetGet(EXIT_CIRCLE, "circleid=" + circleid
                + "&userid=" + userid,null);
	}

	/**
	 * TODO 11 完善用户资料，修改用户资料
	 * 
	 * @return
	 * @throws Exception
	 */
	public static JsonResultBean updateUser(UserInfo user) throws Exception {
//		String circleJson = gson.toJson(objs);
//		List<Parameter> params = new ArrayList<Parameter>();
//		Parameter par = new Parameter("userJson", circleJson);
//		params.add(par);
        return getNetPost(UPDATE_UPDATE_USER, readClassAttr(user), UserInfo.class);
    }

	/**
	 * 获取用户群组信息
	 * 
	 * @param @param userid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Groups>
	 * @Title: getGroups
	 * @Description: TODO
	 */
	public static List<Groups> getGroups(String userid) throws Exception {
		List<Groups> list = new ArrayList<Groups>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GROUPS, "userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Groups bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				bean = gson.fromJson(json.toString(), Groups.class);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * TODO 23 转让公会
	 * 
	 * @param guildid
	 *            公会ID
	 * @param userid
	 *            转让的人
	 * @param nextuserid （新会长）的ID
     * @return
	 * @throws Exception
	 */
	public static JsonResultBean transferGuild(String guildid, String userid, String nextuserid)
			throws Exception {
		return getNetGet(TRANSFER_GUILD, "guildid=" + guildid
                + "&userid=" + userid + "&nextuserid=" + nextuserid, null);
    }
	
	/**
	 * 创建房间失败后需要进行回滚
	 * @Title: delteOpenfire 
	 * @Description: TODO
	 * @param @param path
	 * @param @param name
	 * @param @param id
	 * @param @return
	 * @param @throws Exception     
	 * @return Object[]
	 */
	public static Object[] delteOpenfire(String path,String name, String id)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(path, name+"=" + id);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		return new Object[] { msg, isSucess };
	}

    /**
     * TODO  10、搜索界面的，感兴趣游戏接口（相当于推荐的游戏）
     * @return
     * @throws Exception
     */
    public static List<GameInfo> getGameIntersting() throws Exception{
        List<GameInfo> list = new ArrayList<GameInfo>();
        SyncHttp http = new SyncHttp();
        String jsonStr = http.httpGet(
                MyApplication.HOST
                        + MyApplication.RECOMMEND_GAMELIST, "type=" + 2);
        JSONObject jsonObject = new JSONObject(jsonStr);
        JSONObject obj = jsonObject.getJSONObject("data");
        if (obj.getBoolean("isSucess")) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            JSONArray jsonArray = obj.getJSONArray("result");
            GameInfo info = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                info = gson.fromJson(json.toString(), GameInfo.class);
                info.gamemainimage = MyApplication.HOST + info.gamemainimage;
                list.add(info);
            }
        }
        return list;
    }
    
    /**
     * 修改密码
     * @Title: chagePass 
     * @Description: TODO
     * @param @param id
     * @param @param oldpass
     * @param @param newpass
     * @param @return
     * @param @throws Exception     
     * @return Object[]
     */
    public static JsonResultBean chagePass(String id,String oldpass, String newpass)
			throws Exception {
        return getNetGet(CHANGE_PASS, "userid=" + id + "&userpwd=" + oldpass + "&userpwdnew=" + newpass, null);
    }
    
}
