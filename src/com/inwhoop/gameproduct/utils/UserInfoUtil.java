package com.inwhoop.gameproduct.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.UserInfo;

import java.lang.reflect.Field;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project:
 * @Title:
 * @Package com.inwhoop.zhixin.util
 * @Description: TODO
 * @date 2014/4/11   17:50
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class UserInfoUtil {

    public static void rememberUserInfo(Context context,UserInfo userInfo) {
        SharedPreferences setting = context.getSharedPreferences(
                MyApplication.SETTING_INFO, 0);
        Field[] fields = userInfo.getClass().getDeclaredFields();

        SharedPreferences.Editor spEtor = setting.edit();
        for (Field field : fields) {
            field.setAccessible(true);
            String name = field.getName();
            String va = "";
            try {
                va = field.get(userInfo) + "";
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            spEtor = spEtor.putString(name, va);
        }
//        spEtor = spEtor.putString("pic", userInfo.pic);
        spEtor.commit();

    }

    public static UserInfo getUserInfo(Context context) {
        UserInfo userInfo = new UserInfo();
        SharedPreferences setting = context.getSharedPreferences(
                MyApplication.SETTING_INFO, 0);
//        userInfo.pic = setting.getString("pic", "");
        try {
            Field[] fields = userInfo.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String name = field.getName();
//                String va = field.get(userInfo).toString();
                field.set(userInfo, setting.getString(name, ""));

            }
        } catch (Exception e) {
            LogUtil.e("excaption,得到用户：" + e.toString());
        }
        return userInfo;
    }

    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(MyApplication.SETTING_INFO,
                Context.MODE_PRIVATE);
        sp.edit().clear().commit();
    }
}
