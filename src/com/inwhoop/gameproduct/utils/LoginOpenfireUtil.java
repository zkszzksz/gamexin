package com.inwhoop.gameproduct.utils;

import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;
import com.inwhoop.gameproduct.xmpp.XmppConnectionUtil;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

/**  
 * 
 * 登录openfire
 * @Project: MainActivity
 * @Title: LoginOpenfireUtil.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-17 上午9:48:48
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class LoginOpenfireUtil {
	
	private static Context mcontext = null;

	/**
	 * 登录openfire
	 * @Title: loginOpenfire 
	 * @Description: TODO
	 * @param      
	 * @return void
	 */
	public static void loginOpenfire(final Context context){
		mcontext = context;
		if("".equals(UserInfoUtil.getUserInfo(context).id)){
			return;
		}
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Message msg = new Message();
				try{
					UserInfo info = UserInfoUtil.getUserInfo(context);
					XmppConnectionUtil.getInstance().login(info.chatId.toLowerCase(), info.huanxinpwd);
					msg.obj = context;
					handler.sendEmptyMessage(0);
				}catch (Exception e){
					
				}
			}
		}).start();
	}
	
	public static Handler handler = new Handler(){
		public void handleMessage(Message msg) {
			ClienConServer.getInstance(mcontext);
			ClientGroupServer.getInstance(mcontext);
		};
	};
	
}
