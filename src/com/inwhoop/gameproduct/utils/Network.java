package com.inwhoop.gameproduct.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 检测网络连接
 * 
 * @Project: GameProduct
 * @Title: Network.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-2 上午10:32:04
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class Network {

	/** 网络不可用 */
	public static final int NONETWORK = 0;
	/** 是wifi连接 */
	public static final int WIFI = 1;
	/** 不是wifi连接 */
	public static final int NOWIFI = 2;

	/**
	 * 检验网络连接 并判断是否是wifi连接
	 * 
	 * @Title: checkNetWorkType
	 * @Description: TODO
	 * @param  context 上下文对象
	 * @return int <li>没有网络：Network.NONETWORK;</li> <li>wifi 连接：Network.WIFI;</li>
	 *         <li>mobile 连接：Network.NOWIFI</li>
	 */
	public static int checkNetWorkType(Context context) {

		if (!checkNetWork(context)) {
			return Network.NONETWORK;
		}
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.isConnectedOrConnecting())
			return Network.WIFI;
		else
			return Network.NOWIFI;
	}

	/**
	 * 检测网络是否连接
	 * 
	 * @Title: checkNetWork
	 * @Description: TODO
	 * @param  context 上下文对象
	 * @return boolean <li>true：连接</li><li> false：未连接</li>
	 */
	public static boolean checkNetWork(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm == null) {
			return false;
		}
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null || !ni.isAvailable()) {
			return false;
		}
		return true;
	}
}
