package com.inwhoop.gameproduct.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 监听键盘，隐藏软键盘
 *
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: KeyBoard.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * @date 2014-4-2 上午10:29:39
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class KeyBoard {
    /**
     * 隐藏软键盘
     *
     * @param @param context
     *
     * @return void
     *
     * @Title: HiddenInputPanel
     * @Description: TODO
     */
    public static void HiddenInputPanel(Context context) {
        final View v = ((Activity) context).getWindow().peekDecorView();
        if (v != null && v.getWindowToken() != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    /**
     * 显示软键盘  //不线程的话键盘弹不出
     *
     * @param editText
     */
    public static void showKeyBoard(final EditText editText) {
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(200);
                    InputMethodManager inputManager = (InputMethodManager) editText.getContext()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.showSoftInput(editText, 0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
