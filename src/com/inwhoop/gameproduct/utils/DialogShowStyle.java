package com.inwhoop.gameproduct.utils;

import android.app.ProgressDialog;
import android.content.Context;
import com.inwhoop.gameproduct.view.CustomProgressDialog;

/**
 * Created by LONG on 14-1-13.
 */
public class DialogShowStyle {
    private Context context;
    private String content;
    private CustomProgressDialog progressDialog;

    public DialogShowStyle(Context context, String content) {
        this.context = context;
        this.content = content;
    }

    public void dialogShow() {
        if (progressDialog == null) {
            progressDialog = CustomProgressDialog.createDialog(context);
        }
        progressDialog.setMessage(content);
        progressDialog.setCancelable(true);
        progressDialog.show();
        // progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void dialogDismiss() {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
