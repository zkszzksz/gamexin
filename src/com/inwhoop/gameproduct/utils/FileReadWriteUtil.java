package com.inwhoop.gameproduct.utils;

import org.apache.http.util.EncodingUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: zhixin_2.0
 * @Title:
 * @Package com.inwhoop.zhixin.util
 * @Description: TODO
 * @date 2014/4/24   11:01
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class FileReadWriteUtil {

    public static void write2SDFromInput2(String path,String fileName,String input){
        OutputStream output = null;
        try{
            File paths = new File(path);
            String filepath = path + fileName;
            File file = new File(filepath);
            if (!paths.exists()) {
                paths.mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            }

            output = new FileOutputStream(file);
            output.write(input.getBytes());
            output.flush();

        }catch(Exception e){ e.printStackTrace(); }
        finally{
            try{
                output.close();
            }catch(Exception e){ e.printStackTrace(); } }
        //return file;
    }

    //读SD中的文件
    public static String readFileSdcardFile(String path, String fileName){
        String res="";
        try{
            FileInputStream fin = new FileInputStream(path+fileName);
            int length = fin.available();
            byte [] buffer = new byte[length];
            fin.read(buffer);
            res = EncodingUtils.getString(buffer, "UTF-8");
            fin.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return res;
    }
}
