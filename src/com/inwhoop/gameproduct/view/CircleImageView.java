package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * TODO 重写imageView 使其显示为圆形
 * 
 */
public class CircleImageView extends ImageView {

	Path path;
	public PaintFlagsDrawFilter mPaintFlagsDrawFilter;// ë�߹���
	Paint paint;

	public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init();
	}

	public CircleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init();
	}

	public CircleImageView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init();
	}

	public void init() {
		mPaintFlagsDrawFilter = new PaintFlagsDrawFilter(0,
				Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setColor(Color.WHITE);

	}

	@Override
	protected void onDraw(Canvas cns) {
		// TODO Auto-generated method stub
		/*
		 * float h = getMeasuredHeight()- 3.0f; float w = getMeasuredWidth()-
		 * 3.0f; if (path == null) { path = new Path(); path.addCircle( w/2.0f ,
		 * h/2.0f , (float) Math.min(w/2.0f, (h / 2.0)) , Path.Direction.CCW);
		 * path.close(); } cns.drawCircle(w/2.0f, h/2.0f, Math.min(w/2.0f, h /
		 * 2.0f) + 1.5f, paint); int saveCount = cns.getSaveCount(); cns.save();
		 * cns.setDrawFilter(mPaintFlagsDrawFilter);
		 * cns.clipPath(path,Region.Op.REPLACE);
		 * cns.setDrawFilter(mPaintFlagsDrawFilter); cns.drawColor(Color.WHITE);
		 * super.onDraw(cns); cns.restoreToCount(saveCount); }
		 */
		// 法2
		Drawable drawable = getDrawable();
		if (null != drawable) {
			Bitmap bitmap = drawableToBitmap(drawable); // 但是此法又显示图像过小：原图大些，显示后只有原图的左上角部分
			// Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
			// //此法报转型异常

			Bitmap b = toRoundCorner(bitmap, 14);
			// final Rect rect = new Rect(0, 0, b.getWidth(), b.getHeight());
			final Rect rect = new Rect(0, 0, b.getWidth(), b.getHeight());
			Rect rect1 = new Rect(0, 0, this.getWidth(), this.getHeight());
			paint.reset();
			cns.drawBitmap(b, rect, rect1, paint);

		} else {
			super.onDraw(cns);
		}
	}

	public static Bitmap drawableToBitmap(Drawable drawable) {

		Bitmap bitmap = Bitmap
				.createBitmap(
						drawable.getIntrinsicWidth(), // 原方法宽高写的100固定，则最后的显示图像不全
						drawable.getIntrinsicHeight(),
						drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
								: Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		// canvas.setBitmap(bitmap);
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight());
		drawable.draw(canvas);
		return bitmap;
	}

	private Bitmap toRoundCorner(Bitmap bitmap, int pixels) {
		Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(outBitmap);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPX = bitmap.getWidth() / 2 < bitmap.getHeight() / 2 ? bitmap
				.getWidth() : bitmap.getHeight();
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPX, roundPX, paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return outBitmap;
	}
}