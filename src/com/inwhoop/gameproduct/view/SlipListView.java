package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.utils.LogUtil;

/**
 * @Describe: TODO  左滑Listview的item实现删除。如果设置Listview的layout_height="match_parent"会报错bug，所以要用wrap
 *              TODO 用match_parent，第一次滑出删除按钮后，点击Listview的空白无item区域，报错异常，应该是没有执行到以下5种选择，没有加载到组件
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/08 10:56
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */

public class SlipListView extends ListView {

    private static final String TAG = SlipListView.class.getSimpleName();

    private boolean isShown = false;

    private View mPreItemView;

    private View mCurrentItemView;

    private float mFirstX;

    private float mFirstY;

    private boolean mIsHorizontal;

    public SlipListView(Context context) {
        super(context);
    }

    public SlipListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SlipListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        float lastX = ev.getX();
        float lastY = ev.getY();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:

                mIsHorizontal = false;

                mFirstX = lastX;
                mFirstY = lastY;
                int motionPosition = pointToPosition((int) mFirstX, (int) mFirstY);

                LogUtil.e(TAG + " onInterceptTouchEvent----->ACTION_DOWN position=" + motionPosition);

                if (motionPosition >= 0 ) {
                    View currentItemView = getChildAt(motionPosition - getFirstVisiblePosition());
                    mPreItemView = mCurrentItemView;  // 2  2在先：滑动item则隐藏当前item，但第一次进，报空指针
                    mCurrentItemView = currentItemView; //1  此俩句对调顺序：1在先，不会报错，实现不了滑动另外item则隐藏已有弹出的删除按钮
                }
                break;

            case MotionEvent.ACTION_MOVE:
                float dx = lastX - mFirstX;
                float dy = lastY - mFirstY;

                if (Math.abs(dx) >= 5 && Math.abs(dy) >= 5) {
                    return true;
                }
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:

                LogUtil.i(TAG + "onInterceptTouchEvent----->ACTION_UP");
                if (isShown && mPreItemView != mCurrentItemView) {
                    LogUtil.i(TAG + "1---> hiddenRight");
                    /**
                     * 情况一：
                     * <p>
                     * 一个Item的右边布局已经显示，
                     * <p>
                     * 这时候点击任意一个item, 那么那个右边布局显示的item隐藏其右边布局
                     */
                    hiddenRight(mPreItemView);
                }
                break;
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // TODO Auto-generated method stub
        float lastX = ev.getX();
        float lastY = ev.getY();

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                LogUtil.i(TAG + "---->ACTION_DOWN");
                break;

            case MotionEvent.ACTION_MOVE:
                float dx = lastX - mFirstX;
                float dy = lastY - mFirstY;

                mIsHorizontal = isHorizontalDirectionScroll(dx, dy);

                if (!mIsHorizontal) {
                    break;
                }

                LogUtil.i(TAG + " onTouchEvent ACTION_MOVE");

                if (mIsHorizontal) {
                    if (isShown && mPreItemView != mCurrentItemView) {
                        LogUtil.i(TAG + "2---> hiddenRight");
                        /**
                         * 情况二：
                         * <p>
                         * 一个Item的右边布局已经显示，
                         * <p>
                         * 这时候左右滑动另外一个item,那个右边布局显示的item隐藏其右边布局
                         * <p>
                         * 向左滑动只触发该情况，向右滑动还会触发情况五
                         */
                        hiddenRight(mPreItemView);
                    }
                } else {
                    if (isShown) {
                        LogUtil.i(TAG + "3---> hiddenRight");
                        /**
                         * 情况三：
                         * <p>
                         * 一个Item的右边布局已经显示，
                         * <p>
                         * 这时候上下滚动ListView,那么那个右边布局显示的item隐藏其右边布局
                         */
                        hiddenRight(mPreItemView);
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                LogUtil.i(TAG + "============ACTION_UP");
                if (isShown) {
                    LogUtil.i(TAG + "4---> hiddenRight");
                    /**
                     * 情况四：
                     * <p>
                     * 一个Item的右边布局已经显示，
                     * <p>
                     * 这时候左右滑动当前一个item,那个右边布局显示的item隐藏其右边布局
                     */
                    hiddenRight(mPreItemView);
                }

                if (mIsHorizontal) {
                    if (mFirstX - lastX > 30) {
                        showRight(mCurrentItemView);
                    } else {
                        LogUtil.i(TAG + "5---> hiddenRight");
                        /**
                         * 情况五：
                         * <p>
                         * 向右滑动一个item,且滑动的距离超过了右边View的宽度的一半，隐藏之。
                         */
                        hiddenRight(mCurrentItemView);
                    }
                    return true;
                }
                break;
        }

        return super.onTouchEvent(ev);
    }

    private void showRight(View rightView) {
        View rl_right = (View) rightView.findViewById(R.id.item_right);
        rl_right.setVisibility(View.VISIBLE);

        isShown = true;
    }

    private void hiddenRight(View rightView) {

        View rl_right = (View) rightView.findViewById(R.id.item_right);
        rl_right.setVisibility(View.GONE);

        isShown = false;
    }

    /**
     * @param dx
     * @param dy
     *
     * @return judge if can judge scroll direction
     */
    private boolean isHorizontalDirectionScroll(float dx, float dy) {
        boolean mIsHorizontal = true;

        if (Math.abs(dx) > 30 && Math.abs(dx) > 2 * Math.abs(dy)) {
            mIsHorizontal = true;
            System.out.println("mIsHorizontal---->" + mIsHorizontal);
        } else if (Math.abs(dy) > 30 && Math.abs(dy) > 2 * Math.abs(dx)) {
            mIsHorizontal = false;
            System.out.println("mIsHorizontal---->" + mIsHorizontal);
        }

        return mIsHorizontal;
    }

}

