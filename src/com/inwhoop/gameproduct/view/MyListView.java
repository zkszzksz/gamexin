package com.inwhoop.gameproduct.view;

import com.inwhoop.gameproduct.R;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MyListView extends ListView implements OnScrollListener {

	private LayoutInflater inflater = null;

	private View footView = null;

	private ImageView imageView;

	private TextView textView;

	private ProgressBar progressbar;

	private int startY; //

	private boolean isRecord = false;

	private OnRefreshListener refreshListener;

	private int padLeft;

	private int padRight;

	private int lastItem;

	private int footViewHeight; // 底部view的高度

	private boolean isFinalItem = false; // 是否到达底部

	private boolean isFristItem = false; // 是否到达顶部

	public boolean isRefreshable = true; // 是否可以刷新

	private boolean isBack = false;

	public boolean isOver = false;

	public boolean isComplete;

	private RotateAnimation animation, reverseAnimation; // 箭头变化的动画
	private static final float RATIO = 3;// 用来设置实际间距和上边距之间的比例
	// 表示状态的具体值
	private static final int DONE = 0; // 表示什么都不做
	private static final int PULL_TO_REFRESH = 1; // 表示下拉刷新
	private static final int RELEASE_TO_REFRESH = 2; // 表示松开后刷新
	private static final int REFRESHING = 3; // 表示正在刷新
	public int state = DONE; // 表示状态

	
	private final static int NONE_PULL_REFRESH = 4; // 正常状态
	private final static int ENTER_PULL_REFRESH = 5; // 进入下拉刷新状态
	private final static int OVER_PULL_REFRESH = 6; // 进入松手刷新状态
	private final static int EXIT_PULL_REFRESH = 7; // 松手后反弹后加载状态

	public MyListView(Context context) {
		super(context);
		init(context);
	}

	public MyListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);

	}

	public MyListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public void setAdapter(BaseAdapter adapter) {
		super.setAdapter(adapter);
	}

	public void setRefreshListener(OnRefreshListener refreshListener) {
		this.refreshListener = refreshListener;
	}

	public RotateAnimation getAnimation() {
		animation = new RotateAnimation(0, -180,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		animation.setInterpolator(new LinearInterpolator());
		animation.setDuration(250);
		animation.setFillAfter(true);
		return animation;
	}

	public RotateAnimation getReverseAnimation() {

		reverseAnimation = new RotateAnimation(-180, 0,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		reverseAnimation.setInterpolator(new LinearInterpolator());
		reverseAnimation.setDuration(250);
		reverseAnimation.setFillAfter(true);
		return reverseAnimation;

	}

	private void init(Context context) {

		setOnScrollListener(this);
		inflater = LayoutInflater.from(context);
		footView = (LinearLayout) inflater.inflate(R.layout.foot_layout, null);
		imageView = (ImageView) footView.findViewById(R.id.foot_arrowImageView);
		textView = (TextView) footView.findViewById(R.id.fresh_hint);
		progressbar = (ProgressBar) footView
				.findViewById(R.id.head_progressBar);
		measureView(footView);
		footViewHeight = footView.getMeasuredHeight();
		padRight = 0;
		padLeft = (getWidth() - footView.getMeasuredWidth()) / 3;
		footView.setPadding(padLeft, 0, padRight, -1 * footViewHeight);
		footView.invalidate();
		addFooterView(footView);
		animation = new RotateAnimation(0, -180,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		animation.setInterpolator(new LinearInterpolator());
		animation.setDuration(250);
		animation.setFillAfter(true);
		reverseAnimation = new RotateAnimation(-180, 0,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		reverseAnimation.setInterpolator(new LinearInterpolator());
		reverseAnimation.setDuration(250);
		reverseAnimation.setFillAfter(true);
	}

	public interface OnRefreshListener {
		public void onRefresh();
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		lastItem = totalItemCount;
		if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
			isFinalItem = true;
		} else {
			isFinalItem = false;
		}

		if (firstVisibleItem == totalItemCount) {
			isFristItem = true;
		} else {
			isFristItem = false;
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (isRefreshable && !isComplete) {
			switch (ev.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (isFinalItem && !isRecord) {
					startY = (int) ev.getY();
					isRecord = true;
				}
				break;
			case MotionEvent.ACTION_MOVE:
				int tempY = (int) ev.getY();
				if (isFinalItem && !isRecord) {
					// System.out.println("=====(startY - tempY)======="+(startY
					// - tempY));
					startY = tempY;
					isRecord = true;
				}
				if (state != REFRESHING && isRecord) {
					// if (state == RELEASE_To_REFRESH) {
					if (state == RELEASE_TO_REFRESH) {
						setSelection(lastItem);

						if (((startY - tempY) / RATIO < footViewHeight)
								&& (startY - tempY) > 0) {
							state = PULL_TO_REFRESH;
							changeFootviewState();
						}
						// 一上子推到顶了
						else if (startY - tempY <= 0) {
							state = DONE;
							changeFootviewState();

						} else {

						}
					}
					if (state == PULL_TO_REFRESH) {

						setSelection(lastItem);

						if ((startY - tempY) / RATIO >= footViewHeight) {
							state = RELEASE_TO_REFRESH;
							isBack = true;
							changeFootviewState();

						} else if (startY - tempY <= 0) {
							state = DONE;
							changeFootviewState();
						}
					}

					if (state == DONE) {
						if (startY - tempY > 0) {
							state = PULL_TO_REFRESH;
							changeFootviewState();
						}
					}

					if (state == PULL_TO_REFRESH) {
						footView.setPadding(padLeft, 0, padRight, (int) (-1
								* footViewHeight + (startY - tempY) / RATIO));

					}

					if (state == RELEASE_TO_REFRESH) {
						footView.setPadding(
								padLeft,
								0,
								padRight,
								(int) ((startY - tempY) / RATIO - footViewHeight));
					}

				}
				break;
			case MotionEvent.ACTION_UP:
				if (state != REFRESHING) {
					if (state == DONE) {
						// 什么都不做
					}
					if (state == PULL_TO_REFRESH) {
						state = DONE;
						changeFootviewState();

						// System.out.println("state:"+state+"由上拉刷新状态，到done状态");
					}
					if (state == RELEASE_TO_REFRESH) {
						state = REFRESHING;
						changeFootviewState();
						onRefresh();
						// System.out.println("state:"+state+"由松开刷新状态，到done状态");
					}
				}
				isRecord = false;
				isBack = false;
				break;
			default:
				break;

			}
		}
		return super.onTouchEvent(ev);
	}

	private void changeFootviewState() {
		switch (state) {
		case DONE:
			setSelection(lastItem);
			footView.setPadding(padLeft, 0, padRight, -1 * footViewHeight);
			progressbar.setVisibility(View.GONE);
			imageView.setVisibility(View.VISIBLE);
			imageView.clearAnimation();
			imageView.setImageResource(R.drawable.arrow);
			textView.setText("上拉刷新");
			textView.setVisibility(View.VISIBLE);

			break;

		case PULL_TO_REFRESH:
			setSelection(lastItem);
			textView.setText("上拉刷新");
			imageView.clearAnimation();
			if (isBack) {
				imageView.clearAnimation();
				imageView.setAnimation(getReverseAnimation());
				isBack = false;
			}

			break;

		case RELEASE_TO_REFRESH:
			setSelection(lastItem);
			textView.setText("松开刷新");
			imageView.clearAnimation();
			imageView.setAnimation(getAnimation());
			break;
		case REFRESHING:
			setSelection(lastItem);
			textView.setText("正在加载...");
			imageView.clearAnimation();
			imageView.setVisibility(View.GONE);
			progressbar.setVisibility(View.VISIBLE);
			footView.setPadding(padLeft, 0, padRight, 0);
			break;

		default:
			break;
		}
	}

	private void measureView(View child) {
		ViewGroup.LayoutParams p = child.getLayoutParams();
		if (p == null) {
			p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
		int lpHeight = p.height;
		int childHeightSpec;
		if (lpHeight > 0) {
			childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight,
					MeasureSpec.EXACTLY);
		} else {
			childHeightSpec = MeasureSpec.makeMeasureSpec(0,
					MeasureSpec.UNSPECIFIED);
		}
		child.measure(childWidthSpec, childHeightSpec);
	}

	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			state = DONE;
			changeFootviewState();
		}

	};

	public void onRefreshComplete() {
		state = DONE;
		changeFootviewState();
	}

	private void onRefresh() {
		if (refreshListener != null) {
			refreshListener.onRefresh();
		}
	}

}
