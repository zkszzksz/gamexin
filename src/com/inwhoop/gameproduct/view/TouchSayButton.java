package com.inwhoop.gameproduct.view;

import java.io.File;
import java.io.IOException;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.utils.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

/**
 * 
 * @Project: MainActivity
 * @Title: TouchSayButton.java
 * @Package com.inwhoop.gameproduct.view
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-6-14 下午1:46:34
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class TouchSayButton extends Button {

	private PopupWindow popupWindow = null;

	private Context context;

	private ImageView showImageView = null;

	private boolean isTouch = false;

	private int count = 0;

	private int[] voices = { R.drawable.amp1, R.drawable.amp2, R.drawable.amp3,
			R.drawable.amp4, R.drawable.amp5, R.drawable.amp6 };

	private OnRecordListener onRecordListener = null;
	/** 用于完成录音 */
	private MediaRecorder mRecorder = null;

	private long startRecordTime = 0; // 录音开始时间

	private String path = "";

	public TouchSayButton(Context context) {
		super(context);
		this.context = context;
	}

	public TouchSayButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		initPopwindow();
	}

	public TouchSayButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	public OnRecordListener getOnRecordListener() {
		return onRecordListener;
	}

	public void setOnRecordListener(OnRecordListener onRecordListener) {
		this.onRecordListener = onRecordListener;
	}

	@SuppressLint("ShowToast")
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			if (ev.getAction() == MotionEvent.ACTION_DOWN)
				Toast.makeText(context, "sd卡不存在，不能进行语音发送操作", 0).show();
		} else {
			switch (ev.getAction()) {
			case MotionEvent.ACTION_DOWN:
				isTouch = true;
				showPopupWindow();
				setBackgroundResource(R.drawable.btn_voice_pressed);
				setText("松开发送");
				setTextColor(getResources().getColor(R.color.white));
				break;

			case MotionEvent.ACTION_UP:
				if (null != popupWindow) {
					popupWindow.dismiss();
				}
				setBackgroundResource(R.drawable.btn_voice);
				isTouch = false;
				setText("按住说话");
				setTextColor(getResources().getColor(R.color.green));
				onRecord(path, stopVoice(startRecordTime) / 1000);
				break;

			default:
				break;
			}
		}
		return super.onTouchEvent(ev);
	}

	/**
	 * 初始化popupwindow
	 * 
	 * @Title: initPopwindow
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	@SuppressWarnings("deprecation")
	private void initPopwindow() {
		View view = LayoutInflater.from(context).inflate(
				R.layout.record_popupwindow_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		showImageView = (ImageView) view.findViewById(R.id.voiceimg);
	}

	/**
	 * 
	 * 
	 * @Title: showPopupWindow
	 * @Description: TODO
	 * @param @param view
	 * @return void
	 */
	public void showPopupWindow() {
		popupWindow.showAtLocation(
				LayoutInflater.from(context).inflate(R.layout.activity_main,
						null), Gravity.CENTER, 0, 0);
		showImageView.setBackgroundResource(voices[0]);
		count = 0;
		showVoicimg();
		startVoice();
	}

	/**
	 * 显示动态的声音
	 * 
	 * @Title: showVoicimg
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void showVoicimg() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTouch) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					handler.sendEmptyMessage(0);
				}
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			try {
				int ratio = mRecorder.getMaxAmplitude() / 600;
				int db = 0;// 分贝
				if (ratio > 1)
					db = (int) (20 * Math.log10(ratio));
				switch (db / 10) {
				case 0:
					showImageView.setBackgroundResource(voices[0]);
					break;
				case 1:
					showImageView.setBackgroundResource(voices[1]);
					break;
				case 2:
					showImageView.setBackgroundResource(voices[2]);
					break;
				case 3:
					showImageView.setBackgroundResource(voices[3]);
					break;
				case 4:
					showImageView.setBackgroundResource(voices[4]);
					break;
				case 5:
					showImageView.setBackgroundResource(voices[5]);
					break;
				default:
					showImageView.setBackgroundResource(voices[0]);
					break;
				}
			} catch (Exception e) {

			}
		};
	};

	public interface OnRecordListener {
		public void onRecord(String filepath, long time);
	}

	private void onRecord(String filepath, long time) {
		if (null != onRecordListener) {
			onRecordListener.onRecord(filepath, time);
		}
	}

	/** 开始录音 */
	private String startVoice() {
		path = "/gameproduct/xmppaudio/" + System.currentTimeMillis() + ".amr";
		// 设置录音保存路径
		try {
			startRecordTime = System.currentTimeMillis();
			String mFileName = Environment.getExternalStorageDirectory() + path;
			String state = android.os.Environment.getExternalStorageState();
			if (!state.equals(android.os.Environment.MEDIA_MOUNTED)) {
				return "";
			}
			File directory = new File(mFileName).getParentFile();
			if (!directory.exists() && !directory.mkdirs()) {
				return "";
			}
			mRecorder = new MediaRecorder();
			mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
			mRecorder.setOutputFile(mFileName);
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
			try {
				mRecorder.prepare();
			} catch (IOException e) {
			}
			mRecorder.start();
		} catch (Exception e) {
			System.out.println("===========mRecorder============"
					+ e.toString());
			return "";
		}
		return path;
	}

	/** 停止录音 */
	private long stopVoice(long time) {
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		return System.currentTimeMillis() - time;
	}

}
