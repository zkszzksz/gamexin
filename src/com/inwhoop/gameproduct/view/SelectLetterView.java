package com.inwhoop.gameproduct.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.FriendData;

import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DAIOR
 * @Title: SelectLetterView.java
 * @Package com.dy.daior
 * @Description: TODO
 * @date 2014-6-27 上午11:16:31
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class SelectLetterView extends View {

    private MypullListView puListView; // 下拉刷新的listview

    public List<FriendData> list; // 用户信息

    private Paint paint = new Paint();

    private int noPosition = 0;

    private int itemh = 1;

    private boolean flag = false;

    private TextView nowTextView = null;

    private String[] letter = {"#", "A", "B", "C", "D", "E", "F", "G", "H",
            "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z"};

    public SelectLetterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPuListView(MypullListView puListView) {
        this.puListView = puListView;
    }

    public void setList(List<FriendData> list) {
        this.list = list;
    }

    public void setNowTextView(TextView nowTextView) {
        this.nowTextView = nowTextView;
    }

    public void setNoStr(String noStr) {
        for (int i = 0; i < letter.length; i++) {
            if (noStr.equals(letter[i])) {
                noPosition = i;
                invalidate();
                break;
            }
        }
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        itemh = getMeasuredHeight() / letter.length;
        int width = getMeasuredWidth();
        for (int i = 0; i < letter.length; i++) {
            paint.setColor(getResources().getColor(R.color.game_all_count_color));
            paint.setTextSize(18);
            paint.setAntiAlias(true);
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setDither(true);
            if (i == noPosition) {
                paint.setColor(getResources().getColor(R.color.white));
                paint.setTextSize(18);
                Path path = new Path();
                RectF rect = new RectF(0, i * itemh, width, (i + 1) * itemh);
                path.addRoundRect(rect, 5, 5, Direction.CW);
                Paint paintPath = new Paint();
                paintPath.setStyle(Style.FILL_AND_STROKE);
                paintPath.setColor(getResources().getColor(
                        R.color.blue_33a6ff)); // 路径的画刷为红色
                canvas.drawPath(path, paintPath);
            }
            canvas.drawText(letter[i], width / 2,
                    ((i + 1) * itemh + i * itemh) / 2 + 10, paint);
        }
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float nowY = event.getY();
        noPosition = (int) (nowY / itemh);
        if (noPosition < 0) {
            noPosition = 0;
        } else if (noPosition >= letter.length) {
            noPosition = letter.length - 1;
        }
        if (null != nowTextView) {
            nowTextView.setVisibility(View.VISIBLE);
            nowTextView.setText(letter[noPosition]);
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                flag = true;
                break;

            case MotionEvent.ACTION_UP:
                handler.sendEmptyMessageDelayed(0, 500);
                break;

            default:
                break;
        }
        setListviewSelect(noPosition);
        invalidate();
        return true;
    }

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            flag = false;
            if (null != list && list.size() > 0) {
                nowTextView.setVisibility(View.GONE);
            }
        };
    };

    private void setListviewSelect(int position) {
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (letter[position].equals(list.get(i).letter)) {
                    puListView.setSelection(i);
                    break;
                }
            }
        }
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
