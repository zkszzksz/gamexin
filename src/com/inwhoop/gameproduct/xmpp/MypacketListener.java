package com.inwhoop.gameproduct.xmpp;

import com.inwhoop.gameproduct.utils.LogUtil;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Packet;

import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.utils.TimeRender;

import android.os.Bundle;
import android.os.Handler;

/**
 * @Project: StartActivity
 * @Title: MypacketListener.java
 * @Package com.wz.xmpp
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-1 下午4:20:08
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MypacketListener {

	private static MypacketListener listener = null;

	private Handler fHandler;

	private Handler gHandler;

	private PacketListener mPacketListener;

	public static MypacketListener getInstance() {
		if (null == listener) {
			listener = new MypacketListener();
		}
		return listener;
	}

	private MypacketListener() {
		registerMessageListener();
	}

	public void setfHandler(Handler fHandler) {
		this.fHandler = fHandler;
	}

	public void setgHandler(Handler gHandler) {
		this.gHandler = gHandler;
	}

	/************ start 新消息处理 ********************/
	private void registerMessageListener() {
		if (null != mPacketListener) {
			System.out.println("========messagegetBody===mPacketListener===");
			XmppConnectionUtil.getInstance().getConnection()
					.removePacketListener(mPacketListener);
		}
		PacketTypeFilter filter = new PacketTypeFilter(
				org.jivesoftware.smack.packet.Message.class);
		mPacketListener = new PacketListener() {
			public void processPacket(Packet packet) {
				try {
					System.out.println("========messagegetBody=xxxx====="
							+ ((org.jivesoftware.smack.packet.Message) packet)
									.getBody());
					System.out.println("========message=getFrom======"
							+ ((org.jivesoftware.smack.packet.Message) packet)
									.getFrom());
					if (packet instanceof org.jivesoftware.smack.packet.Message) {// 如果是消息类型
						org.jivesoftware.smack.packet.Message message = (org.jivesoftware.smack.packet.Message) packet;
						if (message.getFrom().contains(
								XmppConnectionUtil.getInstance()
										.getConnection().getUser())) {
						} else if (message
								.getFrom()
								.toString()
								.substring(message.getFrom().indexOf("@") + 1,
										message.getFrom().lastIndexOf("/"))
								.equals(XmppConnectionUtil.getInstance()
										.getConnection().getServiceName())
								&& null != fHandler) {
							System.out.println("========messagegetBody======"
									+ message.getBody());
							System.out.println("========message=getFrom======"
									+ message.getFrom());
							// message.getFrom().cantatins(获取列表上的用户，组，管理消息);
							// 获取用户、消息、时间、IN
							String[] args = new String[] { message.getFrom(),
									message.getBody(),
									TimeRender.getStandardDate(), "IN" };
							// 在handler里取出来显示消息
							android.os.Message msg = fHandler.obtainMessage();
							msg.what = 1;
							msg.obj = args;
							msg.sendToTarget();
						}
						if (!message
								.getFrom()
								.toString()
								.substring(message.getFrom().indexOf("@") + 1,
										message.getFrom().lastIndexOf("/"))
								.equals(XmppConnectionUtil.getInstance()
										.getConnection().getServiceName())
								&& null != gHandler) {
							System.out
									.println("=========getInstance==========");
							System.out
									.println("========message.getBody()======="
											+ message.getBody());
							String body = message.getBody();
							String from = message.getFrom();
//							int index = from.indexOf("/");
//							from = from.substring(index + 1, from.lastIndexOf("@"));
							android.os.Message msg = gHandler.obtainMessage();
							Bundle bundle = new Bundle();
							bundle.putString("body", body);
							bundle.putString("from", from);
							msg.setData(bundle);
							msg.what = MyApplication.MSG_BODY;
							gHandler.sendMessage(msg);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		try {
            XmppConnectionUtil.getInstance().getConnection()
                    .addPacketListener(mPacketListener, filter);// 这是最关健的了，少了这句，前面的都是白费功夫
        }catch (Exception e){
            LogUtil.e("exception,xmpp- -");
        }
	}

	public void loginout() {
		try {
			XmppConnectionUtil.getInstance().getConnection()
					.removePacketListener(mPacketListener);
			listener = null;
			this.fHandler = null;
			this.gHandler = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
