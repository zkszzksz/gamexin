package com.inwhoop.gameproduct.onekeyshare;

import android.content.Context;
import com.inwhoop.gameproduct.R;

/**
 * Created by LONG on 14-2-11.
 */
public class Share {

    public static void showShare(Context context, String content,String imagePath) {
        final OnekeyShare oks = new OnekeyShare();
        oks.setNotification(R.drawable.ic_launcher,
                context.getString(R.string.app_name));
        oks.setAddress("");
        oks.setTitle(context.getString(R.string.share));
        oks.setText(content);
        //oks.setImagePath(imagePath);
        //oks.setImageUrl(imagePath);
        //oks.setImagePath(imgpath);
        //oks.setUrl("http://wwww.jokeep.com");
        oks.setComment(context.getString(R.string.share));
        oks.setSite(context.getString(R.string.app_name));
        //oks.setSiteUrl("http://wwww.jokeep.com");
        // oks.setVenueName("Share SDK");
        // oks.setVenueDescription("This is a beautiful place!");
        // oks.setLatitude(23.056081f);
        // oks.setLongitude(113.385708f);
        oks.setSilent(false);
//        oks.setImagePath(MainActivity.TEST_IMAGE);
//        oks.setImageUrl("http://sharesdk.cn/media/sharesdk.jpg");
//        oks.setUrl("http://www.sharesdk.cn");
//        oks.setFilePath(MainActivity.TEST_IMAGE);
//        oks.setComment(menu.getContext().getString(R.string.share));
//        oks.setSite(menu.getContext().getString(R.string.app_name));
//        oks.setSiteUrl("http://sharesdk.cn");
//        oks.setVenueName("ShareSDK");
//        oks.setVenueDescription("This is a beautiful place!");
//        oks.setLatitude(23.056081f);
//        oks.setLongitude(113.385708f);
//        oks.setSilent(silent);
//        if (platform != null) {
//            oks.setPlatform(platform);
//        }

        // 去除注释，可令编辑页面显示为Dialog模式
//		oks.setDialogMode();

        // 去除注释，在自动授权时可以禁用SSO方式
//		oks.disableSSOWhenAuthorize();

        // 去除注释，则快捷分享的操作结果将通过OneKeyShareCallback回调
//		oks.setCallback(new OneKeyShareCallback());
        //oks.setShareContentCustomizeCallback(new ShareContentCustomizeDemo());

        // 去除注释，演示在九宫格设置自定义的图标
//		Bitmap logo = BitmapFactory.decodeResource(menu.getResources(), R.drawable.ic_launcher);
//		String label = menu.getResources().getString(R.string.app_name);
//		OnClickListener listener = new OnClickListener() {
//			public void onClick(View v) {
//				String text = "Customer Logo -- ShareSDK " + ShareSDK.getSDKVersionName();
//				Toast.makeText(menu.getContext(), text, Toast.LENGTH_SHORT).show();
//				oks.finish();
//			}
//		};
//		oks.setCustomerLogo(logo, label, listener);
        oks.show(context);
    }
}
