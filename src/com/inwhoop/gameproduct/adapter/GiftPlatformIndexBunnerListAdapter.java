package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import net.tsz.afinal.FinalBitmap;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/4/3.
 */
public class GiftPlatformIndexBunnerListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<Map<String, String>> list = null;
    private FinalBitmap fb;

    public GiftPlatformIndexBunnerListAdapter(Context context, List<Map<String, String>> list) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.gift_platform_index_bunner_list_item, null);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);

        RelativeLayout gameImg = (RelativeLayout) convertView.findViewById(R.id.gift_platform_index_bunner_list_item_rela);
        TextView gameName = (TextView) convertView.findViewById(R.id.gift_platform_index_bunner_list_item_text);
        gameName.setText(list.get(position).get("id"));
        if (position == MyApplication.position) {
            gameImg.setBackgroundColor(context.getResources().getColor(R.color.gray));
        }
        return convertView;
    }

}
