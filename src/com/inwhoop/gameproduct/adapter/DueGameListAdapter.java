package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.BaseActivity;
import com.inwhoop.gameproduct.entity.ActivtyCode;
import com.inwhoop.gameproduct.entity.DueGame;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO
 * @date 2014/5/8   19:27
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class DueGameListAdapter extends BaseAdapter{
    private Boolean isShowGift = true;//传参进来，是否显示礼包布局。目前用于游戏详情里游戏资讯和活动礼包

    private LayoutInflater inflater;
    private Context context;
    private List<DueGame> list = null;
    private Boolean isShowDue=false;


    public DueGameListAdapter(Context context, List<DueGame> list, Boolean isShowGift,Boolean isShowDue) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isShowGift = isShowGift;
        this.isShowDue=isShowDue;
    }

    public void add(List<DueGame> addlist){
        for (int i = 0; i < addlist.size(); i++) {
            list.add(addlist.get(i));
        }
    }

    public List<DueGame> getAll(){
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.gift_fragment_list_item, null);
            viewHolder = new ViewHolder();

            viewHolder.gameImg = (ImageView) convertView.findViewById(R.id.gift_fragment_list_item_img);
            viewHolder.gameName = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_title);
            viewHolder.gameAbstracts = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_abstract);
            viewHolder.gameNum = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_num);
            viewHolder.isShowGiftLayout = convertView.findViewById(R.id.show_gift_layout);
            viewHolder.isShowDue=(LinearLayout)convertView.findViewById(R.id.gift_fragment_list_item_due);
            // viewHolder.img=(TextView)convertView.findViewById(R.id.gameinfo_gift_list_item_img);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list) {
            Log.v("活动图片地址为", list.get(position).imgUrl);
            BitmapManager.INSTANCE.loadBitmap(list.get(position).imgUrl, viewHolder.gameImg,
                    R.drawable.default_local, true);
            viewHolder.gameName.setText(list.get(position).gameName);
            viewHolder.gameAbstracts.setText(list.get(position).schTxt);
        }

        if (!isShowGift) viewHolder.isShowGiftLayout.setVisibility(View.GONE);
        if (isShowDue==true){
            //viewHolder.img.setBackgroundResource(R.drawable.due_game_image);
            viewHolder.isShowDue.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    class ViewHolder {
        private ImageView gameImg;
        private TextView gameName;
        private TextView gameAbstracts;
        private TextView gameNum;
        public View isShowGiftLayout;
        public LinearLayout isShowDue;
        public TextView img;
    }

}
