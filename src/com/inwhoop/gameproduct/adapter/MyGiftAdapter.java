package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.DueGame;
import com.inwhoop.gameproduct.entity.MyGift;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/**
 * Created by Administrator on 2014/7/1.
 */
public class MyGiftAdapter extends BaseAdapter {
    private Boolean isShowGift = true;//传参进来，是否显示礼包布局。目前用于游戏详情里游戏资讯和活动礼包

    private LayoutInflater inflater;
    private Context context;
    private List<MyGift> list = null;


    public MyGiftAdapter(Context context, List<MyGift> list) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void add(List<MyGift> addlist){
        for (int i = 0; i < addlist.size(); i++) {
            list.add(addlist.get(i));
        }
    }

    public List<MyGift> getAll(){
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.my_gift_item, null);
            viewHolder = new ViewHolder();
            viewHolder.myGiftImg = (ImageView) convertView.findViewById(R.id.my_gift_item_img);
            viewHolder.myGiftName = (TextView) convertView.findViewById(R.id.my_gift_item_name);
            viewHolder.myGiftAbstracts = (TextView) convertView.findViewById(R.id.my_gift_item_sec);
            viewHolder.myGiftTime = (TextView) convertView.findViewById(R.id.my_gift_item_time);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list) {
            Log.v("活动图片地址为", list.get(position).imgUrl);
            BitmapManager.INSTANCE.loadBitmap(list.get(position).imgUrl, viewHolder.myGiftImg,
                    R.drawable.default_local, true);
            viewHolder.myGiftName.setText(list.get(position).giftBagName);
            viewHolder.myGiftAbstracts.setText(list.get(position).key);
            viewHolder.myGiftTime.setText(list.get(position).createTime);
            /*viewHolder.myGiftAbstracts.setText(list.get(position).schTxt);*/
        }

        return convertView;
    }

    class ViewHolder {
        public ImageView myGiftImg;
        public TextView myGiftName;
        public TextView myGiftAbstracts;
        public TextView myGiftTime;
    }

}
