package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/** 
 * 
 * @Project: DAIOR
 * @Title: MeberlistAdapter.java
 * @Package com.dy.daior
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-27 下午3:54:46
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MeberlistAdapter extends BaseAdapter {
	
	private List<FriendData> list;
	
	private LayoutInflater inflater;
	
	public MeberlistAdapter(Context context, List<FriendData> list){
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if(convertView == null){
			holder = new Holder();
			convertView = inflater.inflate(R.layout.item_layout, null);
			holder.letterLinear = (LinearLayout) convertView.findViewById(R.id.letter_linear);
			holder.letter = (TextView) convertView.findViewById(R.id.letter);
			holder.photo = (ImageView) convertView.findViewById(R.id.photos);
			holder.name = (TextView) convertView.findViewById(R.id.name);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}
		if(position>=1){
			if(list.get(position).letter.equals(list.get(position-1).letter)){
                holder.letterLinear.setVisibility(View.GONE);
			}else{
                holder.letterLinear.setVisibility(View.VISIBLE);
				holder.letter.setText(list.get(position).letter);
			}
		}else{
            holder.letterLinear.setVisibility(View.VISIBLE);
			holder.letter.setText(list.get(position).letter);
		}
        BitmapManager.INSTANCE.loadBitmap(list.get(position).userphoto,
                holder.photo, R.drawable.default_local, true);
		holder.name.setText(list.get(position).nickname);
		return convertView;
	}
	
	class Holder{
		TextView letter,name;
        public ImageView photo;
        public LinearLayout letterLinear;
    }

}
