package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.Actvitieslist;
import com.inwhoop.gameproduct.entity.Gameinformation;

import net.tsz.afinal.FinalBitmap;

import java.util.List;

/**
 * 
 * @Project: MainActivity
 * @Title: GameInformationListAdapter.java
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-28 下午9:15:51
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GameInformationListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<Gameinformation> list = null;
    private FinalBitmap fb;

    public GameInformationListAdapter(Context context, List<Gameinformation> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
    }
    
	
	public void add(List<Gameinformation> addlist){
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}
	
	public List<Gameinformation> getAll(){
		return list;
	}
    
    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.gameinformation_list_item, null);
            viewHolder = new ViewHolder();

            viewHolder.gameImg = (ImageView) convertView.findViewById(R.id.img);
            LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					MyApplication.widthPixels / 4,
					MyApplication.widthPixels / 5);
            viewHolder.gameImg.setLayoutParams(parm);
            viewHolder.gameName = (TextView) convertView.findViewById(R.id.title);
            viewHolder.gamemark = (TextView) convertView.findViewById(R.id.mark);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list) {
            fb.display(viewHolder.gameImg, list.get(position).newsimage);
        }
        viewHolder.gameName.setText(list.get(position).newsname);
        viewHolder.gamemark.setText(list.get(position).newsremark);
        return convertView;
    }

    class ViewHolder {
        private ImageView gameImg;
        private TextView gameName;
        private TextView gamemark;
    }

}
