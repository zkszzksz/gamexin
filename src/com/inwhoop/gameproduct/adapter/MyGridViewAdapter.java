package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/**
 * @Describe: TODO  搜索界面。你可能感兴趣的游戏。gridview的适配器
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/16 20:15
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MyGridViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<GameInfo> lists;
    private Context context;

    public MyGridViewAdapter(Context context, List<GameInfo> lists) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.lists = lists;
    }

    @Override
    public int getCount() {
        if (null != lists)
            return lists.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.view_item_gridview_search, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.textview);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageview);

            viewHolder.title.setTextColor(Color.BLACK);
            viewHolder.title.setTextSize(14);

            LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) viewHolder.image.getLayoutParams();
            linearParams.height = MyApplication.widthPixels/4-20;
            linearParams.width =linearParams.height ;
            viewHolder.image.setLayoutParams(linearParams);
            viewHolder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        GameInfo bean = lists.get(position);

        viewHolder.title.setText(bean.gamename);
        BitmapManager.INSTANCE.loadBitmap(bean.gamemainimage,viewHolder.image,R.drawable.loading,true);

        return convertView;
    }


    class ViewHolder {
        public TextView title;
        public ImageView image;
    }
}
