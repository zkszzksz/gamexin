package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.UserMessage;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/**
 * @Describe: TODO  消息 界面的Listview的适配器
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/03 16:02
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MessageListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private BitmapManager bitmapManager;
    private List<UserMessage> list;

    public MessageListAdapter(Context context, List<UserMessage> list) {
        this.inflater = LayoutInflater.from(context);
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_message_listview, null);
            holder.logo = (ImageView) convertView.findViewById(R.id.imageView_logo);
            holder.title = (TextView) convertView.findViewById(R.id.textView_title);
            holder.info = (TextView) convertView.findViewById(R.id.textView_info);
            holder.time = (TextView) convertView.findViewById(R.id.textView_time);
            holder.number = (TextView) convertView.findViewById(R.id.msg_number_TV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        UserMessage bean = list.get(position);
        BitmapManager.INSTANCE.loadBitmap(bean.logo_url, holder.logo,
                R.drawable.loading, true
//                , MyApplication.widthPixels/6, false
        );    //目测，所以设置了高度为屏幕宽/6
        holder.title.setText(bean.title);
        holder.info.setText(bean.info);
        holder.time.setText(bean.time);
        if (bean.number == 0) {
            holder.number.setVisibility(View.GONE);
        } else{
            holder.number.setVisibility(View.VISIBLE);
            holder.number.setText(""+bean.number);
        }

        int type = Integer.parseInt(bean.type);
        switch (type) {
            case UserMessage.GET_BAG:
                holder.title.setText(R.string.get_bag_ok_text);
                holder.time.setVisibility(View.GONE);
                break;
            case UserMessage.CHAT_ONE:
                holder.info.setVisibility(View.VISIBLE);
//                holder.number.setTextColor(R.drawable.??);//单聊，设置聊天消息的条数的深蓝背景，字为白色。若只要一条消息，只显示个黑色时间？
                break;
            case UserMessage.CHAT_GROUP:
//                holder.title.setText(R.string.checkout_msg);
                holder.info.setVisibility(View.VISIBLE);
//                holder.number.setTextColor(R.drawable.??);// 群聊，设置军团群聊消息的条数的背景，圈子和工会不同。字为白色。
                break;
            case UserMessage.CHAT_GROUP_SOC:
//              holder.title.setText(R.string.checkout_msg);
              holder.info.setVisibility(View.VISIBLE);
//              holder.number.setTextColor(R.drawable.??);// 群聊，设置军团群聊消息的条数的背景，圈子和工会不同。字为白色。
              break;
            case UserMessage.GIFT_BAG:
                holder.title.setVisibility(View.GONE);
                holder.info.setVisibility(View.VISIBLE);
                holder.time.setVisibility(View.GONE);
                break;
            case UserMessage.IS_AGREE:
                holder.title.setText(R.string.checkout_msg);

                break;
        }

        return convertView;
    }

    class ViewHolder {
        ImageView logo;
        TextView title;
        TextView info;
        TextView time;
        TextView number;
    }
}
