package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.ActivtyCode;
import com.inwhoop.gameproduct.utils.BitmapManager;
import net.tsz.afinal.FinalBitmap;

import java.util.List;
import java.util.Map;

/**
 * 礼包平台下的游戏信息列表适配器-dingwenlong
 * Created by Administrator on 2014/4/3.
 */
public class GiftPlatformGameInfoListAdapter extends BaseAdapter {

    private Boolean isShowGift = true;//传参进来，是否显示礼包布局。目前用于游戏详情里游戏资讯和活动礼包

    private LayoutInflater inflater;
    private Context context;
    private List<ActivtyCode> list = null;
    private Boolean isShowDue=false;

    public GiftPlatformGameInfoListAdapter(Context context, List<ActivtyCode> list) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public GiftPlatformGameInfoListAdapter(Context context, List<ActivtyCode> list, Boolean isShowGift) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isShowGift = isShowGift;
    }
    public GiftPlatformGameInfoListAdapter(Context context, List<ActivtyCode> list, Boolean isShowGift,Boolean isShowDue) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isShowGift = isShowGift;
        this.isShowDue=isShowDue;
    }

    public void add(List<ActivtyCode> addlist){
        for (int i = 0; i < addlist.size(); i++) {
            list.add(addlist.get(i));
        }
    }

    public List<ActivtyCode> getAll(){
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.gift_fragment_list_item, null);
            viewHolder = new ViewHolder();

            viewHolder.gameImg = (ImageView) convertView.findViewById(R.id.gift_fragment_list_item_img);
            viewHolder.gameName = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_title);
            viewHolder.gameAbstracts = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_abstract);
            viewHolder.gameNum = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_num);
            viewHolder.isShowGiftLayout = convertView.findViewById(R.id.show_gift_layout);
            viewHolder.isShowDue=(LinearLayout)convertView.findViewById(R.id.gift_fragment_list_item_due);
           // viewHolder.img=(TextView)convertView.findViewById(R.id.gameinfo_gift_list_item_img);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list) {
            ActivtyCode bean = list.get(position);
           BitmapManager.INSTANCE.loadBitmap(bean.imgUrl,viewHolder.gameImg,
                   R.drawable.default_local,true);
            viewHolder.gameName.setText(bean.giftBagName);
            viewHolder.gameAbstracts.setText(bean.giftBagDescription);
            viewHolder.gameNum.setText(bean.giftNum);
        }

        if (!isShowGift) viewHolder.isShowGiftLayout.setVisibility(View.GONE);
        if (isShowDue==true){
            //viewHolder.img.setBackgroundResource(R.drawable.due_game_image);
            viewHolder.isShowDue.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    class ViewHolder {
        private ImageView gameImg;
        private TextView gameName;
        private TextView gameAbstracts;
        private TextView gameNum;
        public View isShowGiftLayout;
        public LinearLayout isShowDue;
        public TextView img;
    }

}
