package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.utils.LogUtil;

import java.util.List;

/**
 * @Project: MainActivity
 * @Title: SubscribeLeftlistAdapter.java
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO 全部游戏的listview的适配器
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-4 下午3:28:11
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GamelistAdapter extends BaseAdapter {

	public List<GameInfo> list = null;

	public LayoutInflater inflater = null;

	public Context context;

	public GamelistAdapter(Context context, List<GameInfo> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	
	public void add(List<GameInfo> addlist){
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}
	
	public List<GameInfo> getAll(){
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == convertView) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.subscribe_leftlist_item,
					null);
			holder.img = (ImageView) convertView.findViewById(R.id.img);
			holder.giftimg = (ImageView) convertView.findViewById(R.id.imggift);
            holder.actimg = (ImageView) convertView.findViewById(R.id.imggact);
			holder.codeimg = (ImageView) convertView.findViewById(R.id.imgcode);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.type = (TextView) convertView.findViewById(R.id.type);
			holder.state = (TextView) convertView.findViewById(R.id.state);
			holder.count = (TextView) convertView.findViewById(R.id.count);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					MyApplication.widthPixels / 8*3,
					MyApplication.widthPixels / 32*9);
			holder.imgrela = (RelativeLayout) convertView
					.findViewById(R.id.imgrela);
			holder.imgrela.setLayoutParams(parm);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}
        GameInfo bean = list.get(position);

		holder.title.setText(""+bean.gamename);
		holder.type.setText(""+bean.typeid);
		holder.count.setText(bean.collectcount +" 收藏");
		if(bean.gamestate==0){
			holder.state.setTextColor(context.getResources().getColor(R.color.game_state_color_one));
		}else if(bean.gamestate==1){
			holder.state.setTextColor(context.getResources().getColor(R.color.game_state_color_two));
		}else{
			holder.state.setTextColor(context.getResources().getColor(R.color.game_state_color_three));
		}
		holder.state.setText(""+bean.gamestate);
		if(bean.isGift){
			holder.giftimg.setVisibility(View.VISIBLE);
		}else{
			holder.giftimg.setVisibility(View.GONE);
		}
		if(bean.isActive){
			holder.actimg.setVisibility(View.VISIBLE);
		}else{
			holder.actimg.setVisibility(View.GONE);
		}
		if(bean.isActcode){
			holder.codeimg.setVisibility(View.VISIBLE);
		}else{
			holder.codeimg.setVisibility(View.GONE);
		}

        BitmapManager.INSTANCE.loadBitmap(bean.gamemainimage,holder.img,R.drawable.loading,true);
         LogUtil.i("gamelist,adapter--im12gUrl:" + bean.gamemainimage);

        return convertView;
	}

	class Holder {
		ImageView img,giftimg,actimg,codeimg;
		TextView title, type,state, count;
		RelativeLayout imgrela;
	}

}
