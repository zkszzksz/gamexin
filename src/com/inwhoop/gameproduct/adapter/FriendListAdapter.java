package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/28 16:21
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class FriendListAdapter extends BaseAdapter {

    private ArrayList<FriendData> list;
    private Context context;

    public FriendListAdapter(Context mContext, ArrayList<FriendData> list) {
        this.context = mContext;
        this.list = list;
    }

    public void addList(List<FriendData> addList) {
        for (FriendData bean : addList)
            this.list.add(bean);
        notifyDataSetChanged();
    }

    //如果有自己，不显示
    public void addNoMeList(List<FriendData> addList, String userid) {
        for (FriendData bean : addList) {
            if (!bean.id.equals(userid))
                this.list.add(bean);
        }
        notifyDataSetChanged();
    }

    public ArrayList<FriendData> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.contact_item, null);
            viewHolder = new ViewHolder();
            viewHolder.tvCatalog = (TextView) convertView.findViewById(R.id.contactitem_catalog);
            viewHolder.ivAvatar = (ImageView) convertView.findViewById(R.id.contactitem_avatar_iv);
            viewHolder.tvNick = (TextView) convertView.findViewById(R.id.contactitem_nick);
            viewHolder.sexTV = (TextView) convertView.findViewById(R.id.sexTV);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvCatalog.setVisibility(View.GONE);
        viewHolder.sexTV.setVisibility(View.VISIBLE);

        FriendData bean = list.get(position);
        BitmapManager.INSTANCE.loadBitmap(bean.userphoto, viewHolder.ivAvatar,
                R.drawable.default_avatar, true);
        if (!bean.nickname.equals(""))
            viewHolder.tvNick.setText(bean.nickname);
        else
            viewHolder.tvNick.setText(bean.userid);

        viewHolder.sexTV.setText(bean.realSex());
        return convertView;
    }


    static class ViewHolder {
        TextView tvCatalog;//目录
        ImageView ivAvatar;//头像
        TextView tvNick;//昵称
        TextView sexTV;//性别
    }
}
