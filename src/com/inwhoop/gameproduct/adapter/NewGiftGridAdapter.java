package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import net.tsz.afinal.FinalBitmap;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/4/3.
 */
public class NewGiftGridAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private Context context;
        private List<Map<String,String>> list = null;
        private int srceenWidth;
        private FinalBitmap fb;
        private String id;

        public NewGiftGridAdapter(Context context, List<Map<String,String>> list, int srceenWidth) {
            this.context = context;
            this.list = list;
            this.srceenWidth = srceenWidth;
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return list.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.new_gift_gridview_item, null);
            fb = FinalBitmap.create(context);
            fb.configLoadingImage(R.drawable.default_local);
            fb.configLoadfailImage(R.drawable.default_local);
            TextView imageView = (TextView) convertView.findViewById(R.id.new_gift_gridview_item_img);
            ViewGroup.LayoutParams listParams = imageView.getLayoutParams();
            listParams.width = (srceenWidth / 2 - 23);
            listParams.height =(listParams.width *7/11);
            imageView.setLayoutParams(listParams);
            TextView Name = (TextView) convertView.findViewById(R.id.new_gift_gridview_item_name);
            Button exchangeBtn = (Button) convertView.findViewById(R.id.new_gift_gridview_item_btn);
            ViewGroup.LayoutParams listParam_1 = exchangeBtn.getLayoutParams();
            listParam_1.width = (srceenWidth / 2 - 23);
            exchangeBtn.setLayoutParams(listParam_1);
            if (null != list) {
               /* if (list.get(position).image.equals("")) {
                    Toast.makeText(context, "获取图片失败", Toast.LENGTH_SHORT).show();
                } else {
                    fb.display(imageView,list.get(position).image);
                }*/
                Name.setText(""+list.get(position).get("id"));
                exchangeBtn.setText("新手礼包");
            }
            //id = list.get(position).id.toString();
            exchangeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // id = list.get(position).id.toString();
                    //System.out.println("纪念币ID为11122222=="+id);
                    //dialog();
                }
            });
            return convertView;
        }

       /* protected void dialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("确认是否兑换！");
            builder.setTitle("提示");
            builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    dialog1.dismiss();
                    dialog = new DialogShowStyle(context, "积分兑换中,请稍候...");
                    dialog.dialogShow();
                    userIntegralExchange();
                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }

        public void userIntegralExchange() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Message msg = new Message();
                    try {
                        String str="Exchange/"+data.userInfo.id+"/"+id+"/"+HttpData.TOKEN;
                        System.out.println("json==积分上传的数据为:" + str);
                        String xmlData = SyncHttp.httpGet(HttpData.HOST, str);
                        System.out.println("json==积分获取到的服务器的数据为:" + xmlData);
                        String data= XmlUtil.getExchangeData(xmlData);
                        if (data.equals("true")) {
                            msg.what = Configs.SUCCESS;
                        } else {
                            msg.what = Configs.FAIL;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("数据异常" + e.toString());
                        msg.what = Configs.ERROR;
                    }
                    mHandler.sendMessage(msg);
                }
            }).start();
        }

        public Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                System.out.println("结果为+++==" + msg.what);
                dialog.dialogDismiss();
                switch (msg.what) {
                    case Configs.SUCCESS:
                        Toast.makeText(context, "恭喜您兑换成功,稍后工作人员将与您联系。", Toast.LENGTH_SHORT).show();
                        break;
                    case Configs.FAIL:
                        Toast.makeText(context, "兑换奖品失败,请查看积分是否足够兑换", Toast.LENGTH_SHORT).show();
                        break;
                    case Configs.ERROR:
                        Toast.makeText(context, "兑换奖品失败,请查看网络连接", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
    }*/

}
