package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.LoopInfoBean;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO
 * @date 2014/6/3   19:44
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class LoopListDataAdapter extends BaseAdapter {

    public List<LoopInfoBean> list = null;

    public LayoutInflater inflater = null;

    public Context context;

    public LoopListDataAdapter(Context context, List<LoopInfoBean> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        Holder holder = null;
        if (null == convertView) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.loop_fragment_list_item,
                    null);
            holder.headImg = (ImageView) convertView.findViewById(R.id.loop_fragment_list_item_head_img);
            holder.loopTitle = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_title);
            holder.loopDec = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_dec);
            holder.loopId = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_id_num);
            holder.personNum = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_people_num);
            convertView.setTag(holder);

            holder.loopId .setVisibility(View.GONE);  //圈子不显示id
            convertView.findViewById(R.id.loop_fragment_list_item_id).setVisibility(View.GONE);
        } else {
            holder = (Holder) convertView.getTag();
        }
        LoopInfoBean bean = list.get(position);
        holder.loopId.setText("" + bean.id);
        holder.loopTitle.setText(bean.circlename);
        holder.loopDec.setText(bean.circleremark);
        holder.personNum.setText("" + bean.personnum);
        BitmapManager.INSTANCE.loadBitmap(bean.circleheadphoto, holder.headImg, R.drawable.loading, true);
        return convertView;
    }

    class Holder {
        public ImageView headImg;
        public TextView loopTitle;
        public TextView loopDec;
        public TextView loopId;
        public TextView personNum;
    }
    }