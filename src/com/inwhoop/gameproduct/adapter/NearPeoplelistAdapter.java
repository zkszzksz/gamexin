package com.inwhoop.gameproduct.adapter;

import java.util.List;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.FriendData;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.view.RoundAngleImageView;

/**
 * @Project: MainActivity
 * @Title: SubscribeLeftlistAdapter.java
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO 全部游戏的listview的适配器
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-4 下午3:28:11
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class NearPeoplelistAdapter extends BaseAdapter {

	public List<FriendData> list = null;

	public LayoutInflater inflater = null;

	public Context context;

	public NearPeoplelistAdapter(Context context, List<FriendData> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	public void add(List<FriendData> addlist) {
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}

	public List<FriendData> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == convertView) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.near_people_list_item,
					null);
			holder.head = (RoundAngleImageView) convertView
					.findViewById(R.id.head_img);
			holder.name = (TextView) convertView.findViewById(R.id.username);
			holder.sex = (TextView) convertView.findViewById(R.id.sex);
			holder.address = (TextView) convertView.findViewById(R.id.address);
			holder.space = (TextView) convertView.findViewById(R.id.space);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		BitmapManager.INSTANCE.loadBitmap(list.get(position).userphoto,
				holder.head, R.drawable.loading, true);
		holder.name.setText(list.get(position).nickname);
		holder.sex.setText(list.get(position).realSex());
		holder.address.setText(list.get(position).useraddress);
		if(list.get(position).range<100){
			holder.space.setText("100m以内");
		}else{
			holder.space.setText(""+list.get(position).range+"m");
		}
		return convertView;
	}

	class Holder {
		RoundAngleImageView head;
		TextView name, sex, address, space;
	}

}
