package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.Gameinfogiftinfo;
import com.inwhoop.gameproduct.entity.Gameinformation;

import net.tsz.afinal.FinalBitmap;

import java.util.List;
import java.util.Map;

/**
 * 礼包平台下的游戏信息列表适配器-dingwenlong
 * Created by Administrator on 2014/4/3.
 */
public class GameInfoGiftPlatformAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<Gameinfogiftinfo> list = null;
    private FinalBitmap fb;

    public GameInfoGiftPlatformAdapter(Context context, List<Gameinfogiftinfo> list) {
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
    }
    

	public void add(List<Gameinfogiftinfo> addlist){
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}
	
	public List<Gameinfogiftinfo> getAll(){
		return list;
	}

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.gameinfo_gift_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.gameImg = (ImageView) convertView.findViewById(R.id.gift_fragment_list_item_img);
            viewHolder.gameName = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_title);
            viewHolder.gameAbstracts = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_abstract);
            viewHolder.gameNum = (TextView) convertView.findViewById(R.id.gift_fragment_list_item_num);
            viewHolder.isShowGiftLayout = convertView.findViewById(R.id.show_gift_layout);
            viewHolder.isShowDue=(LinearLayout)convertView.findViewById(R.id.gift_fragment_list_item_due);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list) {
            fb.display(viewHolder.gameImg, list.get(position).imgUrl);
            viewHolder.gameName.setText(list.get(position).giftBagName);
            viewHolder.gameAbstracts.setText(list.get(position).giftBagDescription);
            viewHolder.gameNum.setText(""+list.get(position).giftNum);
        }
        return convertView;
    }

    class ViewHolder {
        private ImageView gameImg;
        private TextView gameName;
        private TextView gameAbstracts;
        private TextView gameNum;
        public View isShowGiftLayout;
        public LinearLayout isShowDue;
    }

}
