package com.inwhoop.gameproduct.adapter;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendChatinfo;
import com.inwhoop.gameproduct.entity.GroupChatinfo;
import com.inwhoop.gameproduct.utils.FaceConversionUtil;
import com.inwhoop.gameproduct.utils.Utils;

import net.tsz.afinal.FinalBitmap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.LruCache;
import android.text.SpannableString;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @Project: WZSchool
 * @Title: ChatlistAdapter.java
 * @Package com.wz.adapter
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-1-17 下午1:27:08
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GroupChatlistAdapter extends BaseAdapter {

	private LayoutInflater inflater;

	private Context context;

	private List<GroupChatinfo> list;

	private LruCache<String, Bitmap> mMemoryCache;

	private ListView listView = null;

	private AlertDialog dialog = null;

	public FinalBitmap fb;

	private boolean isplay = false;

	private int count = 0;

	private int[] imgleft = { R.drawable.chatfrom_voice_playing_f1,
			R.drawable.chatfrom_voice_playing_f2,
			R.drawable.chatfrom_voice_playing_f3 };

	private int[] imgright = { R.drawable.chatto_voice_playing_f1,
			R.drawable.chatto_voice_playing_f2,
			R.drawable.chatto_voice_playing_f3 };

	public static interface IMsgViewType {
		int IMVT_COM_MSG = 0;
		int IMVT_TO_MSG = 1;
	}

	public GroupChatlistAdapter(Context context, List<GroupChatinfo> list,
			ListView listview) {
		this.context = context;
		this.list = list;
		this.listView = listview;
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
		inflater = LayoutInflater.from(context);
		// 获取应用程序最大可用内存
		int maxMemory = (int) Runtime.getRuntime().maxMemory();
		int cacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// return bitmap.getByteCount();//android api 12
				return bitmap.getRowBytes() * bitmap.getHeight();
			}
		};
	}

	public void addChatinfo(GroupChatinfo info) {
		list.add(info);
	}

	public void addChatinfo(List<GroupChatinfo> info) {
		List<GroupChatinfo> item = new ArrayList<GroupChatinfo>();
		for (int i = info.size() - 1; i >= 0; i--) {
			item.add(info.get(i));
		}
		for (int i = 0; i < list.size(); i++) {
			item.add(list.get(i));
		}
		list.clear();
		list = null;
		list = item;
	}

	public List<GroupChatinfo> getChatlist() {
		return list;
	}

	public int getChatinfolist() {
		return list.size() - 1;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public int getItemViewType(int position) {
		GroupChatinfo entity = list.get(position);
		if (entity.isMymsg) {
			return IMsgViewType.IMVT_COM_MSG;
		} else {
			return IMsgViewType.IMVT_TO_MSG;
		}

	}

	@Override
	public View getView(int position, View view, ViewGroup arg2) {
		Holder holder = null;
		final GroupChatinfo entity = list.get(position);
		boolean isComMsg = entity.isMymsg;
		// if (view == null) {
		holder = new Holder();
		if (isComMsg) {
			view = inflater.inflate(R.layout.chat_right_item, null);
		} else {
			view = inflater.inflate(R.layout.chat_left_item, null);
			holder.name = (TextView) view.findViewById(R.id.chatname);
			holder.name.setText(entity.usernick);
		}
		holder.head = (ImageView) view.findViewById(R.id.headimg);
		if ("".equals(entity.userheadpath)) {
			holder.head.setBackgroundResource(R.drawable.home_head);
		} else {
			fb.display(holder.head, entity.userheadpath);
		}
		holder.text = (TextView) view.findViewById(R.id.wordtv);
		holder.time = (TextView) view.findViewById(R.id.chattime);
		holder.img = (ImageView) view.findViewById(R.id.chatimg);
		holder.voiceImg = (ImageView) view.findViewById(R.id.playimg);
		holder.voicelength = (TextView) view.findViewById(R.id.voicelenth);
		holder.audiolayout = (RelativeLayout) view
				.findViewById(R.id.voicelayout);
		holder.isMy = isComMsg;
		view.setTag(holder);
		if (position == 0) {
			holder.time.setText(Utils.compareNowdate(entity.time));
		} else {
			if (Utils.compareTwoDate(list.get(position - 1).time, entity.time)) {
				holder.time.setText(Utils.compareNowdate(entity.time));
			} else {
				holder.time.setVisibility(View.GONE);
			}
		}
		if (entity.msgtype == 1) {
			SpannableString spannableString = FaceConversionUtil.getInstace()
					.getExpressionString(context, entity.content);
			holder.text.setText(spannableString);
			holder.text.setVisibility(View.VISIBLE);
			holder.img.setVisibility(View.GONE);
			holder.audiolayout.setVisibility(View.GONE);
		} else if (entity.msgtype == 2) {
			holder.text.setVisibility(View.GONE);
			holder.audiolayout.setVisibility(View.GONE);
			holder.img.setVisibility(View.VISIBLE);
			holder.img.setTag(entity.time);
			int w = (int) context.getResources().getDimension(
					R.dimen.chat_img_width);
			int itemw = (int) context.getResources().getDimension(
					R.dimen.chat_item_width);
			Bitmap bitmap = base642Bitmap(entity.content);
			float x = (float) bitmap.getWidth() / (float) bitmap.getHeight();
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(w,
					(int) (w / x));
			if (entity.isMymsg) {
				parm.bottomMargin = itemw;
				parm.leftMargin = itemw;
				parm.rightMargin = itemw * 5;
				parm.topMargin = itemw;
			} else {
				parm.bottomMargin = itemw;
				parm.leftMargin = itemw * 5;
				parm.rightMargin = itemw * 2;
				parm.topMargin = itemw;
			}
			holder.img.setLayoutParams(parm);
			holder.img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					View imgEntryView = inflater.inflate(
							R.layout.big_picture_layout, null); // 加载自定义的布局文件
					dialog = new AlertDialog.Builder(context).create();
					ImageView img = (ImageView) imgEntryView
							.findViewById(R.id.large_image);
					Bitmap bitmap = base642Bitmap(entity.content);
					float x = (float) bitmap.getWidth()
							/ (float) bitmap.getHeight();
					RelativeLayout.LayoutParams parm = null;
					if (x > 1) {
						parm = new RelativeLayout.LayoutParams(
								MyApplication.widthPixels / 5 * 3,
								(int) (MyApplication.widthPixels / 5 * 3 / x));
					} else {
						parm = new RelativeLayout.LayoutParams(
								(int) (MyApplication.heightPixels / 5 * 3 * x),
								MyApplication.heightPixels / 5 * 3);
					}
					img.setLayoutParams(parm);
					img.setImageBitmap(bitmap);
					dialog.setView(imgEntryView); // 自定义dialog
					dialog.show();
					// 点击布局文件（也可以理解为点击大图）后关闭dialog，这里的dialog不需要按钮
					imgEntryView.setOnClickListener(new OnClickListener() {
						public void onClick(View paramView) {
							dialog.cancel();
						}
					});
				}
			});
			setImgBitmap(holder.img, entity);
		} else {
			holder.text.setVisibility(View.GONE);
			holder.img.setVisibility(View.GONE);
			holder.audiolayout.setVisibility(View.VISIBLE);
			holder.audiolayout.setTag(holder.voiceImg);
			holder.voicelength.setText("" + entity.audiolength + "''");
			holder.audiolayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						if (!isplay) {
							isplay = true;
							count = 0;
							final ImageView voiceImageView = (ImageView) v
									.getTag();
							if (entity.isMymsg) {
								showVoiceImg(voiceImageView, 2);
							} else {
								showVoiceImg(voiceImageView, 1);
							}
							MediaPlayer mPlayer = new MediaPlayer();
							mPlayer.reset();
							mPlayer.setDataSource(Environment
									.getExternalStorageDirectory()
									+ entity.audiopath);
							mPlayer.setOnCompletionListener(new OnCompletionListener() {

								@Override
								public void onCompletion(MediaPlayer player) {
									isplay = false;
									if (entity.isMymsg) {
										voiceImageView
												.setBackgroundResource(R.drawable.chatto_voice_playing_f3);
									} else {
										voiceImageView
												.setBackgroundResource(R.drawable.chatfrom_voice_playing_f3);
									}
								}
							});
							mPlayer.prepare();
							mPlayer.start();
						} else {
							Toast.makeText(context, "正在进行播放~~", 0).show();
						}
					} catch (IOException e) {
					}
				}
			});
		}
		return view;
	}

	/**
	 * 
	 * @Title: showVoiceImg
	 * @Description: TODO
	 * @param @param img
	 * @return void
	 */
	private void showVoiceImg(final ImageView img, final int flag) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isplay) {
					Message msg = new Message();
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					msg.obj = img;
					msg.what = flag;
					vHandler.sendMessage(msg);
				}
			}
		}).start();
	}

	private Handler vHandler = new Handler() {
		public void handleMessage(Message msg) {
			ImageView img = (ImageView) msg.obj;
			if (isplay) {
				switch (msg.what) {
				case 1:
					img.setBackgroundResource(imgleft[count % 3]);
					break;
				case 2:
					img.setBackgroundResource(imgright[count % 3]);
					break;

				default:
					break;
				}
				count++;
			}
		};
	};

	public Bitmap base642Bitmap(String s) {
		byte[] bytes = Base64.decode(s, Base64.DEFAULT);
		return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
	}

	class Holder {
		ImageView head, img, voiceImg;
		TextView text, name, time, voicelength;
		RelativeLayout audiolayout;
		boolean isMy = true;
	}

	private void setImgBitmap(ImageView img, final GroupChatinfo entity) {
		Bitmap bitmap = getBitmapFromMemoryCache(entity.time);
		if (null != bitmap) {
			img.setImageBitmap(bitmap);
		} else {
			new Thread(new Runnable() {
				@Override
				public void run() {
					Message msg = new Message();
					try {
						Bitmap bit = base642Bitmap(entity.content);
						addBitmapToMemoryCache(entity.time, bit);
						Bundle bundle = new Bundle();
						bundle.putSerializable("entity", entity);
						msg.setData(bundle);
						msg.obj = bit;
					} catch (Exception e) {
						e.printStackTrace();
					}
					handler.sendMessage(msg);
				}
			}).start();
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			Bitmap bitmap = (Bitmap) msg.obj;
			GroupChatinfo info = (GroupChatinfo) msg.getData()
					.getSerializable("entity");
			ImageView img = (ImageView) listView.findViewWithTag(info.time);
			if (null != img) {
				img.setImageBitmap(bitmap);
			}

		};
	};

	/**
	 * 从LruCache中获取一张图片，如果不存在就返回null。
	 * 
	 * @param key
	 *            LruCache的键，这里传入图片的URL地址。
	 * @return 对应传入键的Bitmap对象，或者null。
	 */
	public Bitmap getBitmapFromMemoryCache(String key) {
		return mMemoryCache.get(key);
	}

	/**
	 * 将一张图片存储到LruCache中。
	 * 
	 * @param key
	 *            LruCache的键，这里传入图片的URL地址。
	 * @param bitmap
	 *            LruCache的键，这里传入从网络上下载的Bitmap对象。
	 */
	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemoryCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

}
