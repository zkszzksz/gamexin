package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.AgreeMessage;
import com.inwhoop.gameproduct.entity.ApplyInfo;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.List;

/**
 * @Describe: TODO  验证消息 Listview适配器
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/08 16:27
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class IsAgreeMsgAdapter extends BaseAdapter {
    private Handler handler;
    private Context mContext;
    private List<ApplyInfo> lists;
    private LayoutInflater inflater;

    // 点击索引：点击列表项；点击按钮；点击名字。

    protected final static int CLICK_INDEX_INFO = 10;

    protected final static int CLICK_INDEX_OK = 11;

    protected final static int CLICK_INDEX_NOT_OK = 12;

    public IsAgreeMsgAdapter(Context mContext, Handler handler, List<ApplyInfo> lists) {
        this.inflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
        this.handler=handler;
        this.lists = lists;
    }

    public List<ApplyInfo> getLists() {
        return lists;
    }

    public void addLists(List<ApplyInfo> list) {
        for (ApplyInfo bean : list)
            lists.add(bean);
        notifyDataSetChanged();
    }
    public void addBean(ApplyInfo bean){
        lists.add(bean);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null == convertView) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_is_agree_msg_listview, null);
            holder.logo = (ImageView) convertView.findViewById(R.id.logo);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.sex = (TextView) convertView.findViewById(R.id.sexTV);
            holder.address = (TextView) convertView.findViewById(R.id.address);
            holder.info = (TextView) convertView.findViewById(R.id.info);

            holder.isAgreeLayout = convertView.findViewById(R.id.is_agreeLayout);
            holder.isAgreeTV = (TextView) convertView.findViewById(R.id.is_agreeTV);
            convertView.setTag(holder);

            convertView.findViewById(R.id.more_info_layout)
                    .setOnClickListener(new OnItemChildClickListener(CLICK_INDEX_INFO,position));
            convertView.findViewById(R.id.agree_btn)
                    .setOnClickListener(new OnItemChildClickListener(CLICK_INDEX_OK, position));
            convertView.findViewById(R.id.not_agree_btn)
                    .setOnClickListener(new OnItemChildClickListener(CLICK_INDEX_NOT_OK,position)); } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ApplyInfo bean = lists.get(position);
        if(bean.isF == 1){
        	BitmapManager.INSTANCE.loadBitmap(bean.userPhoto, holder.logo, R.drawable.default_avatar, true);
        }else if(bean.isF == 3){
        	BitmapManager.INSTANCE.loadBitmap(bean.guildheadphoto, holder.logo, R.drawable.default_avatar, true);
        }else{
        	BitmapManager.INSTANCE.loadBitmap(bean.userPhoto, holder.logo, R.drawable.default_avatar, true);
        }
        holder.sex.setText(bean.getsex());
        holder.address.setText(""+bean.address);
//        holder.address.setText(bean.address);
        if(bean.isF == 1){
        	holder.name.setText(bean.userName+"    添加您为好友");
        }else if(bean.isF == 2){
        	holder.name.setText(bean.userName+"    申请加入公会【"+bean.guildName+ "】");
        }else{
        	holder.name.setText(bean.username+"    邀请加入公会【"+bean.guildname+ "】");
        }
        if("".equals(bean.askremark)){
        	 holder.info.setText("暂无");
        }else{
        	 holder.info.setText(bean.askremark);
        }
        if (!bean.isManage) {   //是否处理过消息
            holder.isAgreeLayout.setVisibility(View.VISIBLE);
            holder.isAgreeTV.setVisibility(View.GONE);
        } else {
            holder.isAgreeLayout.setVisibility(View.GONE);
            holder.isAgreeTV.setVisibility(View.VISIBLE);
            if (bean.isAgree)
                holder.isAgreeTV.setText(R.string.yitongyi);
            else
                holder.isAgreeTV.setText(R.string.yijujue);
        }

        return convertView;
    }

    private class OnItemChildClickListener implements View.OnClickListener {
                // 点击类型索引，对应前面的CLICK_INDEX_xxx
        private int clickIndex;
        private int position;

        public OnItemChildClickListener(int clickIndex, int position) {
            this.clickIndex = clickIndex;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // 创建Message并填充数据，通过mHandle联系Activity接收处理
            Message msg = new Message();
            msg.what = clickIndex;
            msg.arg1 = position;
            Bundle b = new Bundle();
            b.putSerializable("bean", lists.get(position));
            msg.setData(b);
            handler.sendMessage(msg);
        }
    }


    class ViewHolder {
        ImageView logo;
        TextView name;
        TextView sex;
        TextView address;
        TextView info;
        View isAgreeLayout;
        TextView isAgreeTV;
//        Button agreeBtn;
//        Button not_agreeBtn;
    }
}
