package com.inwhoop.gameproduct.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.inwhoop.gameproduct.entity.GroupChatinfo;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Project: WZSchool
 * @Title: HandleGroupmemberDB.java
 * @Package com.wz.db
 * @Description: TODO 操作好友聊天记录
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午3:00:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HandleGroupchatRecordDB {

	private DatabaseDBhelper dbhelper = null;

	private SQLiteDatabase readDB, writeDB;

	private String saveSql = "insert into readgroupchatrec(userid,touserid,usernick,groupid,groupname,userheadpath,chatcontent,"
			+ "stime,ismsg,msgtype,audiopath,audiolength,isloop,groupphoto) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	public HandleGroupchatRecordDB(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 
	 * @Title: saveGmember
	 * @Description: TODO
	 * @param @param chatinfo
	 * @return void
	 */
	public void saveGmember(GroupChatinfo chatinfo) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			if (chatinfo.isMymsg) {
				writeDB.execSQL(saveSql, new String[] { "" + chatinfo.userid,chatinfo.touserid,
						chatinfo.usernick, chatinfo.groupid,
						chatinfo.groupname, chatinfo.userheadpath,
						chatinfo.content, getTime(chatinfo.time), "" + 1,
						"" + chatinfo.msgtype, chatinfo.audiopath,
						"" + chatinfo.audiolength, "" + chatinfo.isloop,
						chatinfo.groupphoto });
			} else {
				writeDB.execSQL(saveSql, new String[] { "" + chatinfo.userid,chatinfo.touserid,
						chatinfo.usernick, chatinfo.groupid,
						chatinfo.groupname, chatinfo.userheadpath,
						chatinfo.content, getTime(chatinfo.time), "" + 2,
						"" + chatinfo.msgtype, chatinfo.audiopath,
						"" + chatinfo.audiolength, "" + chatinfo.isloop,
						chatinfo.groupphoto });
			}
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}
		}
	}

	/**
	 * 
	 * @Title: deleteGmember
	 * @Description: TODO
	 * @param @param groupid
	 * @return void
	 */
	public boolean deleteGmember(String userid,String groupid) {
		boolean isSucc = false;
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL("delete from readgroupchatrec where userid=? and groupid=?",new String[]{userid,groupid});
			writeDB.setTransactionSuccessful();
			isSucc = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}
		}
		return isSucc;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getTime(String user_time) {
		String re_time = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date d;
		try {
			d = sdf.parse(user_time);
			long l = d.getTime();
			String str = String.valueOf(l);
			re_time = str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re_time;
	}

	public static String getStrTime(String cc_time) {
		String re_StrTime = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long lcc_time = Long.valueOf(cc_time);
		re_StrTime = sdf.format(new Date(lcc_time));
		return re_StrTime;
	}

	/**
	 * 
	 * @Title: getUserBygroupid
	 * @Description: TODO
	 * @param @param number
	 * @param @param groupid
	 * @param @return
	 * @return List<Chatinfo>
	 */
	public List<GroupChatinfo> getUserBygroupid(String groupid, String userid,
			int page) {
		List<GroupChatinfo> list = new ArrayList<GroupChatinfo>();
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB
					.rawQuery(
							"select * from readgroupchatrec where userid=? and groupid=? order by id desc limit ?,?",
							new String[] { userid, groupid,
									"" + 10 * (page - 1), "" + 10 * page });
			if (cursor != null && cursor.getCount() > 0) {
				GroupChatinfo info = null;
				while (cursor.moveToNext()) {
					info = new GroupChatinfo();
					info.userid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.touserid = cursor.getString(cursor
							.getColumnIndex("touserid"));
					info.groupid = cursor.getString(cursor
							.getColumnIndex("groupid"));
					info.groupname = cursor.getString(cursor
							.getColumnIndex("groupname"));
					info.usernick = cursor.getString(cursor
							.getColumnIndex("usernick"));
					info.userheadpath = cursor.getString(cursor
							.getColumnIndex("userheadpath"));
					info.content = cursor.getString(cursor
							.getColumnIndex("chatcontent"));
					info.time = getStrTime(cursor.getString(cursor
							.getColumnIndex("stime")));
					if (cursor.getInt(cursor.getColumnIndex("ismsg")) == 1) {
						info.isMymsg = true;
					} else {
						info.isMymsg = false;
					}
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("msgtype"));
					info.audiopath = cursor.getString(cursor
							.getColumnIndex("audiopath"));
					info.groupphoto = cursor.getString(cursor
							.getColumnIndex("groupphoto"));
					info.audiolength = cursor.getInt(cursor
							.getColumnIndex("audiolength"));
					info.isloop = cursor
							.getInt(cursor.getColumnIndex("isloop"));
					list.add(info);
				}
				List<GroupChatinfo> nlist = new ArrayList<GroupChatinfo>();
				for (int i = list.size() - 1; i >= 0; i--) {
					nlist.add(list.get(i));
				}
				list.clear();
				list = nlist;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}

	/**
	 * 
	 * @Title: getfristBygroupid
	 * @Description: TODO
	 * @param @param username
	 * @param @param userid
	 * @param @return
	 * @return GroupChatinfo
	 */
	public GroupChatinfo getfristBygroupid(String groupid, String userid) {
		GroupChatinfo info = null;
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();

			cursor = readDB
					.rawQuery(
							"select * from readgroupchatrec where userid=? and groupid=? order by id desc limit 0,1",
							new String[] { userid, groupid });
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					info = new GroupChatinfo();
					info.userid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.touserid = cursor.getString(cursor
							.getColumnIndex("touserid"));
					info.groupid = cursor.getString(cursor
							.getColumnIndex("groupid"));
					info.groupname = cursor.getString(cursor
							.getColumnIndex("groupname"));
					info.usernick = cursor.getString(cursor
							.getColumnIndex("usernick"));
					info.userheadpath = cursor.getString(cursor
							.getColumnIndex("userheadpath"));
					info.content = cursor.getString(cursor
							.getColumnIndex("chatcontent"));
					info.time = getStrTime(cursor.getString(cursor
							.getColumnIndex("stime")));
					if (cursor.getInt(cursor.getColumnIndex("ismsg")) == 1) {
						info.isMymsg = true;
					} else {
						info.isMymsg = false;
					}
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("msgtype"));
					info.audiopath = cursor.getString(cursor
							.getColumnIndex("audiopath"));
					info.groupphoto = cursor.getString(cursor
							.getColumnIndex("groupphoto"));
					info.audiolength = cursor.getInt(cursor
							.getColumnIndex("audiolength"));
					info.isloop = cursor
							.getInt(cursor.getColumnIndex("isloop"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor)
				cursor.close();
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return info;
	}
	
	public List<GroupChatinfo> searchChatRec(String groupid, String search,
			Context context) {
		List<GroupChatinfo> list = new ArrayList<GroupChatinfo>();
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB
					.rawQuery(
							"select * from readgroupchatrec where chatcontent like ? and groupid=? and userid=? and msgtype=1",
							new String[] {"%"+search+"%",
									groupid,
									""+ UserInfoUtil.getUserInfo(context).username});
			if (cursor != null && cursor.getCount() > 0) {
				GroupChatinfo info = null;
				while (cursor.moveToNext()) {
					info = new GroupChatinfo();
					info.id = cursor.getInt(cursor
							.getColumnIndex("id"));
					info.userid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.touserid = cursor.getString(cursor
							.getColumnIndex("touserid"));
					info.groupid = cursor.getString(cursor
							.getColumnIndex("groupid"));
					info.groupname = cursor.getString(cursor
							.getColumnIndex("groupname"));
					info.usernick = cursor.getString(cursor
							.getColumnIndex("usernick"));
					info.userheadpath = cursor.getString(cursor
							.getColumnIndex("userheadpath"));
					info.content = cursor.getString(cursor
							.getColumnIndex("chatcontent"));
					info.time = getStrTime(cursor.getString(cursor
							.getColumnIndex("stime")));
					if (cursor.getInt(cursor.getColumnIndex("ismsg")) == 1) {
						info.isMymsg = true;
					} else {
						info.isMymsg = false;
					}
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("msgtype"));
					info.audiopath = cursor.getString(cursor
							.getColumnIndex("audiopath"));
					info.groupphoto = cursor.getString(cursor
							.getColumnIndex("groupphoto"));
					info.audiolength = cursor.getInt(cursor
							.getColumnIndex("audiolength"));
					info.isloop = cursor
							.getInt(cursor.getColumnIndex("isloop"));
					list.add(info);
				}
				List<GroupChatinfo> nlist = new ArrayList<GroupChatinfo>();
				for (int i = list.size() - 1; i >= 0; i--) {
					nlist.add(list.get(i));
				}
				list.clear();
				list = nlist;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}
	
	public List<GroupChatinfo> searchChatid(String groupid, String userid,String id,
			int page) {
		List<GroupChatinfo> list = new ArrayList<GroupChatinfo>();
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			if(page == 0){
				cursor = readDB
						.rawQuery(
								"select * from readgroupchatrec where userid=? and  groupid=? and id>=? order by id desc",
								new String[] { userid,groupid,id});
			}else{
				cursor = readDB
						.rawQuery(
								"select * from readgroupchatrec where userid=? and groupid=? and id<? order by id asc limit ?,? ",
								new String[] { userid, groupid,id,
										"" + 10 * (page - 1), "" + 10 * page });
			}
			if (cursor != null && cursor.getCount() > 0) {
				GroupChatinfo info = null;
				while (cursor.moveToNext()) {
					info = new GroupChatinfo();
					info.userid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.touserid = cursor.getString(cursor
							.getColumnIndex("touserid"));
					info.groupid = cursor.getString(cursor
							.getColumnIndex("groupid"));
					info.groupname = cursor.getString(cursor
							.getColumnIndex("groupname"));
					info.usernick = cursor.getString(cursor
							.getColumnIndex("usernick"));
					info.userheadpath = cursor.getString(cursor
							.getColumnIndex("userheadpath"));
					info.content = cursor.getString(cursor
							.getColumnIndex("chatcontent"));
					info.time = getStrTime(cursor.getString(cursor
							.getColumnIndex("stime")));
					if (cursor.getInt(cursor.getColumnIndex("ismsg")) == 1) {
						info.isMymsg = true;
					} else {
						info.isMymsg = false;
					}
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("msgtype"));
					info.audiopath = cursor.getString(cursor
							.getColumnIndex("audiopath"));
					info.groupphoto = cursor.getString(cursor
							.getColumnIndex("groupphoto"));
					info.audiolength = cursor.getInt(cursor
							.getColumnIndex("audiolength"));
					info.isloop = cursor
							.getInt(cursor.getColumnIndex("isloop"));
					list.add(info);
				}
				List<GroupChatinfo> nlist = new ArrayList<GroupChatinfo>();
				for (int i = list.size() - 1; i >= 0; i--) {
					nlist.add(list.get(i));
				}
				list.clear();
				list = nlist;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}

}
