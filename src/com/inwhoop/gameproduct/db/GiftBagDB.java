package com.inwhoop.gameproduct.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.inwhoop.gameproduct.entity.GiftBagInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.db
 * @Description: TODO
 * @date 2014/4/24   19:23
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GiftBagDB {
    private GameinfoDatabaseDBhelper dbhelper = null;
    private SQLiteDatabase readDB, writeDB;
    private Long updateTime=System.currentTimeMillis();
    /*
       保存活动礼包和用户礼包的数据
    */
    public void saveGiftBagInfo(GiftBagInfo giftBagInfo) {
        try {
            writeDB = dbhelper.getWritableDatabase();
            writeDB.beginTransaction();
            String sql = "insert into giftBagTable(giftBagId,giftBagName,giftBagDescription," +
                    "imgUrl,giftNum,giftBagType,updateTime) values(?,?,?,?,?,?,?)";
            writeDB.execSQL(sql, new String[]{""+giftBagInfo.giftBagId,
                    giftBagInfo.giftBagName, giftBagInfo.giftBagDescription,
                    giftBagInfo.imgUrl, ""+giftBagInfo.giftNum,""+giftBagInfo.giftBagType,""+updateTime});
            writeDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != writeDB) {
                writeDB.endTransaction();
                writeDB.close();
            }
        }
    }
     /*
     查询活动礼包和用户礼包的数据
      */

    public List<GiftBagInfo> getGiftBaginfo() {
        List<GiftBagInfo> list = new ArrayList<GiftBagInfo>();
        Cursor cursor = null;
        try {
            readDB = dbhelper.getReadableDatabase();
            readDB.beginTransaction();
            cursor = readDB.rawQuery("select * from giftBagTable", null);
            if (cursor != null && cursor.getCount() > 0) {
                GiftBagInfo giftBagInfo = null;
                while (cursor.moveToNext()) {
                    giftBagInfo = new GiftBagInfo();
                    giftBagInfo.giftBagId = cursor
                            .getInt(cursor.getColumnIndex("gift123BagId"));
                    giftBagInfo.giftBagName = cursor.getString(cursor
                            .getColumnIndex("gift123BagName"));
                    giftBagInfo.giftBagDescription = cursor.getString(cursor
                            .getColumnIndex("giftBagDesc123ription"));
                    giftBagInfo.imgUrl = cursor.getString(cursor
                            .getColumnIndex("im123gUrl"));
                    giftBagInfo.giftNum = cursor.getInt(cursor
                            .getColumnIndex("gift123Num"));
                    giftBagInfo.giftBagType = cursor.getInt(cursor
                            .getColumnIndex("gift123BagType"));
                    giftBagInfo.updateTime = cursor.getInt(cursor
                            .getColumnIndex("upda123teTime"));
                    list.add(giftBagInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            if (null != readDB) {
                readDB.endTransaction();
                readDB.close();
            }
        }
        return list;
    }
}
