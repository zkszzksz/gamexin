package com.inwhoop.gameproduct.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project: WZSchool
 * @Title: HandleGroupmemberDB.java
 * @Package com.wz.db
 * @Description: TODO 操作聊天记录列表
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午3:00:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HandleSetGrouptopDB {
	private DatabaseDBhelper dbhelper = null;

	private SQLiteDatabase readDB, writeDB;

	private String saveSql = "insert into setgrouptop(groupid,myid) values(?,?)";

	public HandleSetGrouptopDB(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 
	 * @Title: save 
	 * @Description: TODO
	 * @param @param groupid     
	 * @return void
	 */
	public void save(String groupid,String myid) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL(saveSql, new String[] { groupid,myid});
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}

		}

	}

	/**
	 * 
	 * @Title: delete 
	 * @Description: TODO
	 * @param @param groupid     
	 * @return void
	 */
	public void delete(String groupid,String myid) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL("delete from setgrouptop where groupid=" + groupid+" and myid="+myid);
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}
		}
	}

	/**
	 * 
	 * 按时间的倒序查询所有的聊天对象
	 * 
	 * @Title: getAlltagetlist
	 * @Description: TODO
	 * @param @param myid
	 * @param @return
	 * @return List<FriendChatinfo>
	 */
	public List<String> getAlltagetlist(String myid) {
		List<String> list = new ArrayList<String>();
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			Cursor cursor = readDB
					.rawQuery(
							"select * from setgrouptop where myid=? order by id asc",
							new String[] { myid });
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					list.add(cursor.getString(cursor
							.getColumnIndex("groupid")));
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}

	/**
	 * 
	 * @Title: isExist
	 * @Description: TODO 判断用户是否存在
	 * @param @param userid
	 * @param @return
	 * @return boolean
	 */
	public boolean isExist(String groupid,String myid) {
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB
					.rawQuery(
							"select * from setgrouptop where groupid=? and myid=?",
							new String[] { groupid, myid});
			String isuserid = "";
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					isuserid = cursor
							.getString(cursor.getColumnIndex("groupid"));
				}
			}
			if (!"".equals(isuserid)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return false;
	}

}
