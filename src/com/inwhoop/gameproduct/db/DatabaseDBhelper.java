package com.inwhoop.gameproduct.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 
 * @Project: DDCJ
 * @Title: DatabaseDBhelper.java
 * @Package com.inwhoop.ddcj.db
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-28 下午7:08:47
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class DatabaseDBhelper extends SQLiteOpenHelper {

	private static final String name = "ddcj"; // 数据库名称
	private static final int version = 1; // 数据库版本

	public DatabaseDBhelper(Context context) {
		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS readfriendchatrec(id integer "
				+ "primary key autoincrement,userid varchar(20),touserid varchar(20),username varchar(20),usernick varchar(20),"
				+ "userheadpath varchar(80),chatcontent varchar(200),stime integer,ismsg integer,"
				+ "msgtype integer,audiopath varchar(100),audiolength integer)");
		db.execSQL("CREATE TABLE IF NOT EXISTS chattagetlist (id integer primary"
				+ " key autoincrement,userid varchar(20),type"
				+ " integer,newtime varchar(20),myid integer,tagethead varchar(50),tagetnick varchar(20))");
		db.execSQL("CREATE TABLE IF NOT EXISTS readgroupchatrec(id integer "
				+ "primary key autoincrement,userid varchar(20),usernick varchar(20),touserid varchar(20),"
				+ "groupid varchar(20),groupname varchar(40),userheadpath varchar(80),"
				+ "chatcontent varchar(200),stime integer,ismsg integer,"
				+ "msgtype integer,audiopath varchar(100),audiolength integer,isloop integer,groupphoto varchar(40))");
		db.execSQL("CREATE TABLE IF NOT EXISTS noreadfriendchatrec(id integer "
				+ "primary key autoincrement,userid varchar(20),touserid varchar(20),username varchar(20),usernick varchar(20),"
				+ "userheadpath varchar(80),chatcontent varchar(200),stime integer,ismsg integer,"
				+ "msgtype integer,audiopath varchar(100),audiolength integer)");
		db.execSQL("CREATE TABLE IF NOT EXISTS noreadgroupchatrec(id integer "
				+ "primary key autoincrement,userid varchar(20),touserid varchar(20),usernick varchar(20),"
				+ "groupid varchar(20),groupname varchar(40),userheadpath varchar(80),"
				+ "chatcontent varchar(200),stime integer,ismsg integer,"
				+ "msgtype integer,audiopath varchar(100),audiolength integer,isloop integer,groupphoto varchar(40))");
		db.execSQL("CREATE TABLE IF NOT EXISTS setgrouptop (id integer primary key autoincrement,groupid varchar(20),myid varchar(20))");
		db.execSQL("CREATE TABLE IF NOT EXISTS stocklist (id integer primary key autoincrement,stockcode varchar(20),stockname varchar(20))");
		db.execSQL("CREATE TABLE IF NOT EXISTS pushdb (id integer primary key autoincrement,pushid varchar(10),content varchar(20)" +
				",sender varchar(10),time varchar(10),headImg varchar(10),type varchar(10),name varchar(10),userid varchar(10),isagree varchar(10),isread varchar(10))");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS stocklist");
		db.execSQL("DROP TABLE IF EXISTS readfriendchatrec");
		db.execSQL("DROP TABLE IF EXISTS chattagetlist");
		db.execSQL("DROP TABLE IF EXISTS readgroupchatrec");
		db.execSQL("DROP TABLE IF EXISTS noreadfriendchatrec");
		db.execSQL("DROP TABLE IF EXISTS noreadgroupchatrec");
		db.execSQL("DROP TABLE IF EXISTS pushdb");
		db.execSQL("DROP TABLE IF EXISTS setgrouptop");
		onCreate(db);
	}

}
