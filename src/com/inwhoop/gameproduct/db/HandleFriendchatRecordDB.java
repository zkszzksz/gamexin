package com.inwhoop.gameproduct.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.inwhoop.gameproduct.entity.FriendChatinfo;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Project: WZSchool
 * @Title: HandleGroupmemberDB.java
 * @Package com.wz.db
 * @Description: TODO 操作群聊天记录
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午3:00:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HandleFriendchatRecordDB {
	private DatabaseDBhelper dbhelper = null;

	private SQLiteDatabase readDB, writeDB;

	private String saveSql = "insert into readfriendchatrec(userid,touserid,username,usernick,userheadpath,chatcontent,"
			+ "stime,ismsg,msgtype,audiopath,audiolength) values(?,?,?,?,?,?,?,?,?,?,?)";

	public HandleFriendchatRecordDB(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 
	 * @Title: saveGmember
	 * @Description: TODO
	 * @param @param chatinfo
	 * @return void
	 */
	public void saveGmember(FriendChatinfo chatinfo) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			if (chatinfo.isMymsg) {
				writeDB.execSQL(saveSql, new String[] { "" + chatinfo.userid,chatinfo.touserid,
						chatinfo.username, chatinfo.usernick,
						chatinfo.userheadpath, chatinfo.content,
						getTime(chatinfo.time), "" + 1, "" + chatinfo.msgtype,
						chatinfo.audiopath, "" + chatinfo.audiolength });
			} else {
				writeDB.execSQL(saveSql, new String[] { "" + chatinfo.userid,chatinfo.touserid,
						chatinfo.username, chatinfo.usernick,
						chatinfo.userheadpath, chatinfo.content,
						getTime(chatinfo.time), "" + 2, "" + chatinfo.msgtype,
						chatinfo.audiopath, "" + chatinfo.audiolength });
			}
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}
		}
	}

	/**
	 * 
	 * @Title: deleteGmember
	 * @Description: TODO
	 * @param @param groupid
	 * @return void
	 */
	public void deleteGmember(int userid) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.execSQL("delete from noreadfriendchatrec where userid="
					+ userid);
		} catch (Exception e) {

		} finally {
			if (null != writeDB) {
				writeDB.close();
			}
		}
	}

	@SuppressLint("SimpleDateFormat")
	public static String getTime(String user_time) {
		String re_time = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date d;
		try {
			d = sdf.parse(user_time);
			long l = d.getTime();
			String str = String.valueOf(l);
			re_time = str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re_time;
	}

	public static String getStrTime(String cc_time) {
		String re_StrTime = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long lcc_time = Long.valueOf(cc_time);
		re_StrTime = sdf.format(new Date(lcc_time));
		return re_StrTime;
	}

	/**
	 * 
	 * @Title: getUserBygroupid
	 * @Description: TODO
	 * @param @param number
	 * @param @param groupid
	 * @param @return
	 * @return List<Chatinfo>
	 */
	public List<FriendChatinfo> getUserBygroupid(String username,
			String userid, int page) {
		List<FriendChatinfo> list = new ArrayList<FriendChatinfo>();
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB
					.rawQuery(
							"select * from readfriendchatrec where userid=? and username=? order by id desc limit ?,?",
							new String[] { userid, username,
									"" + 10 * (page - 1), "" + 10 * page });
			if (cursor != null && cursor.getCount() > 0) {
				FriendChatinfo info = null;
				while (cursor.moveToNext()) {
					info = new FriendChatinfo();
					info.userid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.touserid = cursor.getString(cursor
							.getColumnIndex("touserid"));
					info.username = cursor.getString(cursor
							.getColumnIndex("username"));
					info.usernick = cursor.getString(cursor
							.getColumnIndex("usernick"));
					info.userheadpath = cursor.getString(cursor
							.getColumnIndex("userheadpath"));
					info.content = cursor.getString(cursor
							.getColumnIndex("chatcontent"));
					info.time = getStrTime(cursor.getString(cursor
							.getColumnIndex("stime")));
					if (cursor.getInt(cursor.getColumnIndex("ismsg")) == 1) {
						info.isMymsg = true;
					} else {
						info.isMymsg = false;
					}
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("msgtype"));
					info.audiopath = cursor.getString(cursor
							.getColumnIndex("audiopath"));
					info.audiolength = cursor.getInt(cursor
							.getColumnIndex("audiolength"));
					list.add(info);
				}
				List<FriendChatinfo> nlist = new ArrayList<FriendChatinfo>();
				for (int i = list.size() - 1; i >= 0; i--) {
					nlist.add(list.get(i));
				}
				list.clear();
				list = nlist;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}

	/**
	 * 
	 * @Title: getfristBygroupid
	 * @Description: TODO
	 * @param @param username
	 * @param @param userid
	 * @param @return
	 * @return FriendChatinfo
	 */
	public FriendChatinfo getfristBygroupid(String username, String userid) {
		FriendChatinfo info = null;
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();

			cursor = readDB
					.rawQuery(
							"select * from readfriendchatrec where userid=? and username=? order by id desc limit 0,1",
							new String[] { userid, username });
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					info = new FriendChatinfo();
					info.userid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.touserid = cursor.getString(cursor
							.getColumnIndex("touserid"));
					info.username = cursor.getString(cursor
							.getColumnIndex("username"));
					info.usernick = cursor.getString(cursor
							.getColumnIndex("usernick"));
					info.userheadpath = cursor.getString(cursor
							.getColumnIndex("userheadpath"));
					info.content = cursor.getString(cursor
							.getColumnIndex("chatcontent"));
					info.time = getStrTime(cursor.getString(cursor
							.getColumnIndex("stime")));
					if (cursor.getInt(cursor.getColumnIndex("ismsg")) == 1) {
						info.isMymsg = true;
					} else {
						info.isMymsg = false;
					}
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("msgtype"));
					info.audiopath = cursor.getString(cursor
							.getColumnIndex("audiopath"));
					info.audiolength = cursor.getInt(cursor
							.getColumnIndex("audiolength"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor)
				cursor.close();
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return info;
	}

}
