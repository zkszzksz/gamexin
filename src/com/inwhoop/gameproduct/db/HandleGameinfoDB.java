package com.inwhoop.gameproduct.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.inwhoop.gameproduct.entity.Actvitieslist;
import com.inwhoop.gameproduct.entity.GameInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: HandleGameinfoDB.java
 * @Package com.inwhoop.gameproduct.db
 * @Description: TODO 操作游戏信息
 * @date 2014-4-16 下午3:10:45
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class HandleGameinfoDB {
    private GameinfoDatabaseDBhelper dbhelper = null;

    private SQLiteDatabase readDB, writeDB;
    private String saveSql = "insert into gameinfo(gameid,gamename,gamexplain,gamemainimage,gameimgage,gametype) values(?,?,?,?,?,?)";
    private Long updateTime=System.currentTimeMillis();
	public HandleGameinfoDB(Context context) {
		dbhelper =GameinfoDatabaseDBhelper.getInstance(context);
	}
    /**
     * 保存游戏信息
     *
     * @param @param gameInfo
     *
     * @return void
     *
     * @Title: saveGmember
     * @Description: TODO
     */
    public void saveGameinfo(GameInfo gameInfo,int type) {
        try {
            writeDB = dbhelper.getWritableDatabase();
            writeDB.beginTransaction();
            writeDB.execSQL(saveSql, new String[]{"" + gameInfo.gameid,
                    gameInfo.gamename, gameInfo.logotitle,
                    gameInfo.gamemainimage, gameInfo.gamemainimage,
                    "" + type});
            writeDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != writeDB) {
                writeDB.endTransaction();
                writeDB.close();
            }
        }
    }
    
    /**
     * 保存活动信息
     *
     * @param @param gameInfo
     *
     * @return void
     *
     * @Title: saveGmember
     * @Description: TODO
     */
    public void saveGameinfo(Actvitieslist info,int type) {
        try {
            writeDB = dbhelper.getWritableDatabase();
            writeDB.beginTransaction();
            writeDB.execSQL(saveSql, new String[]{"" + info.actId,
                    info.actName, "",
                    info.imgUrl, info.imgUrl,
                    "" + type});
            writeDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != writeDB) {
                writeDB.endTransaction();
                writeDB.close();
            }
        }
    }

    /**
     * 根据是否是热门、活动或者是banner的type进行删除
     *
     * @param @param gametype
     *
     * @return void
     *
     * @Title: deleteGmember
     * @Description: TODO
     */
    public void deleteGameinfoBytype(int gametype) {
        try {
            writeDB = dbhelper.getWritableDatabase();
            writeDB.beginTransaction();
            writeDB.execSQL("delete from gameinfo where gametype=" + gametype);
            writeDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != writeDB) {
                writeDB.endTransaction();
                writeDB.close();
            }
        }
    }

	/**
	 * 根据查询首页banner、热门的信息
	 * 
	 * @Title: getUserBygroupid
	 * @Description: TODO
	 * @param @param gametype
	 * @param @return
	 * @return List<GameInfo>
	 */
	public List<GameInfo> getAllGameinfo(int typeid) {
		List<GameInfo> list = new ArrayList<GameInfo>();
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB.rawQuery("select * from gameinfo where gametype=?", new String[]{""+typeid});
			if (cursor != null && cursor.getCount() > 0) {
				GameInfo info = null;
				while (cursor.moveToNext()) {
					info = new GameInfo();
					info.gameid = cursor
							.getInt(cursor.getColumnIndex("ga123meid"));
					info.gamename = cursor.getString(cursor
							.getColumnIndex("gam123ename"));
					info.logotitle = cursor.getString(cursor
							.getColumnIndex("gam123explain"));
					info.gamemainimage = cursor.getString(cursor
							.getColumnIndex("gam123eimgage"));
//					info.game123type = cursor.getInt(cursor
//							.getColumnIndex("ga123metype"));
					list.add(info);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}
	
	/**
	 * 查询活动信息
	 * @Title: getActvitieslist 
	 * @Description: TODO
	 * @param @param typeid
	 * @param @return     
	 * @return List<Actvitieslist>
	 */
	public List<Actvitieslist> getActvitieslist(int typeid) {
		List<Actvitieslist> list = new ArrayList<Actvitieslist>();
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB.rawQuery("select * from gameinfo where gametype=?", new String[]{""+typeid});
			if (cursor != null && cursor.getCount() > 0) {
				Actvitieslist info = null;
				while (cursor.moveToNext()) {
					info = new Actvitieslist();
					info.actId = cursor
							.getInt(cursor.getColumnIndex("gameid"));
					info.actName = cursor.getString(cursor
							.getColumnIndex("gamename"));
					info.imgUrl = cursor.getString(cursor
							.getColumnIndex("gameimgage"));
					list.add(info);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}
    /**
     * TODO    根据游戏类型查询游戏的信息。【1 xx】，【2 xx】，【3 xx】，【4感兴趣】
     *
     * @param gametype
     *
     * @return
     */
    public List<GameInfo> getInterestingGameinfo(int gametype) {
        List<GameInfo> list = new ArrayList<GameInfo>();
        Cursor cursor = null;
        try {
            readDB = dbhelper.getReadableDatabase();
            readDB.beginTransaction();
            cursor = readDB.rawQuery("select * from gameinfo where gametype=?", new String[]{gametype + ""});
            if (cursor != null && cursor.getCount() > 0) {
                GameInfo info = null;
                while (cursor.moveToNext()) {
                    info = new GameInfo();
                    info.gameid = cursor
                            .getInt(cursor.getColumnIndex("ga123meid"));
                    info.gamename = cursor.getString(cursor
                            .getColumnIndex("ga123mename"));
                    info.logotitle = cursor.getString(cursor
                            .getColumnIndex("game123xplain"));
                    info.gamemainimage = cursor.getString(cursor
                            .getColumnIndex("game123imgage"));
//                    info.gamet123ype = cursor.getInt(cursor
//                            .getColumnIndex("gam123etype"));
                    list.add(info);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
            if (null != readDB) {
                readDB.endTransaction();
                readDB.close();
            }
        }
        return list;
    }

}
