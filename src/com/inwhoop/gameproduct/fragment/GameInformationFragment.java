package com.inwhoop.gameproduct.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.GameNewsActivity;
import com.inwhoop.gameproduct.adapter.GameInformationListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.Gameinformation;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.view.XListView;

import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: TODO   游戏详情-游戏资讯 列表fragment
 * @date 2014/4/16 19:49
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GameInformationFragment extends Fragment implements
        XListView.IXListViewListener {

    private XListView giftList;

    private int gameid = 0;

    private Context context;

    private GameInformationListAdapter adapter;

    private List<Gameinformation> list = null;

    public GameInformationFragment() {
        super();
    }

    public GameInformationFragment(int gameid) {
        super();
        this.gameid = gameid;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gift_fragment_item, null);
        context = getActivity();
        ShareSDK.initSDK(getActivity());
        initView(view);
        return view;
    }

    private void initView(View view) {
        giftList = (XListView) view.findViewById(R.id.gift_fragment_item_list);
        giftList.setXListViewListener(this);
        giftList.setPullLoadEnable(true);
        read();
    }

    //TODO   获取游戏详情-游戏资讯
    private void read() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    list = JsonUtils.getGameinformationlist(gameid, Integer.MAX_VALUE);
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            giftList.stopRefresh();
            switch (msg.what) {
                case MyApplication.READ_FAIL:

                    break;

                case MyApplication.READ_SUCCESS:
                    if (list.size() == 0) {
                        MyToast.showShort(context, "暂无资讯信息");
                    }
                    if (list.size() < 10) {
                        giftList.setPullLoadEnable(false);
                    }
                    adapter = new GameInformationListAdapter(getActivity(), list);
                    giftList.setAdapter(adapter);
                    giftList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            Bundle bundle = new Bundle();
                            Log.v("资讯ID为", list.get(position - 1).newsid + "");
                            bundle.putString("newsid", list.get(position - 1).newsid + "");
                            bundle.putString("newsName",list.get(position-1).newsname+"");
                            Intent intent = new Intent(getActivity(), GameNewsActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                    break;

                default:
                    break;
            }

        }

        ;
    };

    @Override
    public void onRefresh() {
        read();
    }

    @Override
    public void onLoadMore() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    list = JsonUtils.getGameinformationlist(gameid, adapter.getAll().get(adapter.getAll().size() - 1).newsid);
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    LogUtil.e("Exception" + e.toString());
                    msg.what = MyApplication.READ_FAIL;
                }
                addhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler addhandler = new Handler() {
        public void handleMessage(Message msg) {
            giftList.stopLoadMore();
            switch (msg.what) {
                case MyApplication.READ_FAIL:

                    break;

                case MyApplication.READ_SUCCESS:
                    if (list.size() == 0) {
                        MyToast.showShort(context, "暂无资讯信息");
                        return;
                    }
                    if (list.size() < 10) {
                        giftList.setPullLoadEnable(false);
                    }
                    adapter.add(list);
                    adapter.notifyDataSetChanged();
                    break;

                default:
                    break;
            }

        }

        ;
    };

}
