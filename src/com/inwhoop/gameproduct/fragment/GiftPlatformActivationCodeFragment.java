package com.inwhoop.gameproduct.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GiftPlatformGameInfoListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/4/4.
 */
public class GiftPlatformActivationCodeFragment extends Fragment {

    private ListView activationCodeList;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.gift_platform_activation_code_fragment,null);
        context=getActivity();
        activationCodeList=(ListView)view.findViewById(R.id.gift_platform_activation_code_fragment_list);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        Map<String, String> map;
        for (int i = 0; i < 5; i++) {
            map = new HashMap<String, String>();
            map.put("id", "1");
            list.add(map);
        }
     /*   GiftPlatformGameInfoListAdapter adapter = new GiftPlatformGameInfoListAdapter(context, list);
        activationCodeList.setAdapter(adapter);*/
    }
}
