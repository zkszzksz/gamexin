package com.inwhoop.gameproduct.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.FriendChatActivity;
import com.inwhoop.gameproduct.adapter.MeberlistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.FileReadWriteUtil;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.SyncHttp;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.MypullListView;
import com.inwhoop.gameproduct.view.SelectLetterView;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: 个人好友界面-游戏圈
 * @date 2014/4/8   16:20
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class PersonFriendFragment extends BaseFragment implements MypullListView.OnRefreshListener {
    private UserInfo userInfo = null;
    private List<FriendData> friendListData = new ArrayList<FriendData>();
    private static String[] nicks = {};
    private String path = MyApplication.APP_PATH + "friend/";
    private TextView nowletter;
    private SelectLetterView letterView;
    private MypullListView listView;
    private List<FriendData> list = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        View view = inflater.inflate(R.layout.person_friend_fragment, null);
        initView(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView(View view) {
        userInfo = UserInfoUtil.getUserInfo(mContext);
        nowletter = (TextView) view.findViewById(R.id.person_friend_fragment_nowletter);
        letterView = (SelectLetterView) view.findViewById(R.id.person_friend_fragment_letter);
        listView = (MypullListView) view.findViewById(R.id.person_friend_fragment_listview);
        listView.setonRefreshListener(this);

        String jsonString = FileReadWriteUtil.readFileSdcardFile(path, "friendListInfo" + userInfo.id);
        getFriendListDatas(2, jsonString);
        getFriendListDatas(1, "");
    }

    private void read() {
//        showProgressDialog(getResources().getString(R.string.load_data));
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    list = getlist();
                    System.out.println("List数据的大小为==" + list.size());
                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    handler.sendEmptyMessage(0);
                }
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            dismissProgressDialog();
            listView.onRefreshComplete();
            letterView.setList(list);
            letterView.setPuListView(listView);
            letterView.setNowTextView(nowletter);
            final MeberlistAdapter adapter = new MeberlistAdapter(mContext, list);
            listView.setAdapter(adapter);
            if (null != list && list.size() > 0) {
                letterView.setNoStr(list.get(0).letter);
            }
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(mContext, FriendChatActivity.class);
                    intent.putExtra("username", list.get(position - 1).userid);
                    intent.putExtra("usernick", list.get(position - 1).nickname);
                    intent.putExtra("userphoto", list.get(position - 1).userphoto);
                    startActivity(intent);
                }
            });
        }

        ;
    };

    private List<FriendData> getlist() throws Exception {
        List<FriendData> list = new ArrayList<FriendData>();
        FriendData info = null;
        for (int i = 0; i < friendListData.size(); i++) {
            info = new FriendData();
            info.userphoto = friendListData.get(i).userphoto;
            info.nickname = friendListData.get(i).nickname;
            info.userid=friendListData.get(i).userid;
            list.add(info);
        }
        List<FriendData> otherlist = new ArrayList<FriendData>();
        List<FriendData> pinlist = new ArrayList<FriendData>();
        for (int i = 0; i < list.size(); i++) {
            if (isChinese(list.get(i).nickname.charAt(0))) {
                pinlist.add(list.get(i));
            } else {
                otherlist.add(list.get(i));
            }
        }
        pinlist = getSortListFrompin(pinlist);
        otherlist = getSortListFrome(otherlist);
        list.clear();
      /*  FriendData hui = new FriendData();
        hui.letter = "会长";
        hui.userPhoto=presidentList.get(0).userPhoto;
        hui.userName = presidentList.get(0).userName;
        list.add(hui);*/
        for (int i = 0; i < otherlist.size(); i++) {
            list.add(otherlist.get(i));
        }
        for (int i = 0; i < pinlist.size(); i++) {
            list.add(pinlist.get(i));
        }
        return list;
    }

    /**
     * 是否是汉字和字母
     *
     * @param @param  a
     * @param @return
     * @return boolean
     * @Title: isChinese
     * @Description: TODO
     */
    public boolean isChinese(char a) {
        int v = (int) a;
        return (v >= 19968 && v <= 171941) || (v >= 65 && v <= 90) || (v >= 97 && v <= 122);
    }

    @Override
    public void onRefresh() {
        showProgressDialog(getResources().getString(R.string.load_data));
        String jsonString = FileReadWriteUtil.readFileSdcardFile(path, "friendListInfo" + userInfo.id);
        getFriendListDatas(2, jsonString);
        getFriendListDatas(1, "");
    }

    /**
     * 对对非数字客户名进行排序
     *
     * @return
     * @throws BadHanyuPinyinOutputFormatCombination
     */
    private List<FriendData> getSortListFrompin(List<FriendData> list)
            throws BadHanyuPinyinOutputFormatCombination {
        Collections.sort(list, new Comparator<FriendData>() {
            @SuppressLint("DefaultLocale")
            public int compare(FriendData arg0, FriendData arg1) {
                try {
                    return getPinYin(arg0.nickname.toLowerCase()).compareTo(
                            getPinYin(arg1.nickname.toLowerCase()));
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = strTOup(getPinYin(list.get(i).nickname));
        }
        return list;
    }

    private String strTOup(String str) {
        return ("" + str.charAt(0)).toUpperCase();
    }

    /**
     * 对对非数字客户名进行排序
     *
     * @return
     */
    private List<FriendData> getSortListFrome(List<FriendData> list) {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                return arg0.nickname.compareTo(arg1.nickname);
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = "#";
        }
        return list;
    }

    /**
     * 将中文转换成拼音
     *
     * @param -汉字
     * @return
     */
    public static String getPinYin(String zhongwen)
            throws BadHanyuPinyinOutputFormatCombination {
        String zhongWenPinYin = "";
        char[] chars = zhongwen.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            String[] pinYin = PinyinHelper.toHanyuPinyinStringArray(chars[i],
                    getDefaultOutputFormat());
            // 当转换不是中文字符时,返回null
            if (pinYin != null) {
                zhongWenPinYin += pinYin[0];
            } else {
                zhongWenPinYin += chars[i];
            }
        }

        return zhongWenPinYin + "-" + zhongwen;
    }

    /**
     * 输出格式
     *
     * @return
     */
    public static HanyuPinyinOutputFormat getDefaultOutputFormat() {
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);// 小写
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);// 没有音调数字
        format.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);// u显示
        return format;
    }

    @Override
    public void scrollitem(int first) {
        if (null != letterView && null != list && !letterView.isFlag() && list.size() > 0) {
            letterView.setNoStr(list.get(first).letter);
        }
    }


    private void getFriendListDatas(final int tag, final String jsonString) {
//        showProgressDialog(getResources().getString(R.string.load_data));
//        showProgressDialog(getResources().getString(R.string.load_data));
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                Message msg = new Message();
                try {
                    String jsonData = "";
                    if (tag == 1) {
                        String str = MyApplication.GET_MY_FRIEND + "?userid=" + userInfo.id;
                        jsonData = SyncHttp.Get(MyApplication.HOST, str);
                        Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                        Log.v("json==获取到的服务器的数据为", jsonData);
                    } else if (tag == 2) {
                        jsonData = jsonString;
                    }
                    friendListData.clear();
                    friendListData = JsonUtils.getFriendListData(jsonData);
                    if (null != friendListData && friendListData.size() > 0) {
                        FileReadWriteUtil.write2SDFromInput2(path,
                                "friendListInfo" + userInfo.id, jsonData);
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
                Looper.loop();
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    List<String> list = new ArrayList<String>();
                    for (FriendData friendData : friendListData) {
                        list.add(friendData.userid);
                    }
                    read();
                    break;
                case MyApplication.FAIL:
                    dismissProgressDialog();
                    Toast.makeText(mContext,
                            "你还木有添加任何好友~", Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    dismissProgressDialog();
                    break;
            }
        }
    };

}
