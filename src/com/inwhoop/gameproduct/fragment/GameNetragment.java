package com.inwhoop.gameproduct.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GamelistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.view.MyListView;
import com.inwhoop.gameproduct.view.MyListView.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project: MainActivity
 * @Title: GameListFragment.java
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-15 下午5:44:51
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GameNetragment extends BaseFragment implements OnRefreshListener {

	private MyListView listView = null;

	private static int type; // 是否是热门游戏或者活动游戏

	private static int gametype; // 游戏所属类型

	private int page = 1; // 分页标签

	private List<GameInfo> list = null; // 游戏信息

	private GamelistAdapter adapter = null; // listview的adapter

	private View view = null;

	public static GameNetragment getInstance(int inputype, int inpugametype) {
		GameNetragment fragment = new GameNetragment();
		type = inputype;
		gametype = inpugametype;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		if (null == view) {
			view = inflater.inflate(R.layout.game_list_layout,container, false);
			listView = (MyListView) view.findViewById(R.id.gamelistview);
			listView.setRefreshListener(this);
			System.out.println("==========gametype============"+gametype);
			initData();
		}else{
			container.removeView(view);
		}
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	private void initData() {
		read();
	}

	/**
	 * 根据id读取游戏信息
	 * 
	 * @Title: read
	 * @Description: TODO
	 * @param @param id 类型
	 * @return void
	 */
	private void read() {
		showProgressDialog("正在努力加载...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = getSubcrilist(0);
					Thread.sleep(2000);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if (list.size() < 10) {
					listView.isRefreshable = false;
				} else {
					page++;
				}
				adapter = new GamelistAdapter(mContext, list);
				listView.setAdapter(adapter);
				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onRefresh() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = getSubcrilist(0);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				addhandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler addhandler = new Handler() {
		public void handleMessage(Message msg) {
			listView.onRefreshComplete();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if (list.size() < 10) {
					listView.isRefreshable = false;
				} else {
					page++;
				}
				adapter.add(list);
				adapter.notifyDataSetChanged();
				break;

			default:
				break;
			}

		};
	};

	/**
	 * 测试数据
	 * 
	 * @Title: getSubcrilist
	 * @Description: TODO
	 * @param @param id
	 * @param @return
	 * @return List<SubscibeInfo>
	 */
	private List<GameInfo> getSubcrilist(int type) {
		List<GameInfo> list = new ArrayList<GameInfo>();
		for (int i = 0; i < 10; i++) {
			GameInfo info = new GameInfo();
			info.gameid = i + 1;
			info.collectcount = 15782;
			info.gamename = "魔兽世界";
//			info.type123id = "角色扮演/武侠/3D";
//			info.ga123mestate = "正在运营";
			if (i % 2 == 0) {
				info.isActive = false;
			}
			if (i % 3 == 0) {
				info.isGift = false;
			}
			if (i % 4 == 0) {
				info.isActcode = false;
			}
			list.add(info);
		}
		return list;
	}

}
