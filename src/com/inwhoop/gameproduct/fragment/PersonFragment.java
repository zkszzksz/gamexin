package com.inwhoop.gameproduct.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.*;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.RoundAngleImageView;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;
import com.inwhoop.gameproduct.xmpp.MypacketListener;
import com.inwhoop.gameproduct.xmpp.XmppConnectionUtil;

/**
 * @Describe: TODO 个人中心/我的 2.上面背景图多高？本地图？ //目前取消跳一些界面 * * * ****** Created by ZK
 *            ********
 * @Date: 2014/04/18 12:13
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class PersonFragment extends BaseFragment implements
		View.OnClickListener {
	private String path = "http://h.hiphotos.baidu.com/image/w%3D2048/sign=e7e477224334970a4773172fa1f2d0c8/50da81cb39dbb6fd1d515a2b0b24ab18972b37b0.jpg";

	private View[] btns; // 将实现点击跳转的按钮

	private Class[] classes;// 点击后跳转的界面
	private UserInfo userInfo;
	private RoundAngleImageView headLogoImg;
	private TextView main_head_name_TV;

	@Override
	public void onResume() {
		super.onResume();
		if (!UserInfoUtil.getUserInfo(mContext).id.equals("")) {
			userInfo = UserInfoUtil.getUserInfo(mContext);
			setUserInfo();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_person, container, false);
		mContext = getActivity();
		userInfo = UserInfoUtil.getUserInfo(mContext);

		initData(view);

		setUserInfo();

		return view;
	}

	private void initData(View view) {
		init(view);
		setTitle(R.string.person_title);

		main_head_name_TV = (TextView) view
				.findViewById(R.id.main_head_name_TV);

		// 此图片后面要换掉
		ImageView bgIMG = (ImageView) view.findViewById(R.id.main_img_bg);
		bgIMG.setLayoutParams(new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				MyApplication.heightPixels / 4));
//		String bgPath = "http://img1.gamersky.com/image2013/06/20130610r_122/gamersky_037small_074_20136102324BA7.jpg";
//		BitmapManager.INSTANCE.loadBitmap(bgPath, bgIMG, R.drawable.loading,
//				true);

		btns = new View[] { (RadioButton) view.findViewById(R.id.first_btn), // 0
																				// 我的礼包
				(RadioButton) view.findViewById(R.id.second_btn), // 1 我的收藏
				(RadioButton) view.findViewById(R.id.eveyday_recommend), // 2
																			// 每日推荐
				(RadioButton) view.findViewById(R.id.elite_recommend), // 3 精品推荐
				(RadioButton) view.findViewById(R.id.get_msg_setting), // 4 推送设置
				(RadioButton) view.findViewById(R.id.safe_and_personal), // 5
																			// 安全与隐私
				(RadioButton) view.findViewById(R.id.about), // 6 关于
				(RadioButton) view.findViewById(R.id.third_btn), // 7 我的预定
				(ImageView) view.findViewById(R.id.main_head_logo_circleIV),// 8头像跳转个人信息
				(RadioButton) view.findViewById(R.id.get_out_login) }; // 9 退出登录
		for (int i = 0; i < btns.length; i++) {
			btns[i].setOnClickListener(this);
		}
		classes = new Class[] { MyGiftActivity.class, // 0 我的礼包
				GameCollectListActivity.class, // 1 我的收藏
                GameRecommendActivity.class, // 2 每日推荐
                GameRecommendActivity.class, // 3 精品推荐
				SettingGetMsgActivity.class, // 4 推送设置
				SafePersonalActivity.class, // 5 安全与隐私
				AboutActivity.class, // 6 关于
				AboutActivity.class, // 7 我的预定
				PersonInfoActivity.class, // 8个人信息
				LoadActivity.class }; // 9退出登录

	}

	// TODO 设置用户信息
	private void setUserInfo() {
		path = userInfo.userphoto; // 此行后面要打开
		BitmapManager.INSTANCE.loadBitmap(path, (ImageView) btns[8],
				R.drawable.default_avatar, true);
		main_head_name_TV.setText(userInfo.username);
		if (userInfo.username.equals(""))
			main_head_name_TV.setText(userInfo.userid);
		if (userInfo.id.equals(""))
			main_head_name_TV.setText(R.string.not_logining);
	}

	@Override
	public void onClick(View v) {
		Bundle b = new Bundle();
		for (int i = 0; i < btns.length; i++) {
			int a = btns[i].getId();
			if (v.getId() == a) {
				switch (i) {
				case 9:
					loginout();
				case 0:
				case 1:
				case 5:
				case 6:
				case 8:
					Act.toAct(mContext, classes[i]);
                    break;
				case 2:
					b.putInt("type", 0);
					Act.toAct(mContext, classes[i],b);
	                 break;
				case 3:
					b.putInt("type", 1);
                    Act.toAct(mContext, classes[i],b);
                    break;
				case 4:
				case 7:
					MyToast.showShort(mContext, "暂未开放");
					break;
				}
			}
		}
	}
	
	public void loginout(){
		try {
			ClientGroupServer.client = null;
			ClienConServer.client = null;
			if (null != ((MainActivity) getActivity()).messageFragment
					&& null != ((MainActivity) getActivity()).messageFragment.mRecever) {
				getActivity()
						.unregisterReceiver(
								((MainActivity) getActivity()).messageFragment.mRecever);
			}
			MypacketListener.getInstance().loginout();
			XmppConnectionUtil.getInstance().closeConnection();
		} catch (Exception e) {
		}
		UserInfoUtil.clear(mContext);
		getActivity().finish();
	}
	
}
