package com.inwhoop.gameproduct.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.SociatySureActivity;
import com.inwhoop.gameproduct.adapter.SociatyListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;

import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: TODO   游戏圈子中的公会页面
 * @date 2014-06-20 11:45:25。ZK
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class TradeFragment extends BaseFragment implements IXListViewListener{
    private UserInfo userInfo = null;
    private SociatyListAdapter adapter;
    private boolean isRefresh = false;//下拉没数据才提示

//    private boolean isShot = false;
    
    private  XListView listView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trade_fragment, null);
        mContext = getActivity();
        userInfo = UserInfoUtil.getUserInfo(getActivity());
        initView(view);

//        showProgressDialog(getResources().getString(R.string.load_data));
//        getLoopListData("-1", Integer.parseInt(userInfo.id));

        return view;
    }

    private void initView(View view) {
        listView = (XListView) view.findViewById(R.id.trade_fragment_list);
        listView.setXListViewListener(this);
		listView.setPullLoadEnable(false);

        adapter = new SociatyListAdapter(mContext);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0)return;
                Bundle b = new Bundle();
                b.putSerializable("bean", adapter.getList().get(position-1));
                Act.toAct(mContext, SociatySureActivity.class, b);
//                isShot = true;
            }
        });
    }
    
    @Override
    public void onResume() {
    	super.onResume();
//    	if(isShot){
        userInfo = UserInfoUtil.getUserInfo(getActivity());
        String uId = userInfo.id;
        if (!"".equals(uId) ) {
    		getLoopListData("-1", Integer.parseInt(userInfo.id));
        }
//    		isShot = false;
//    	}
    }

    public void getLoopListData(final String minId, final int userid) {
        adapter.getList().clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.searchMyGuild(minId, userid);
                    msg.what = MyApplication.SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            listView.stopRefresh();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    List<SociatyBean> sociatyBeanList = (List<SociatyBean>) msg.obj;
                    if (null != sociatyBeanList && sociatyBeanList.size() > 0){
                        adapter.add(sociatyBeanList);
                    }else{
                        if (isRefresh) {
                            Toast.makeText(mContext, "你木有加入任何公会哦~", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(mContext, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

	@Override
	public void onRefresh() {
        isRefresh = true;
        getLoopListData("-1", Integer.parseInt(userInfo.id));
	}

	@Override
	public void onLoadMore() {
		listView.stopLoadMore();
	}
}
