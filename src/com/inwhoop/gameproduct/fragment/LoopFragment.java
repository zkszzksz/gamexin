package com.inwhoop.gameproduct.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.LoopGroupChatActivity;
import com.inwhoop.gameproduct.adapter.LoopListDataAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.LoopInfoBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: TODO 游戏圈中圈子页面
 * @date 2014/5/21   14:27
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class LoopFragment extends BaseFragment implements IXListViewListener {
    private XListView listView;
    private UserInfo userInfo = null;
    private List<LoopInfoBean> loopListData = new ArrayList<LoopInfoBean>();
    private LoopListDataAdapter adapter;
    private boolean isRefresh = false;//下拉没数据才提示
//    private boolean isShot = false;

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.i("LoopFragment,onResume");
        userInfo = UserInfoUtil.getUserInfo(getActivity());
        String uId = userInfo.id;
        if (!"".equals(uId) ) {
            //传-2的话虽然是没有数据返回,但是不必执行查询
            getLoopListData(userInfo.id);
//            isShot = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loop_fragment, null);
        mContext=getActivity();

        initView(view);

//        showProgressDialog(getResources().getString(R.string.load_data));
//        getLoopListData(UserInfoUtil.getUserInfo(mContext).id);

        return view;
    }

    private void initView(View view) {
        userInfo = UserInfoUtil.getUserInfo(getActivity());
        listView = (XListView) view.findViewById(R.id.loop_fragment_list);
        listView.setXListViewListener(this);
        listView.setPullRefreshEnable(true);
        listView.setPullLoadEnable(false);

        adapter = new LoopListDataAdapter(getActivity(), loopListData);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                Bundle b = new Bundle();
                b.putSerializable("bean", loopListData.get(position - 1));
                b.putString("groupid", ""+loopListData.get(position - 1).id);
                b.putString("groupname", loopListData.get(position - 1).circlename);
                b.putString("groupphoto", loopListData.get(position - 1).circleheadphoto);
                Act.toAct(getActivity(), LoopGroupChatActivity.class, b);
//                isShot = true;
                //startActivity(new Intent(getActivity(), InviteMembersActivity.class));
            }
        });
    }

    public void getLoopListData(final String uId) {
        loopListData.clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    List<LoopInfoBean> listData = JsonUtils.geLoopListData(uId);
                    if (null != listData && listData.size() > 0) {
                        msg.obj = listData;
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            listView.stopRefresh();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    List<LoopInfoBean> listData = (List<LoopInfoBean>) msg.obj;
                    for (LoopInfoBean bean : listData) {
                        loopListData.add(bean);
//                        ClientGroupServer.getInstance(mContext).noExistToJoin(""+bean.id, bean.circlename);
                    }
                    adapter.notifyDataSetChanged();
                    break;
                case MyApplication.FAIL:  //因为onResume方法会被各种反复执行，所以这里不弹提示。最好是界面显示提示
                    if (isRefresh) {
                        Toast.makeText(getActivity(), "你木有加入任何圈子！", Toast.LENGTH_SHORT).show();
                        isRefresh = false;
                    }
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(getActivity(), "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onRefresh() {
        isRefresh = true;
        if (!"".equals(userInfo.id)) {
            //传-2的话虽然是没有数据返回,但是不必执行查询
            getLoopListData(userInfo.id);
        }
    }

    @Override
    public void onLoadMore() {
        listView.stopLoadMore();
    }
}
