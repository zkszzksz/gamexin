package com.inwhoop.gameproduct.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.GameAllListActivity;
import com.inwhoop.gameproduct.acitivity.GameInfosActivity;
import com.inwhoop.gameproduct.acitivity.SearchGameActivity;
import com.inwhoop.gameproduct.adapter.HomepageViewpagerAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.db.HandleGameinfoDB;
import com.inwhoop.gameproduct.entity.Actvitieslist;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.Network;
import com.inwhoop.gameproduct.view.MyGridView;
import net.tsz.afinal.FinalBitmap;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 首页界面
 * 
 * @Project: GameProduct
 * @Title: HomeFragment.java
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-2 下午2:34:17
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HomeFragment extends BaseFragment implements OnClickListener {

	private LayoutInflater inflater = null;

	private ViewPager bannerMyViewpager = null; // 首页banner

	private LinearLayout pointLayout = null; // 点点

	private LinearLayout contentLayout = null; // 加载热点、活动等的layout

	private List<GameInfo> bannergamelist = null; // 存储banner信息的list

	private List<GameInfo> wyList = new ArrayList<GameInfo>(); // 网游

	private List<GameInfo> syList = new ArrayList<GameInfo>(); // 手游

//	private List<GameInfo> yyList = new ArrayList<GameInfo>(); // 页游
//
//	private List<GameInfo> djList = new ArrayList<GameInfo>(); // 单机

	private List<GameInfo> netwyList = new ArrayList<GameInfo>(); // 网游

	private List<GameInfo> netsyList = new ArrayList<GameInfo>(); // 手游

//	private List<GameInfo> netyyList = new ArrayList<GameInfo>(); // 页游
//
//	private List<GameInfo> netdjList = new ArrayList<GameInfo>(); // 单机

	private List<Actvitieslist> actList = null; // 活动

	private List<GameInfo> newsList = null; // 已订阅游戏新闻

	private List<View> bannerViews = new ArrayList<View>(); // banner的views

	private String[] itemtitles = null;

	private List<View> pointList = new ArrayList<View>();

	private RelativeLayout bannerLayout = null;

	private TextView bannertitleTextView = null; // banner的标题

	private ScheduledExecutorService scheduledExecutorService;

	private int currentItem = 0; // 当前的索引号

	private FinalBitmap fb = null;

	private HandleGameinfoDB db = null;
	
	private int[] icons = {R.drawable.icon_wangyou,R.drawable.icon_shouyou};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater = inflater;
		db = new HandleGameinfoDB(getActivity());
		mContext = getActivity();
		itemtitles = getResources().getStringArray(R.array.home_item);
		View view = inflater.inflate(R.layout.homepage_layout, null);
		bannerMyViewpager = (ViewPager) view
				.findViewById(R.id.banner_viewpager);
		contentLayout = (LinearLayout) view.findViewById(R.id.contentlayout);
		pointLayout = (LinearLayout) view.findViewById(R.id.point_lin);
		bannerLayout = (RelativeLayout) view.findViewById(R.id.bannerlayout);
		bannerLayout.setVisibility(View.GONE);
		bannertitleTextView = (TextView) view.findViewById(R.id.v_title);
		fb = FinalBitmap.create(getActivity());
		fb.configLoadingImage(R.drawable.home_hot_bg1);
		fb.configLoadfailImage(R.drawable.home_hot_bg1);
		super.init(view);
		setTitle(R.string.app_name);
		setRightSecondBt(R.drawable.search);
		getRightSecondBt().setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initData();
	}

	private void initData() {
		readDB();
	}

	private void readDB() {
		bannergamelist = db.getAllGameinfo(1);
		wyList = db.getAllGameinfo(2);
		syList = db.getAllGameinfo(3);
//		yyList = db.getAllGameinfo(4);
//		djList = db.getAllGameinfo(5);
		// if (bannergamelist.size() == 0) {
		// showProgressDialog("正在读取信息...");
		// readBannerinfo();
		// readInfo();
		// } else {
		setViewPager();
		setItemlayout(0, wyList);
		setItemlayout(1, syList);
//		setItemlayout(2, yyList);
//		setItemlayout(3, djList);
		readBannerinfo();
		readInfo(1);
		// }
	}

	/**
	 * 读取banner信息
	 * 
	 * @Title: readBannerinfo
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void readBannerinfo() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					bannergamelist = JsonUtils.getHomeBannerList();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				bannerHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler bannerHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if (bannergamelist.size() > 0) {
					db.deleteGameinfoBytype(1);
					for (int i = 0; i < bannergamelist.size(); i++) {
						db.saveGameinfo(bannergamelist.get(i), 1);
					}
				}
				setViewPager();
				break;

			default:
				break;
			}

		};
	};

	/**
	 * 设置banner
	 * 
	 * @Title: setViewPager
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void setViewPager() {
		if (null != pointList) {
			pointList.clear();
			pointList = null;
			pointList = new ArrayList<View>();
		}
		if (null != bannerViews) {
			bannerViews.clear();
			bannerViews = null;
			bannerViews = new ArrayList<View>();
		}
		if (null != scheduledExecutorService) {
			scheduledExecutorService.shutdown();
			scheduledExecutorService = null;
			currentItem = 0;
		}
		pointLayout.removeAllViews();
		if (null != bannergamelist && bannergamelist.size() > 0) {
			bannerLayout.setVisibility(View.VISIBLE);
			for (int i = 0; i < bannergamelist.size(); i++) {
				View view = inflater.inflate(R.layout.homepage_banner_item,
						null);
				ImageView img = (ImageView) view.findViewById(R.id.img);
				view.setTag(i);
				view.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(mContext,
								GameInfosActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("bean",
								bannergamelist.get((Integer) v.getTag()));
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
				if(bannergamelist.get(i).gameinfoimage.indexOf("http")!=-1){
					fb.display(img, bannergamelist.get(i).gameinfoimage);
				}else{
					try {
						InputStream in = mContext.getAssets().open(
								bannergamelist.get(i).gameinfoimage);
						Bitmap bmp = BitmapFactory.decodeStream(in);
						img.setImageBitmap(bmp);
					} catch (IOException e) {
					}
				}
				bannerViews.add(view);
				ImageView point = new ImageView(getActivity());
				point.setBackgroundResource(R.drawable.gray1);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				params.setMargins(5, 0, 5, 0);
				point.setLayoutParams(params);
				pointList.add(point);
				pointLayout.addView(point);
			}
			if (bannergamelist.size() < 3 && bannergamelist.size() > 1) {
				for (int i = 0; i < bannergamelist.size(); i++) {
					View view = inflater.inflate(R.layout.homepage_banner_item,
							null);
					ImageView img = (ImageView) view.findViewById(R.id.img);
					fb.display(img, bannergamelist.get(i).gameinfoimage);
					bannerViews.add(view);
				}
			}
			pointList.get(0).setBackgroundResource(R.drawable.green1);
			bannertitleTextView.setText(bannergamelist.get(0).logotitle);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, (int) (MyApplication.widthPixels / 64.00*37.4));  //640：374
			bannerLayout.setLayoutParams(parm);
			bannerMyViewpager.setAdapter(new HomepageViewpagerAdapter(
					bannerViews));
			bannerMyViewpager.setOnPageChangeListener(new MyPagelistener());
			if (bannergamelist.size() > 1) {
				scheduledExecutorService = Executors
						.newSingleThreadScheduledExecutor();
				scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(),
						1, 5, TimeUnit.SECONDS);
			}
		}
	}

	/**
	 * 
	 * 
	 * @Title: readInfo
	 * @Description: TODO
	 * @param @param type
	 * @return void
	 */
	private void readInfo(final int platId) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					switch (platId) {
					case 1:
						netwyList = JsonUtils.getHomegamelist(platId, 4, "0");
						break;

					case 2:
						netsyList = JsonUtils.getHomegamelist(platId, 4, "0");
						break;

//					case 3:
//						netyyList = JsonUtils.getHomegamelist(platId);
//						break;
//					case 4:
//						netdjList = JsonUtils.getHomegamelist(platId);
//						break;

					default:
						break;
					}
					msg.arg1 = platId;
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				switch (msg.arg1) {
				case 1:
					readInfo(2);
					break;
				case 2:
					readInfo(3);
					break;
				case 3:
					readInfo(4);
					break;
				case 4:
					contentLayout.removeAllViews();
					if(netwyList.size()>0){
						wyList.clear();
						wyList = new ArrayList<GameInfo>();
						wyList = netwyList;
						db.deleteGameinfoBytype(2);
						for (int i = 0; i < netwyList.size(); i++) {
							db.saveGameinfo(netwyList.get(i), 2);
						}
						setItemlayout(0, wyList);
					}else{
						setItemlayout(0, wyList);
					}
					if(netsyList.size()>0){
						syList.clear();
						syList = new ArrayList<GameInfo>();
						syList = netsyList;
						db.deleteGameinfoBytype(3);
						for (int i = 0; i < netsyList.size(); i++) {
							db.saveGameinfo(netsyList.get(i), 3);
						}
						setItemlayout(1, syList);
					}else{
						setItemlayout(1, syList);
					}
//					if(netyyList.size()>0){
//						yyList.clear();
//						yyList = new ArrayList<GameInfo>();
//						yyList = netyyList;
//						db.deleteGameinfoBytype(4);
//						for (int i = 0; i < netyyList.size(); i++) {
//							db.saveGameinfo(netyyList.get(i), 4);
//						}
//						setItemlayout(2, yyList);
//					}else{
//						setItemlayout(2, yyList);
//					}
//					if(netdjList.size()>0){
//						djList.clear();
//						djList = new ArrayList<GameInfo>();
//						djList = netdjList;
//						db.deleteGameinfoBytype(5);
//						for (int i = 0; i < netdjList.size(); i++) {
//							db.saveGameinfo(netdjList.get(i), 5);
//						}
//						setItemlayout(3, djList);
//					}else{
//						setItemlayout(3, djList);
//					}
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}

		};
	};

//	/**
//	 * 设置热点
//	 * 
//	 * @Title: setHotAndAct
//	 * @Description: TODO
//	 * @param @param type
//	 * @return void
//	 */
//	private void setHot(final int type, final List<GameInfo> list) {
//		if (null != list && list.size() > 0) {
//			View view = inflater.inflate(R.layout.homepage_conent_item_one,
//					null);
//			TextView title = (TextView) view.findViewById(R.id.itemname);
//			title.setText(itemtitles[type - 1]);
//			title.setTextColor(getResources().getColor(
//					R.color.homepage_news_color));
//			LinearLayout itemLayout = (LinearLayout) view
//					.findViewById(R.id.itemlayout);
//			for (int i = 0; i < list.size(); i++) {
//				View item = inflater.inflate(R.layout.homepage_itemlayout_item,
//						null);
//				ImageView img = (ImageView) item.findViewById(R.id.item_img);
//				RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(
//						MyApplication.widthPixels / 5 * 2,
//						MyApplication.widthPixels / 25 * 7);
//				parms.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//				img.setLayoutParams(parms);
//				if (Utils.getpreference(mContext, MyApplication.HOT)
//						.equals("1")) {
//					fb.display(img, list.get(i).img123Url);
//				} else {
//					try {
//						InputStream in = mContext.getAssets().open(
//								list.get(i).img123Url);
//						Bitmap bmp = BitmapFactory.decodeStream(in);
//						img.setImageBitmap(bmp);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//				TextView titleTextView = (TextView) item
//						.findViewById(R.id.item_name);
//				titleTextView.setText(list.get(i).ga123meName);
//				itemLayout.addView(item);
//				item.setTag(i);
//				item.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View view) {
//						Intent intent = new Intent(mContext,
//								GameInfosActivity.class);
//						Bundle bundle = new Bundle();
//						bundle.putSerializable("bean",
//								list.get((Integer) view.getTag()));
//						intent.putExtras(bundle);
//						startActivity(intent);
//
//					}
//				});
//			}
//			LinearLayout showallLayout = (LinearLayout) view
//					.findViewById(R.id.showallayout);
//			showallLayout.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					Intent intent = new Intent(getActivity(),
//							GameAllListActivity.class);
//					startActivity(intent);
//				}
//			});
//			contentLayout.addView(view);
//		}
//	}

	private void setItemlayout(final int type, final List<GameInfo> list) {
		if (null != list && list.size() > 0) {
			View view = inflater.inflate(R.layout.home_item_layout, null);
			TextView title = (TextView) view.findViewById(R.id.itemname);
			title.setText(itemtitles[type]);
			title.setTextColor(getResources().getColor(
					R.color.homepage_news_color));
			LinearLayout showallLayout = (LinearLayout) view
					.findViewById(R.id.showallayout);
			ImageView img = (ImageView) view.findViewById(R.id.icon);
			img.setBackgroundResource(icons[type]);
			showallLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(getActivity(),
							GameAllListActivity.class);
					intent.putExtra("type", type);
					startActivity(intent);
				}
			});
			MyGridView gridView = (MyGridView) view
					.findViewById(R.id.itemgridview);
			Myadapter adapter = new Myadapter(list);
			gridView.setAdapter(adapter);
			gridView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					if(Network.checkNetWork(mContext)){
						Intent intent = new Intent(mContext,
								GameInfosActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("bean",
								list.get(position));
						intent.putExtras(bundle);
						startActivity(intent);
					}else{
						showToast("网络未连接，请检查您的网络");
					}
				}
			});
			contentLayout.addView(view);
		}
	}

//	/**
//	 * 设置热点
//	 * 
//	 * @Title: setHotAndAct
//	 * @Description: TODO
//	 * @param @param type
//	 * @return void
//	 */
//	private void setAct(final int type, final List<Actvitieslist> list) {
//		if (null != list && list.size() > 0) {
//			View view = inflater.inflate(R.layout.homepage_conent_item_one,
//					null);
//			TextView title = (TextView) view.findViewById(R.id.itemname);
//			title.setText(itemtitles[type - 1]);
//			title.setTextColor(getResources().getColor(
//					R.color.homepage_news_color));
//			LinearLayout itemLayout = (LinearLayout) view
//					.findViewById(R.id.itemlayout);
//			for (int i = 0; i < list.size(); i++) {
//				View item = inflater.inflate(R.layout.homepage_itemlayout_item,
//						null);
//				ImageView img = (ImageView) item.findViewById(R.id.item_img);
//				RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(
//						MyApplication.widthPixels / 5 * 2,
//						MyApplication.widthPixels / 25 * 7);
//				parms.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//				img.setLayoutParams(parms);
//				if (Utils.getpreference(mContext, MyApplication.ACT)
//						.equals("1")) {
//					fb.display(img, list.get(i).imgU123rl);
//				} else {
//					try {
//						InputStream in = mContext.getAssets().open(
//								list.get(i).img123Url);
//						Bitmap bmp = BitmapFactory.decodeStream(in);
//						img.setImageBitmap(bmp);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//				TextView titleTextView = (TextView) item
//						.findViewById(R.id.item_name);
//				titleTextView.setText(list.get(i).actName);
//				itemLayout.addView(item);
//				item.setTag(i);
//				item.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View view) {
//						Intent intent = new Intent(mContext,
//								ActInfoActivity.class);
//						Bundle bundle = new Bundle();
//						bundle.putSerializable("info",
//								list.get((Integer) view.getTag()));
//						intent.putExtras(bundle);
//						startActivity(intent);
//					}
//				});
//			}
//			LinearLayout showallLayout = (LinearLayout) view
//					.findViewById(R.id.showallayout);
//			showallLayout.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					Intent intent = new Intent(getActivity(),
//							GameActActivity.class);
//					startActivity(intent);
//				}
//			});
//			contentLayout.addView(view);
//		}
//	}

	/**
	 * 设置已订阅游戏新闻
	 * 
	 * @Title: setNewslayout
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void setNewslayout() {
		View view = inflater.inflate(R.layout.homepage_content_item_news, null);
		TextView title = (TextView) view.findViewById(R.id.itemname);
		title.setText(itemtitles[2]);
		title.setTextColor(getResources().getColor(R.color.homepage_news_color));
		LinearLayout itemLayout = (LinearLayout) view
				.findViewById(R.id.itemlayout);
		for (int i = 0; i < newsList.size(); i++) {
			View item = inflater.inflate(R.layout.homepage_news_item, null);
			ImageView img = (ImageView) item.findViewById(R.id.item_img);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					MyApplication.widthPixels / 5 * 2,
					MyApplication.widthPixels / 25 * 6);
			img.setLayoutParams(parm);
			img.setBackgroundResource(R.drawable.home_hot_bg1);
			TextView titleTextView = (TextView) item.findViewById(R.id.title);
			titleTextView.setText(newsList.get(i).gamename);
			TextView zhaiyaoTextView = (TextView) item
					.findViewById(R.id.zhaiyao);
			zhaiyaoTextView.setText("狼来了！！！！！");
			itemLayout.addView(item);
		}
		LinearLayout showallLayout = (LinearLayout) view
				.findViewById(R.id.showallayout);
		showallLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});
		contentLayout.addView(view);
	}

	/**
	 * banner的pagerlistener监听
	 * 
	 * @Project: MainActivity
	 * @Title: HomeFragment.java
	 * @Package com.inwhoop.gameproduct.fragment
	 * @Description: TODO
	 * 
	 * @author dyong199046@163.com 代勇
	 * @date 2014-4-3 下午1:33:00
	 * @Copyright: 2014 呐喊信息技术 All rights reserved.
	 * @version V1.0
	 */
	class MyPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int position) {

			for (int i = 0; i < pointList.size(); i++) {
				pointList.get(i).setBackgroundResource(R.drawable.gray1);
			}
			pointList.get(position % pointList.size()).setBackgroundResource(
					R.drawable.green1);
			if (bannergamelist.size() > position % pointList.size()) {
				bannertitleTextView.setText(bannergamelist.get(position
						% pointList.size()).logotitle);
			}
			currentItem = position;
		}
	}

	/**
	 * 
	 * @Title: getGameList
	 * @Description: TODO
	 * @param @return
	 * @return List<GameInfo>
	 */
	private List<GameInfo> getGameList() {
		List<GameInfo> list = new ArrayList<GameInfo>();
		String[] path = {
				"http://115.28.139.158/File/news_slide_635299945936155201.png",
				"http://115.28.139.158/File/news_slide_635299884820635780.png",
				"http://115.28.139.158/File/news_slide_635299912605681213.png",
				"http://115.28.139.158/File/news_slide_635305692993523900.png" };
		for (int i = 0; i < path.length; i++) {
			GameInfo info = new GameInfo();
			info.gameid = i + 1;
//			info.gam123etype = 3;
			info.gamename = "永恒之塔";
			info.gameinfoimage = path[i];
			list.add(info);
		}
		return list;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.head_right_second:
			Intent intent = new Intent(getActivity(), SearchGameActivity.class);
			startActivity(intent);
			break;

		default:
			break;
		}
	}

	/**
	 * 换行切换任务
	 * 
	 * @author Administrator
	 * 
	 */
	private class ScrollTask implements Runnable {

		public void run() {
			synchronized (bannerMyViewpager) {
				currentItem++;
				xhandler.obtainMessage().sendToTarget();
			}
		}
	}

	private Handler xhandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			bannerMyViewpager.setCurrentItem(currentItem);
		};
	};

	class Myadapter extends BaseAdapter {

		private List<GameInfo> list = null;
		
		public Myadapter(List<GameInfo> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			Holder holder = null;
			if (null == convertView) {
				holder = new Holder();
				convertView = inflater.inflate(
						R.layout.homepage_itemlayout_item, null);
				holder.img = (ImageView) convertView
						.findViewById(R.id.item_img);
				RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(
						android.widget.RelativeLayout.LayoutParams.FILL_PARENT,
						MyApplication.widthPixels / 25 * 7);
				parms.addRule(RelativeLayout.ALIGN_PARENT_TOP);
				holder.img.setLayoutParams(parms);
				holder.tv = (TextView) convertView.findViewById(R.id.item_name);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			if(list.get(position).gameinfoimage.indexOf("http")!=-1){
				fb.display(holder.img, list.get(position).gameinfoimage);
			}else{
				try {
					InputStream in = mContext.getAssets().open(
							list.get(position).gameinfoimage);
					Bitmap bmp = BitmapFactory.decodeStream(in);
					holder.img.setImageBitmap(bmp);
				} catch (IOException e) {
				}
			}
			holder.tv.setText(list.get(position).gamename);
			return convertView;
		}

		class Holder {
			ImageView img;
			TextView tv;
		}

	}

}
