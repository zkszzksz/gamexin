package com.inwhoop.gameproduct.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.GameLoopAddActivity;
import com.inwhoop.gameproduct.acitivity.MainActivity;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.utils.DialogShowStyle;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.SyncHttp;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title: TODO
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: 游戏圈页面
 * @date 2014/4/8   15:08
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GameLoopFragment extends BaseFragment {
    private Context context;
    private RadioGroup gameLoopGroup;
    private RadioButton gameOfTrade;
    private RadioButton gameOfLoop;
    private RadioButton personalFriends;
    private PersonFriendFragment personFriendFragment;
    private LoopFragment loopFragment;
    private TradeFragment tradeFragment;
    private DialogShowStyle dialogShowStyle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_loop_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        gameLoopGroup = (RadioGroup) view.findViewById(R.id.game_loop_fragment_group);
        gameOfLoop = (RadioButton) view.findViewById(R.id.game_loop_fragment_game_of_loop);
        gameOfTrade = (RadioButton) view.findViewById(R.id.game_loop_fragment_game_of_trade);
        personalFriends = (RadioButton) view.findViewById(R.id.game_loop_fragment_personal_friends);
        init(view);
        setRightSecondBt(R.drawable.add_friend);
        setTitle(R.string.game_loop);
        //dialogShow();
        loadFragment(0);
        gameLoopGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (gameOfLoop.isChecked()) {
                    setRadioButtonTextColor(gameOfLoop);
                    //dialogShow();
                    loadFragment(0);
                }
                if (gameOfTrade.isChecked()) {
                    setRadioButtonTextColor(gameOfTrade);
                    //dialogShow();
                    loadFragment(1);
                }
                if (personalFriends.isChecked()) {
                    setRadioButtonTextColor(personalFriends);
                  // dialogShow();
                    loadFragment(2);
//                    loadFragment(2);
                }
            }
        });
        setAddClick();
    }

    private void dialogShow() {
        dialogShowStyle=new DialogShowStyle(getActivity(), getResources().getString(R.string.load_data));
        dialogShowStyle.dialogShow();
    }

    /*
    设置添加好友的点击事件
     */
    private void setAddClick() {
        getRightSecondBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, GameLoopAddActivity.class));
            }
        });
    }

    private void loadingPage(final int pagePosition) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                loadFragment(pagePosition);
                msg.what = MyApplication.SUCCESS;
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    dialogShowStyle.dialogDismiss();
                    break;
                case MyApplication.FAIL:
                    break;
                case MyApplication.ERROR:
                    break;
            }
        }
    };

    private void loadFragment(final int index) {

        FragmentTransaction fm = getFragmentManager().beginTransaction();
        if (null != personFriendFragment) {
            fm.hide(personFriendFragment);
        }
        if (null != loopFragment) {
            fm.hide(loopFragment);
        }
        if (null != tradeFragment) {
            fm.hide(tradeFragment);
        }
        switch (index) {
            case 0:
                if (null == loopFragment) {
                    loopFragment = new LoopFragment();
                    fm.add(R.id.game_loop_fragment_layout, loopFragment, "");
                } else {
                    fm.show(loopFragment);
                }
                break;
            case 1:
                if (null == tradeFragment) {
                    tradeFragment = new TradeFragment();
                    fm.add(R.id.game_loop_fragment_layout, tradeFragment, "");
                } else {
                    fm.show(tradeFragment);
                }
                break;
            case 2:
                if (null == personFriendFragment) {
                    personFriendFragment = new PersonFriendFragment();
                    fm.add(R.id.game_loop_fragment_layout, personFriendFragment, "");
                } else {
                    fm.show(personFriendFragment);
                }
                break;
        }
        fm.commit();
    }

    /*
           设置radioButton被点击是的颜色切换
            */
    private void setRadioButtonTextColor(RadioButton radioButton) {
        gameOfTrade.setTextColor(context.getResources().getColor(R.color.blue_33a6ff));
        gameOfLoop.setTextColor(context.getResources().getColor(R.color.blue_33a6ff));
        personalFriends.setTextColor(context.getResources().getColor(R.color.blue_33a6ff));
        radioButton.setTextColor(context.getResources().getColor(R.color.white));
    }
}
