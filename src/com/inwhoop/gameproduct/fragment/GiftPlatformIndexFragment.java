package com.inwhoop.gameproduct.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GiftPlatformGameInfoListAdapter;
import com.inwhoop.gameproduct.adapter.GiftPlatformIndexBunnerListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.view.CustomList;
import com.inwhoop.gameproduct.view.NewCustomList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 礼包平台页面-首页
 *
 * @author dingwenlong
 * @version V1.0
 * @Project: GameProduct
 * @Title: GiftPlatformIndexFragment.java
 * @Description: TODO
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GiftPlatformIndexFragment extends Fragment {
    private CustomList gameInfoList = null;
    private Context context;
    private DisplayMetrics dm;
    private RelativeLayout bunnerRela;
    private NewCustomList smallBunner;
    private ScrollView scrollView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        View view = inflater.inflate(R.layout.gift_platform_index_fragment, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        gameInfoList = (CustomList) view.findViewById(R.id.gift_bag_platform_fragment_game_list);
        bunnerRela = (RelativeLayout) view.findViewById(R.id.gift_bag_platform_fragment_img_rela);
        smallBunner = (NewCustomList) view.findViewById(R.id.gift_bag_platform_fragment_bunner_list);
        scrollView = (ScrollView) view.findViewById(R.id.gift_bag_platform_fragment_scroll);
        ViewGroup.LayoutParams params = bunnerRela.getLayoutParams();
        params.height = MyApplication.widthPixels / 2;
        bunnerRela.setLayoutParams(params);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListDataShow();
        setBunnerListDataShow();
    }

    /*
   设置游戏信息列表显示
    */
    private void setListDataShow() {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        Map<String, String> map;
        for (int i = 0; i < 5; i++) {
            map = new HashMap<String, String>();
            map.put("id", "1");
            list.add(map);
        }
       /* GiftPlatformGameInfoListAdapter adapter = new GiftPlatformGameInfoListAdapter(context, list);
        gameInfoList.setAdapter(adapter);*/
    }

    private void setBunnerListDataShow() {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        Map<String, String> map;
        for (int i = 0; i < 5; i++) {
            map = new HashMap<String, String>();
            map.put("id", "1");
            list.add(map);
        }
        final GiftPlatformIndexBunnerListAdapter adapter = new GiftPlatformIndexBunnerListAdapter(context, list);
        smallBunner.setAdapter(adapter);

        smallBunner.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        smallBunner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long id) {
                MyApplication.position = position;
                adapter.notifyDataSetChanged();
            }
        });
        smallBunner.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    scrollView.requestDisallowInterceptTouchEvent(true);
                }
                return false;
            }
        });
    }
}
